<?php
    session_start();
    require_once '../models/conexion.php';
	require_once '../controllers/funcs/funcs.php';
        
    $busqueda = strtolower($_REQUEST['nombre']);
    if(empty($busqueda)){
        header("location: ../views/users.php");
        mysqli_close($mysqli);
    }
    $query = mysqli_query($mysqli,"SELECT * FROM usuarios WHERE usuarios.nombre LIKE '%$busqueda%'");
    
    mysqli_close($mysqli);
    $result = mysqli_num_rows($query);
    if($result > 0){
        while($data = mysqli_fetch_array($query)){
?>

<html>
    <head>
        <link rel="stylesheet" href="../views/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../views/AlertifyJS/css/alertify.min.css" />
        <link rel="stylesheet" type="text/css" href="../views/css/menu_materias.css">
        <link rel="stylesheet" type="text/css" href="../views/css/lista_usuarios.css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
        <title>Lista de Usuarios</title>
    </head>
    <body>
        <div  id="cols" class="col-12">
        <div id="col-img" class="">
            <img  id="sepaes2" src="../views/css/imagenes/sepaes2.png">
        </div>
        <div id="col-saludo" class="">
            
        </div>
        <div id="col-btns" class="">
          <a id="btn-salir" class="btn btn-primary" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a>
        </div>
            
    </div>
    <br><br><br><br>

    <div class="todo">  
        <div id="contenido">
            <form class="finder" action="../controllers/buscar.php">
                <div class="search-box">
                    <input name="nombre" type="text" class="search-txt" placeholder="Buscar usuario">
                    <input type="submit" value="Buscar" class="search-btn" href="../controllers/buscar.php">
                </div>
            </form>
        <table style="margin: auto; width: 800px; border-collapse: separate; border-spacing: 10px 5px;">
            <thead>
                <th>Usuario</th>
                <th>Nombre</th>
                <th>Correo Electrónico</th>
                <th>Imágen</th>
                <th> <a href="registro.php">   <button type="button" class="btn btn-info">Nuevo</button></a></th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $data['usuario'];?></td>
                    <td><?php echo $data['nombre'];?></td>
                    <td><?php echo $data['correo']?></td>
                    <td><?php echo "<img src='".$data['logo_url']."' width='50' >";?></td>
                    <td><a href='../views/modificar.php?id=".$registro['id']."'> <button type='button' class='btn btn-success'>Modificar</button> </a></td>
                    <td><a href='eliminar.php?id=".$registro['id']."'> <button type='button' class='btn btn-danger'>Eliminar</button> </a></td>
                </tr>
            </tbody>
    </table>
    </div>
    </div>
    </body>
</html>
<?php 
                                                }
    }
    
?>