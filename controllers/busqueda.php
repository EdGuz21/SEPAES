<?php
    session_start();
    require_once '../models/conexion.php';
    require_once 'funcs/funcs.php';

    $salida = "";

    $query = "SELECT * FROM usuarios";

    if (isset($_POST['consulta'])) {
    	$q = $mysqli->real_escape_string($_POST['consulta']);
    	$query = "SELECT * FROM usuarios WHERE usuarios.nombre LIKE '%$q%' OR usuarios.usuario LIKE '%$q%'";
    }

    $resultado = $mysqli->query($query);

    if ($resultado-> num_rows > 0) {
    	$salida.="<table col-10 col-md-10 col-sm-10 class='tabla_datos table'>
    			<thead>
    				<tr id='titulo'>
    					<th>Usuario</th>
                        <th>Nombre</th>
                        <th>Correo Electrónico</th>
                        <th>Imágen</th>
    				</tr>

    			</thead>
    			

    	<tbody>";

    	while ($fila = $resultado->fetch_assoc()) {
    		$salida.="<tr>
    					<td>".$fila['usuario']."</td>
    					<td>".$fila['nombre']."</td>
    					<td>".$fila['correo']."</td>
    					<td><img src='".$fila['logo_url']."' width='50'></td>
                        <td><a href='../views/modificar.php?id=".$fila['id']."'><button type='button' class='btn btn-success'>Modificar</button></a></td>
                        <td><a href='eliminar.php?id=".$fila['id']."'> <button type='button' class='btn btn-danger'>Eliminar</button> </a></td>
        			</tr>";
    	}

    	$salida.="</tbody></table>";
        }else{
    	$salida.="<text class='h4'>Usuario no encontrado</text>";
        }


    echo $salida;



?>