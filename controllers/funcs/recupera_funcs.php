<?php
    if (!empty($_POST)) {
    $user_id = $mysqli->real_escape_string($_POST['user_id']);
    $token = $mysqli->real_escape_string($_POST['token']);
    $password = $mysqli->real_escape_string($_POST['password']);
    $con_password = $mysqli->real_escape_string($_POST['con_password']);

    if (empty($_POST['password']) && empty($_POST['con_password'])) {
        $error ='No ha ingresado ninguna contraseña';
    }
     if (!empty($_POST['password']) && empty($_POST['con_password'])) {
        $error ='No ha confirmado su contraseña';
    }
     if (empty($_POST['password']) && !empty($_POST['con_password'])) {
        $error ='No ha llenado el primer campo de contraseña';
    }

    if (!empty($_POST['password']) && !empty($_POST['con_password'])) 
    {

        if (!isValidPass($password)) {
            $error = validar_clave($password);
        }

        if (isValidPass($password)) 
        {
            if(validaPassword($password, $con_password))
            {
                $pass_hash = hashPassword($password);
                if(cambiaPassword($pass_hash, $user_id, $token))
                {
                echo "<script language='javascript'>con();</script>";
                } else 
                {
                    $error = "Error al modificar contrase&ntilde;a";    
                }      
            } else 
            {
                $error ='Las contraseñas no coinciden';
            }
        }
        } 
        if (isset($error)) {
                 $no_user = 
            "<div class="."''".">
            <div class="."''".">
            <div class="."'alert alert-danger'".">
            <button class="."'close'"." data-dismiss="."'alert'".">
            <span>&times;</span>
            </button>".
            $error."
            </div>
            </div>
            </div>";
            }
    }

    
?> 