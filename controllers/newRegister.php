<?php 
    $errors = array();
	
	if(!empty($_POST))
	{
					
	   $nombre = $mysqli->real_escape_string($_POST['nombre']);	
		$usuario = $mysqli->real_escape_string($_POST['usuario']);	
		$password = $mysqli->real_escape_string($_POST['password']);	
		$con_password = $mysqli->real_escape_string($_POST['con_password']);	
		$email = $mysqli->real_escape_string($_POST['email']);	
	
		
		$activo = 1;
		$tipo_usuario = 2;

         	
		if(isNull($nombre, $usuario, $password, $con_password, $email))
		{
			$errors[] = "Debe llenar todos los campos";
		}
		
		if(!isEmail($email)){
			$errors[] = "Dirección de correo inválida";
		}

		if (!isValidPass($password)) {
			$errors[] = validar_clave($password);
		}

		if (isValidPass($password)) 
		{
			if(!validaPassword($password, $con_password))
			{
				$errors[] = "Las contraseñas no coinciden";
			}
		}

		if(usuarioExiste($usuario))
		{
			$errors[] = "El nombre de usuario $usuario ya existe";
		}
		
		if(emailExiste($email))
		{
			$errors[] = "El correo electronico $email ya existe";
		}
		

        if(count($errors) == 0)
		{
            $pass_hash = hashPassword($password);
            $token = generateToken();
			 	
            $registro = registraUsuario($usuario, $pass_hash, $nombre, $email, $activo, $token, $tipo_usuario);
				
				
            header('Location: index.php');

				
					
        }else{
            $errors[] = "Error al Registrar";
        }   
    }else{
		echo "";
	}
?>