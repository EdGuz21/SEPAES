<?php
  // No almacenar en el cache del navegador esta página.
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                     // Expira en fecha pasada
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");        // Siempre página modificada
        header("Cache-Control: no-cache, must-revalidate");                   // HTTP/1.1
        header("Pragma: no-cache");
?>