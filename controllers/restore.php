<?php
    $errors = array();
    if(!empty($_POST))
    {
        $email = $mysqli->real_escape_string($_POST['email']);
        
        if (empty($_POST['email'])) {
        	$error = "No ha ingresado ningún correo electrónico";
        	 
        }
          if(!empty($_POST['email']) && !isEmail($email))
        {
        	 $error = "El correo electrónico no es válido"; 
        }

        if (!empty($_POST['email']) && isEmail($email) && !emailExiste($email)) {
        	$error="El correo electrónico ingresado no pertence a un usuario";
        }
        
        if(emailExiste($email))
        {           
            $user_id = getValor('id', 'correo', $email);
            $nombre = getValor('nombre', 'correo', $email);
            
            $token = generaTokenPass($user_id);
            
            $url = 'http://'.$_SERVER["HTTP_HOST"].'/SEPAES/views/cambia_pass.php?user_id='.$user_id.'&token='.$token;
            
            $asunto = 'Recuperar Password - Sistema de Usuarios';
            $cuerpo = "Hola $nombre: <br /><br />Se ha solicitado un reinicio de contrase&ntilde;a. <br/><br/>Para restaurar la contrase&ntilde;a, visita la siguiente direcci&oacute;n: <a href='$url'>$url</a>";
            
            if(enviarEmail($email, $nombre, $asunto, $cuerpo)){
               // $error= "Vamos a enviar un correo electronico a la direcion ". $email. " para restablecer tu password";
                echo "<script language='javascript'>enviado();</script>";
                
            }
            } else {
            	
        	}
        	if (isset($error)) {
        		 $no_user = 
            "<div class="."''".">
            <div class="."''".">
            <div class="."'alert alert-danger'".">
            <button class="."'close'"." data-dismiss="."'alert'".">
            <span>&times;</span>
            </button>".
            $error."
            </div>
            </div>
            </div>";
        	}
        	
    }
?>