<?php
	require_once '../models/conexion.php';
	require_once '../controllers/funcs/funcs.php';

    if(isset($_SESSION["id_usuario"]) && $_SESSION['tipo_usuario']==1) {
        header("Location: ../views/users.php");
    } 
	
	if(!isset($_SESSION["id_usuario"])){ //Si no ha iniciado sesión redirecciona a index.php
		header("Location: ../index.php");
	}
	
	$idUsuario = $_SESSION['id_usuario'];
	
	$sql = "SELECT id, nombre FROM usuarios WHERE id = '$idUsuario'";
	$result = $mysqli->query($sql);
	
	$row = $result->fetch_assoc();
?>