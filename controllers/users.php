<!DOCTYPE html>

<html>
<head>
	<link rel="stylesheet" href="../views/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../views/AlertifyJS/css/alertify.min.css" />
    <link rel="stylesheet" type="text/css" href="../views/css/lista_usuarios.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <title>Lista de Usuarios</title>
</head>
<body>
	<div  id="cols" class="col-12 cont-barra">
        <div id="col-img" class="cont-img">
            <img class="logo" id="sepaes2" src="../views/css/imagenes/sepaes2.png">
        </div>
        <div id="col-saludo" class="cont-hi">
            <h1>Lista de usuarios</h1>
        </div>
        <div id="col-btns" class="cont-btn">
          <a id="btn-salir" class="btn btn-primary float-right" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a>
        </div>  
    </div>

    <br><br><br><br>

    <div class="todo">  
        <div id="contenido">
            <div class="finder">
            	<div class="container search-box">
					<label for="caja_busqueda">Buscar:</label>
					<input type="text" name="caja_busqueda" id="caja_busqueda" class="search-txt">
				</div>
            </div>
        </div>
        <div id="datos">
			
		</div>
    </div>

	<script type="text/javascript" src="../controllers/JS/jquery.min.js"></script>
	<script type="text/javascript" src="../controllers/JS/busqueda.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../controllers/JavaScript/swiper.min.js"></script>
</body>

</html>