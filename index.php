<?php
	require_once 'models/conexion.php';
	include_once 'controllers/funcs/funcs.php';
	session_start(); //Iniciar una nueva sesión o reanudar la existente	
?>
<html>
	<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" type="text/css" href="views/iconos-vectoriales/icomoon/style.css">
        <link rel="stylesheet" href="views/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="views/AlertifyJS/css/alertify.min.css" />
        <link rel="stylesheet" href="views/AlertifyJS/css/themes/semantic.min.css" />
        <link rel="stylesheet" type="text/css" href="views/css/login.css">
        <script src="views/AlertifyJS/alertify.min.js"></script>
        <script type="text/javascript" src="views/JS/alertas.js"></script>
		<title >Login</title>
	</head>
    <body>
        <?php require_once 'controllers/funcs/funcs_login.php';?>				
	<div class="modal-dialog text-center"> 	
        <div class="col-sm-8 main-section">
            <div class="modal-content">
                <div class="col-12 user-img">
                <img src="views/css/imagenes/sepaes2.png">
               	</div>
                <form id="loginform" class="col-12"  role="form" action="<?php $_SERVER['PHP_SELF']?>" method="POST" autocomplete="off">
                    <div class="form-group" id="user-group">     
                        <input class="form-control" type="text" name="usuario" placeholder="Ingrese su usuario" id="txtuser">
                    </div>
                    <div class="form-group" id="pass-group">     
                       <input class="form-control" type="password" name="password" placeholder="Ingrese su contraseña" id="txtpass">
                    </div>
              
                <button class="btn btn-primary" type="submit" name="entrar" value="Entrar" id="btnentrar"><img id="img-entrar" src="views/css/imagenes/login/entrar.png"> INGRESAR</button>
                </form>
                <div class="col-12 forgot">
                    <a href="views/recupera.php" >¿Olvidaste tu contraseña?</a>
                </div>
                <div class="col-12" id="copi"> <img src="views/css/imagenes/FlowingTecnology.png"> Flowing Tecnology &copy; 2019 </div>
                <br>      
            </div>
        </div>
    </div>
				<div id="notis" class="col-12">
                    <div ></div>
                    <div class="col-4" id="noti-no-user">
                        <?php
                        if (isset($no_user)) {
                            echo $no_user;
                         } 
                        ?>
                    </div> 
                </div>			   
    <script type="text/javascript" src="views/JS/alertas.js"></script>
    <script src="views/bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="views/bootstrap/js/popper.min.js"></script>
    <script src="views/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>						