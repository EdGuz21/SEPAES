-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-08-2019 a las 20:51:09
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `login`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id_pregunta` int(2) NOT NULL,
  `lenguaje` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `matematicas` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `ciencias` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `sociales` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id_pregunta`, `lenguaje`, `matematicas`, `ciencias`, `sociales`) VALUES
(1, 'b', '', '', ''),
(2, 'a', '', '', ''),
(3, 'c', '', '', ''),
(4, 'c', '', '', ''),
(5, 'b', '', '', ''),
(6, 'c', '', '', ''),
(7, 'd', '', '', ''),
(8, 'c', '', '', ''),
(9, 'a', '', '', ''),
(10, 'd', '', '', ''),
(11, 'a', '', '', ''),
(12, 'c', '', '', ''),
(13, 'd', '', '', ''),
(14, 'b', '', '', ''),
(15, 'd', '', '', ''),
(16, 'a', '', '', ''),
(17, 'd', '', '', ''),
(18, 'a', '', '', ''),
(19, 'b', '', '', ''),
(20, 'a', '', '', ''),
(21, 'c', '', '', ''),
(22, 'd', '', '', ''),
(23, 'c', '', '', ''),
(24, 'a', '', '', ''),
(25, 'c', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `tipo`) VALUES
(1, 'Administrador'),
(2, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `password` varchar(130) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `last_session` datetime DEFAULT NULL,
  `activacion` int(11) NOT NULL DEFAULT '0',
  `token` varchar(40) NOT NULL,
  `token_password` varchar(100) DEFAULT NULL,
  `password_request` int(11) DEFAULT '0',
  `id_tipo` int(11) NOT NULL,
  `logo_url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `password`, `nombre`, `correo`, `last_session`, `activacion`, `token`, `token_password`, `password_request`, `id_tipo`, `logo_url`) VALUES
(32, 'Luis', '$2y$10$c0Uv8/CyjtR22AQFh/GfMuwliSZxmFpP28LKkNfD2MaNrgPGSNqJq', 'luis_arias', 'luis.m.arias0610@gmail.com', '2019-06-25 11:52:25', 1, 'd421b94d4380e0e1721d4051feac3146', '', 0, 1, 'imagenes/user.png'),
(35, '22totto', '$2y$10$PsHmuCcCYK3zReAIj9QPfu2MBQ.AoNMFj5V7iO6CCg9Vegu2vzy8W', 'Roberto', 'rodriguez17rj@gmail.com', '2019-06-25 11:48:23', 1, '651191d1779e39efd6808a19aeeac731', '', 0, 2, 'imagenes/roberto.jpeg'),
(37, 'luis_estudiante', '$2y$10$aP3X2vEIaUu4LlkzdtBOquolwp9HJ9r672Os6PF5/BLI5n0MTy8ve', 'Luis Arias', 'luis.m.arias0610@outlook.com', '2019-08-15 13:57:34', 1, '37a7ad9e75f3d8e791f15a9bf62b1b97', '', 0, 2, 'imagenes/user.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
