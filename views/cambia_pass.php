<?php
	
	require_once '../models/conexion.php';
	require_once '../controllers/funcs/funcs.php';

	
	if(empty($_GET['user_id'])){
		header('Location: index.php');
	}
	
	if(empty($_GET['token'])){
		header('Location: index.php');
	}
	
	$user_id = $mysqli->real_escape_string($_GET['user_id']);
	$token = $mysqli->real_escape_string($_GET['token']);
	
	if(!verificaTokenPass($user_id, $token))
	{
        header("Location: index.php");
        exit;
	}
?>


 

<html>
	<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">   
    <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css" />
    <link rel="stylesheet" href="AlertifyJS/css/themes/semantic.min.css" />
    <link rel="stylesheet" type="text/css" href="css/cambiar-pass.css">
		<title>Cambiar Password</title>
	</head>
	
	<body>
    <script src="AlertifyJS/alertify.min.js"></script>
    <script type="text/javascript" src="JS/alertas.js"></script>
    
    <?php require_once '../controllers/funcs/recupera_funcs.php';?>

        <div class="modal-dialog text-center">
            <div class="col-sm-12 main-section">
                <div class="modal-content">
                    <div class="col-12">
                        <h3>Establecer nueva contraseña</h3>
                        <hr>
                        <p>Ingrese una nueva contraseña para su usuario</p>

                    </div>
                    <form id="loginform" class="col-12" role="form" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" autocomplete="off">
                    <input type="hidden" id="user_id" name="user_id" value ="<?php echo $user_id; ?>" />
                    <input type="hidden" id="token" name="token" value ="<?php echo $token; ?>" />
                    <div class="form-group">
                         <input class="form-control" type="password" name="password" placeholder=" Ingresar nueva contraseña " id="txtnp">            
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="con_password" placeholder=" Confirmar contraseña " id="txtnc">    
                    </div>
                    <input class="btn btn-primary" type="submit" name="entrar" value="Cambiar contraseña" id="btnconfirmar">
                    <hr>
                     <label for="usuario" id="">Regresar a <a href="../index.php" >Inicio de Sesi&oacute;n</a></label>
                </form>
                        
                </div>
            </div>
        </div>
        <div id="notis" class="col-12">
                    <div ></div>
                    <div class="col-4" id="noti-no-user">
                        <?php
                        if (isset($error)) {
                            echo $no_user;
                         } 
                        ?>
                    </div> 
                </div>

        <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>	