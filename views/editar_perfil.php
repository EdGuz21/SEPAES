<?php require_once '../controllers/user.php';?>

<html>
    <head>
    <title>Editar perfil</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css" />
    <link rel="stylesheet" type="text/css" href="css/menu_materias.css">


    </head>

    <body style="background-color: #f4f1de">
    <script src="AlertifyJS/alertify.min.js"></script>
    <script type="text/javascript" src="JS/alertas.js"></script>

    <div  id="cols" class="col-12">
    <div id="col-img" class="col-3">
            <img  id="sepaes2"   src="imagenes/sepaes2.png">
    </div>
    <div id="col-saludo" class="col-6">
            
    <div id="col-btns" class="margin-left">
            <a id="homee" class="btn btn-primary" href="elegir_materia.php">Materias <span class="sr-only">(current)</span></a>
            <a id="homee" class="btn btn-primary" href="ranking.html">Ranking<span class="sr-only">(current)</span></a>
      		<a id="homee" class="btn btn-primary" href="logout.php">Salir<span class="sr-only">(current)</span></a>

    </div>    
    </div>
    <form action="controllers/funcs/editar_perfil1.php" method="POST" style="border-collapse: separate; border-spacing: 10px 5px;">
      <input type="hidden" name="id"  name  ="id" value="<?php echo $id_pro;?>">
        
        <label>Nombre: </label>
        <input type="text" id="nombre" name="nombre" value="<?php echo $consulta[1] ?>"><br>
        
        <label>correo: </label>
        <input type="text" id="correo" name="correo" value="<?php echo $consulta[2] ?>"><br>
        
        <br>
        <button type="submit" class="btn btn-success">Guardar</button>
     </form>
    </div>
    
			
     </body>  
     </html>       
