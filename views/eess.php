<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Estudios Sociales</title>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilo_eess.css" type="text/css">
    </head>
    <body>
        <div  id="cols" class="col-12">
              <div id="col-img" class="">
                  <button onclick="location.href='elegir_materia.php'" class="btn btn-primary"><img src="css/imagenes/back.png" id="imgBack"> <a href="elegir_materia.php" id="frback" >Materias</a></button>
              </div>
              <div id="col-saludo" class="">
                  <button class="btn btn-primary" onclick="location.href='testS/test.php'" id="">Iniciar Test</button>
              </div>
              <div id="col-btns" class="">
                  <a id="homee" class="btn btn-primary" href="perfil.php" title="Ir al Perfil">  <span class="sr-only">(current)</span></a>
                  <a id="" class="btn btn-primary" href="ranking.html">Ranking<span class="sr-only">(current)</span></a>
                  <a id="" class="btn btn-primary" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a>
              </div>
        </div>
        <br><br><br>
    <div id="frmateria">
             <p>Estudios Sociales</p>   
    </div>
        <div id="contenedor-cuerpo">
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 1. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Gabriel es un joven que trabaja en una empresa privada como operador de maquinaria
                            eléctrica. A raíz de un corte de energía, la máquina con la que trabajaba se dañó. Gabriel
                            reporta el daño a sus jefes y le dicen que él debe asumir los gastos de la reparación o
                            compra de nueva maquinaria, que se le descontará de su salario. Gabriel manifiesta que
                            es injusto que le hagan pagar por ello. Su jefe le indica que está despedido y que no le
                            darán ninguna compensación.
                            Gabriel acude al Ministerio de Trabajo y Previsión Social (MTPS), donde lo asesoran y
                            tras una demanda a la empresa, le devuelven su trabajo y le pagan los días no laborados.
                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">1</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Según el caso, acudir a instancias autorizadas para resolver problemas como el de
                            Gabriel es importante porque?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                              Velan por el cumplimiento de los derechos laborales cuando no afectan a las empresas
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                               Permiten a los trabajadores dar opiniones sobre el ambiente en el que laboran
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                                Garantizan el respeto de los derechos laborales a través del cumplimiento de la ley
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Aseguran la protección de los derechos patronales en los accidentes de trabajo
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            
         <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 2. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            María Belén es parte de la directiva de la comunidad «Nuevo Horizonte», junto a sus vecinos se ha
                            interesado en conocer el presupuesto de la municipalidad y los costos que generó la remodelación de la
                            casa comunal, que fue inaugurada recientemente por las autoridades. Una de sus vecinas explica que no es
                            posible obtener ese tipo de información, porque así lo ha establecido la municipalidad. Luego de escuchar
                            opiniones, la directiva decide hacer uso de la Ley de Acceso a la Información Pública y realizar los trámites
                            pertinentes.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">2</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuáles son los aportes de la participación ciudadana en los procesos
                            municipales?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-correcta">
                              Permite a la población involucrarse en los asuntos públicos y exigir transparencia en el manejo de los
                              fondos 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div></div>
                            <div class="opcion-respuesta">
                               Promueve la división de ideas entre las personas de la comunidad para que la alcaldía sienta la presión
                               social
                               <br><br>
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Permite la inversión de fondos municipales y la distribución de los gastos por las directivas
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                 Promueve la creación de directivas locales para que las personas sean auditoras de la gestión municipal
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
             <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 3. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            En El Salvador, la construcción de una sociedad pacífica depende de la voluntad y responsabilidad de todos
                            los sectores sociales. La sociedad civil tiene un compromiso histórico de asumir con responsabilidad el rol
                            que le corresponde para contribuir a la lucha en contra de la injusticia y de la corrupción en El Salvador,
                            con la finalidad de avanzar en el proceso de la consolidación democrática que facilite herramientas de
                            resolución de conflictos con enfoque de la no violencia y el respeto a los derechos humanos.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">3</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuáles son los retos de El Salvador para la construcción de una
                            sociedad pacífica?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Promover el activismo social de algunos sectores de la sociedad salvadoreña

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div></div>
                            <div class="opcion-respuesta">
                            Redactar manuales de convivencia social que integren ideales de paz y solidaridad.
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Generar espacios de reflexión y sensibilizar a la sociedad de crear un comité social
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                            Mejorar las condiciones de vida de las personas y promover la convivencia armónica.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            
            
                <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 4. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            La reforma al Artículo 63, en la Constitución de la República de El Salvador, en el 2012, enfatiza el compromiso
                            del Estado con los pueblos indígenas de mantener y desarrollar su identidad étnica y cultural, cosmovisión,
                            valores y espiritualidad. Ante esta situación el Consejo Coordinador Nacional Indígena Salvadoreño (CCNIS),
                            sostiene que no contar con un mecanismo que regule la aplicación de los derechos propios de los pueblos
                            indígenas, incide en que no se respete su cosmovisión y sus diferencias en aspectos étnicos. En el país se ha
                            desplazado el idioma náhuatl, se modifican las costumbres y es necesario que la cultura indígena sea
                            visibilizada y se avance en el respeto a sus derechos. 

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">4</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál es la importancia de visibilizar a la población indígena?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Promover la cultura de los pueblos originarios por medio de actividades folclóricas

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                            Desarrollar acciones que promuevan el idioma autóctono de la sociedad salvadoreña
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                            Garantizar la igualdad de los grupos originarios en la sociedad salvadoreña
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Fomentar reformas constitucionales continuas con expresiones de los pueblos originarios
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
         
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 5. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            El sistema político salvadoreño tiene tareas pendientes, entre ellas la legalización de la ley del agua que
                            garantice la eficiente administración, distribución y cobertura del bien público que es indispensable para
                            la vida. También se pueden analizar problemas relacionados a la corrupción, debilitamiento del Estado de
                            Derecho, discriminación, violencia contra las mujeres y las niñas, entre otros.


                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">5</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál es el desafío actual del sistema político salvadoreño?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-correcta">
                            Garantizar el cumplimiento de los derechos humanos en todos los sectores de la sociedad salvadoreña

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             Evitar los casos de vulnerabilidad de los derechos humanos en algunos sectores de la sociedad salvadoreña
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                             Implementar proyectos que fomenten espacios para la recreación de la sociedad salvadoreña
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Promover campañas informativas para reducir la violencia en las familias salvadoreñas
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
         
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 6. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Miguel y Jorge se dedican a transportar mercadería. Miguel le pregunta a Jorge si exige factura cada vez que
                            compra gasolina, Jorge responde que eso le quita tiempo y por eso no pide factura. En nuestro país existen
                            muchas personas como Jorge y las empresas no se preocupan que los clientes reclamen factura cuando
                            realizan una compra, ya que se evitan pagar impuestos que deben asumir por ley; esto perjudica a El Salvador
                            porque evita que los fondos se utilicen en diferentes programas.


                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">6</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                             ¿Cuál es la importancia de la recaudación fiscal en el país?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                             Aumenta la responsabilidad del Estado en el manejo de fondos

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-correcta">
                             Recolecta fondos públicos para invertirlos en áreas sociales
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                             Ayuda a las empresas a mejorar las ganancias a través de las ventas
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Promueve la igualdad tributaria entre vendedores y compradores
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 7. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Algunos sectores de los movimientos sociales que participaron en las campañas en contra de la
                            privatización de la salud en los noventa e inicios del siglo XXI, coordinan el Foro Nacional de la Salud y
                            convocó a asambleas populares en cinco regiones en 2010, con el fin de diseñar un nuevo plan de salud
                            pública que facilite una mayor cobertura médica a la población. Otros movimientos sociales continúan
                            movilizándose para promover temas relacionados con la defensa del medio ambiente como derecho
                            humano.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">7</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                              ¿Cuál es el papel de los movimientos sociales en El Salvador?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                              Impulsan el apoyo de las organizaciones populares al poder político más influyente

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             Contribuyen a que los sectores que los integran logren solo sus beneficios particulares
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                             Defienden los privilegios e intereses de sus líderes antes que los de sus miembros
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                            Promueven cambios y transformaciones sociales en favor del bien común
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 8. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                           Ante la demanda del crecimiento poblacional, las empresas desarrollan proyectos de construcción de
                            viviendas, para ello, talan árboles de forma indiscriminada, afectando la flora y fauna de la región. En la
                            actualidad varias especies de animales han desaparecido y otras están en peligro de extinción como los
                            garrobos, iguanas, culebras y gran variedad de aves. Hay grupos ambientalistas que han llamado a analizar
                            la situación y a buscar estrategias para preservar el medio ambiente
                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">8</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                              ¿Qué acciones reducen los efectos negativos en el medio ambiente?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Construir espacios para albergar a las especies y explotación de los recursos no renovables
                                <br><br><br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-correcta">
                             Asignar áreas protegidas con suficiente espacio para la preservación de las especies y cumplir las
                             normativas legales de construcción
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                              Impedir la construcción de viviendas para evitar la tala de árboles y reducir la cantidad de habitantes en
                              cada casa
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Imponer multas a las personas que practican la caza y creación de viveros familiares con plantas
                            ornamentales
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 9. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                           La contaminación de los recursos naturales es un problema global con graves consecuencias. Disminuir los índices
                            de contaminación y degradación ambiental plantea uno de los principales desafíos actuales para la humanidad. Es
                            necesario que se realicen acciones desde la organización social que superen la pasividad o conformidad en la
                            actuación de las instituciones. Sin duda, los seres humanos son los que generan mayor contaminación, por lo que
                            hoy se deben desarrollar acciones de cambio que permitan incidir de manera positiva en el ambiente.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">9</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                        ¿cuál de los siguientes casos presenta acciones ciudadanas que contribuyen
                        a reducir efectos negativos en el medio ambiente?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-correcta">
                             Lucía se unió a un movimiento social en su comunidad que realiza proyectos de reciclaje, reutilización y reforestación
                            <br><br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             Asignar áreas protegidas con suficiente espacio para la preservación de las especies y cumplir las
                             normativas legales de construcción
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                              Impedir la construcción de viviendas para evitar la tala de árboles y reducir la cantidad de habitantes en
                              cada casa
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Imponer multas a las personas que practican la caza y creación de viveros familiares con plantas
                            ornamentales
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
             
                <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 10. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        Con la desintegración de la URSS y la caída del Muro de Berlín, muchos países abandonaron el modelo
                        económico que se centraba en el Estado y la industrialización por sustitución de importaciones,
                        provocando un auge en el desarrollo de las economías de mercado alrededor del mundo. Los procesos
                        de desarrollo económico se transformaron y permitieron el intercambio de las empresas para
                        posicionarse en los mercados de distintas partes del planeta. 

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">10</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                         ¿Qué consecuencia económica se produjo a nivel mundial posterior a la
                         caída del bloque socialista?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                             Modificación de la relación de poder entre los países

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             Mayor competencia entre los mercados locales
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                              Globalización de los procesos comerciales
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Control del Estado en la economía nacional
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
             
                <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 11. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        Durante los años setenta, El Salvador experimentó una profunda crisis política, que se manifestó en el
                        deterioro de la comunicación entre la sociedad civil y el Estado. Los mecanismos violentos de control
                        hacia los sectores populares, como la desmovilización violenta de las manifestaciones, generalizaron la
                        insatisfacción de la población, dando como resultado un amplio movimiento popular organizado que
                        alcanzó la cima entre 1970 y 1980, que encabezó la resistencia y lucha en el conflicto armado
                        salvadoreño. 

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">11</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                         Según la información, el conflicto armado en El Salvador fue el resultado de

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                             El aumento de la desigualdad entre salvadoreños para mantener el sistema político

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-correcta">
                             La polarización de la sociedad y la represión a las expresiones de descontento social
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                             La búsqueda del bienestar de los salvadoreños en la repartición equitativa de la tierra
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                             La desaparición y asesinato de líderes revolucionarios para mantener el equilibrio social.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
             
             <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 12. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            El 13 de julio del 2016, la Sala de lo Constitucional de la Corte Suprema de Justicia de El Salvador, resolvió
                            que la Ley de Amnistía de 1993 era inconstitucional, porque violaba las obligaciones internacionales del
                            país de investigar y juzgar. La sentencia restablece el derecho de acceso a la justicia de las víctimas y da
                            pautas para que se legisle en materia de reparación, por lo que se espera que los autores de crímenes
                            como masacres, desapariciones forzadas y torturas durante el conflicto armado sean juzgados.  

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">12</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                         ¿Qué pretende la Sala de lo Constitucional con la sentencia planteada?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Proponer que se investiguen todos los casos de violación de derechos humanos en El Salvador 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                            Establecer que en El Salvador se fortalezcan las leyes vigentes y el respeto a los derechos humanos
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                             Manifestar el sobrepeso permanente de los convenios internacionales sobre las leyes del país
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                            Aplicar la justicia en casos de violaciones a los derechos humanos que quedaron impunes
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
            
            
                         <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 13. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Uno de los puntos decisivos del proceso de diálogo en El Salvador fue el tema de seguridad pública, tras
                            intensas rondas de negociación, se acuerda la supresión de los cuerpos de seguridad militarizados (Guardia
                            Nacional, Policía de Hacienda y Policía Nacional) y la creación de nuevas instituciones como la Policía
                            Nacional Civil y la Academia Nacional de Seguridad Pública, con la misión principal de garantizar el orden
                            y la seguridad pública con valores de una sociedad democrática, ayudando a superar el autoritarismo
                            militar y represivo de los antiguos cuerpos de seguridad.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">13</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                          ¿Cuál fue la visión al crear una nueva institución de seguridad pública?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                             Establecer un modelo de seguridad con enfoque inclusivo 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                            Garantizar la tranquilidad y pasividad de la población salvadoreña
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                              Mantener un sistema de seguridad, respetando los derechos humanos
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Modernizar los cuerpos de seguridad, ampliando las zonas de vigilancia
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
            
             <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 14. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Los estudiantes de segundo año de bachillerato, realizaron una investigación social sobre los tiraderos de
                            basura en la comunidad «El Castaño». Al finalizar la investigación se determinó que los promontorios de
                            basura se deben a que las personas sacan la basura a cualquier hora, sin respetar el horario del tren de
                            aseo y en ocasiones porque los encargados de brindar este servicio lo incumplen. Además, los pepenadores
                            rompen las bolsas para extraer material reciclable y los perros riegan la basura al buscar desperdicios

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">14</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                          ¿Qué acciones de solución se deben realizar?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            La alcaldía debe organizar campañas de higiene y los habitantes deben mantener limpio el área donde viven
                            <br><br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             La directiva de la colonia debe denunciar en la alcaldía municipal el foco de contaminación y compartir información entre vecinos
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            La unidad de salud debe promover charlas sobre hábitos higiénicos y asesorar a la comunidad
                            <br><br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                        La alcaldía debe respetar el horario para que pase el tren de aseo y los habitantes deben sacar la basura en los tiempos estipulados
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
             
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 15. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Gloria es una joven deportada de Estados Unidos, le gusta visitar centros comerciales, hacer uso de las
                            redes sociales y alimentarse con comida rápida (no tradicional o comida chatarra). Marcela, su amiga, le
                            comenta que han cambiado sus hábitos alimenticios y sus preferencias de recreación, porque cuando
                            vivía en El Salvador, prefería la comida autóctona, visitar parques y plazas públicas, para interactuar con
                            sus amistades.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">15</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Cuál es el fenómeno social que refleja Gloria?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-correcta">
                             La transnacionalización de la cultura

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             El sentido de identidad nacional
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                           Globalización comercial
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Transformación social.
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
             
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 16. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Una organización social realizó una investigación sobre la violencia de género en las escuelas del
                            municipio Mixco. Se determinó que las niñas y adolescentes son violentadas por sus compañeros de
                            clase, sobre todo con expresiones y bromas relacionadas con sexo. Al finalizar la investigación
                            compartieron los resultados con los directores de las instituciones involucradas. Al recibir la
                            información, el director de la escuela «El Bálsamo» se reunió con los docentes, el consejo de
                            estudiantes y representantes de padres de familia, quienes analizaron la situación de la escuela y
                            decidieron realizar una serie de proyectos para minimizar o erradicar la violencia de género en la
                            escuela y en los hogares.


                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">16</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Cuál es la utilidad de la investigación social?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                             Posibilita obtener información de diferentes zonas del país en tiempos diferentes

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             Promueve la organización de los diferentes sectores de la sociedad
                             <br><br>
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Estudia diferentes problemáticas haciendo comparación entre diferentes actores
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                            Permite la búsqueda de acciones que den solución a los problemas investigados
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
             
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 17. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Un estudio realizado por la Organización Internacional del Trabajo (OIT), determinó que las mujeres que
                            se forman en cursos de ocupación laboral de panadería, cosmetología, corte y confección, en los
                            Institutos de Formación Profesional (IFP) del INSAFORP en El Salvador, tienen como modo de ingreso la
                            práctica del oficio aprendido. El estudio concluye que existe una mayor tasa de participación femenina,
                            especialmente en niveles técnicos. Esto representa una apuesta estratégica por la situación demográfica
                            que viven los países centroamericanos.


                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">17</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Cómo contribuye la investigación de la OIT al desarrollo de la sociedad?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                              Fomenta el consumo de los bienes y servicios producidos por las mujeres

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                              Permite la ocupación de la mujer en los países de Latinoamérica
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                            Promueve políticas que generen espacios de desarrollo laboral para la mujer
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Mejora la cantidad de empleo a través de la creación de carreras universitarias
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
             
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 18. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        Los docentes del Instituto Nacional «Llano Abajo», decidieron investigar las causas del bajo
                        rendimiento académico de los estudiantes, en la asignatura de Estudios Sociales, para ello, han
                        resuelto realizar una investigación cualitativa y presentar el planteamiento del problema con el
                        enunciado, ¿Qué factores influyen en el aprendizaje de Estudios Sociales, en el primer trimestre
                        del año 2017, en los estudiantes de bachillerato? Con el que pretenden conocer las opiniones
                        de los jóvenes, sobre las dificultades que presentan para el aprendizaje de Estudios Sociales


                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">18</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Cuál de las siguientes técnicas de recolección de datos deben aplicar los docentes en su estudio?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Análisis documental, los docentes obtienen datos de los estudiantes en las fichas y otros documentos de la escuela

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-correcta">
                            Entrevista estructurada, los docentes elaboran una lista de preguntas dirigidas a los estudiantes
                            <br><br>
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Encuesta cerrada, los docentes estructuran un cuestionario con dos opciones de respuesta
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Observación experimental, los docentes elaboran guías para observar a los estudiantes
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
             
             <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 19. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        El Salvador es un importador de petróleo y sus derivados, por lo que se encuentra inmerso en la dinámica del
                        mercado mundial, lamentablemente el impulso de los combustibles alternativos como el etanol (gas) por el
                        momento ha disminuido. El precio del barril de petróleo ha empezado a subir irrefrenablemente desde el último
                        bimestre del 2017 e inicios del 2018, lo que supondrá un importante impacto en la factura petrolera del país y
                        en el bolsillo de los salvadoreños.



                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">19</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Cuál de las siguientes técnicas de recolección de datos deben aplicar los docentes en su estudio?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Análisis documental, los docentes obtienen datos de los estudiantes en las fichas y otros documentos de la escuela

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-correcta">
                            Entrevista estructurada, los docentes elaboran una lista de preguntas dirigidas a los estudiantes
                            <br><br>
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Encuesta cerrada, los docentes estructuran un cuestionario con dos opciones de respuesta
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            Observación experimental, los docentes elaboran guías para observar a los estudiantes
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
            
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 20. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        Las Tecnologías de la Información y Comunicación (TIC) en El Salvador apoyan el desarrollo en la
                            administración pública, el ámbito empresarial, la educación, capacitación, la salud, el empleo, el medio
                            ambiente, la agricultura y la ciencia, en el marco nacional de estrategias cibernéticas y otros. Las
                            personas reciben becas y oportunidades laborales que les permiten continuar sus estudios y prepararse
                            para integrarse a la demanda nacional de personas capacitadas en tecnología.


                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">20</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Cuáles son los aportes de las TIC en el desarrollo económico de El Salvador?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Proporcionan información necesaria sobre diferentes tipos de empleo a los que las personas pueden acceder fácilmente 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                            Comunican a la población constantemente las alzas y bajas de la economía mundial para que esta realice sus
                            compras
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                            Facilitan el acceso a la información, mejoran la preparación profesional y fortalecen los sistemas de negocios
                            <br><br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                             Ofertan estudios en línea con acceso a empleo a nivel nacional e internacional para mejorar la vida de la población
                            </div>
                        </div>
                    </div>    
                </div>
             </div>
            
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 21. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        La globalización en El Salvador, se puede analizar desde dos perspectivas:
                           <br> 1. Los beneficios que gozan una minoría privilegiada que aprovecha el comercio, la inversión, las
                            comunicaciones, las finanzas globales y la concentración de la riqueza.
                           <br> 2. Las mayorías que se encuentran en desventaja para gozar de los beneficios y oportunidades que la
                            globalización ofrece al mundo.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">21</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Qué impacto tiene la globalización para la mayoría de salvadoreños?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                            Disminuye la economía de los salvadoreños debido al aumento del comercio. 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-correcta">
                            Profundiza la desigualdad social en las condiciones de vida de los salvadoreños                          
                        </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Impulsa proyectos que promueven la calidad de vida de los salvadoreños
                            </div> 
                        </div>
                        
                         <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                             Aumenta la inversión social por los beneficios de la inversión extranjera
                            </div>
                        </div>
                        </div>    
                </div>
                </div>
             
                 <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 22. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        La Ley de Protección Integral de la Niñez y Adolescencia (LEPINA), establece en su artículo 35, que el
                            Estado tiene el deber de prever en la política medioambiental, programas permanentes dirigidos a
                            promover la participación de la niña, niño y adolescente en la protección, conservación y disfrute de los
                            recursos naturales y reducir los riesgos resultantes de los peligros ambientales.

                        </p>
                        
                        <p>
                            
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">22</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           ¿Qué acciones debe implementar el Estado para garantizar un medio ambiente sano?

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                           Apoyar financieramente a organizaciones ambientales para la ejecución de programas de educación ambiental y preservación del medio ambiente

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             Mejorar los canales de comunicación de la población con las entidades públicas de medio ambiente para la realización de proyectos ambientales.   
                             <br><br>                     
                        </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Promover la participación de la comunidad y la familia en conmemoraciones y celebraciones de actividades relacionadas con el medio ambiente
                            </div> 
                        </div>
                        
                         <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                             Involucrar a la comunidad y la familia en programas ambientales, planes de prevención de riesgos y ofrecer apoyo institucional medio ambiental
                             <br><br>
                            </div>
                        </div>
                        </div>    
                </div>
                </div>
            
              <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 23. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        Un grupo de estudiantes de bachillerato, visita una universidad local para determinar los
                            campos de estudio de las ciencias sociales: Sociología, Antropología, Economía y Política. Esta
                            tarea se realizó por medio de la observación de diversas situaciones que se viven en la
                            universidad.
                            <br>
                            Las situaciones observadas por los estudiantes fueron las siguientes:
                            
                        </p>
                        
                        <p>
                            <img src="css/imagenes/sociales/imagen_pregunta23_soc.png" id="imgpregunta23_opcion_sociales">
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">23</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Selecciona la opción que representa la clasificación correcta en las situaciones
                            observadas, con el campo de estudio de las ciencias sociales. 

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-correcta">
                           1c, 2d, 3a, 4b. 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             1a, 2d, 3c, 4b.                         
                        </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            1d, 2b, 3a, 4c. 
                            </div> 
                        </div>
                        
                         <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                              1b, 2a, 3c, 4d
                            </div>
                        </div>
                        </div>    
                </div>
                </div>
             
                <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 24. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                        Los rubros del Producto Interno Bruto (PIB) se han caracterizado por reflejar prioridad en las
                            actividades económicas del sector terciario de la economía (comercio y servicios).
                            A continuación, se presenta la distribución del porcentaje del PIB por sector económico. 
                       
                            
                        </p>
                        
                        <p>
                            <img src="css/imagenes/sociales/imagen_pregunta24_soc.png" id="imgpregunta24_opcion_sociales">
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">24</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Los datos presentados, indican que la economía nacional está sostenida por


                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                           El intercambio de productos extranjeros que benefician al país

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                             El ingreso de dinero proveniente de las exportaciones del país                        
                        </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                            Las ganancias por compra de artículos producidos en fábricas
                            </div> 
                        </div>
                        
                         <div class="contenedor-respuestas-der">
                            <div class="literal-padre-correcta"><div class="literal-hijo">D</div></div>
                            <div class="opcion-correcta">
                             Las divisas que producen los salvadoreños en el exterior
                            </div>
                        </div>
                        </div>    
                </div>
                </div>
                           <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 25. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            La migración en El Salvador es una problemática compleja y multicausal, sin embargo, la pobreza es
                            considerada uno de los factores determinantes, que motiva a muchos salvadoreños a migrar.
                            A continuación, se presentan algunos indicadores de pobreza que más inciden en la migración.
                       
                            
                        </p>
                        
                        <p>
                            <img src="css/imagenes/sociales/imagen_pregunta25_soc.png" id="imgpregunta25_opcion_sociales">
                        </p>
                    </div>
                </div>
                  
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">25</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                           Con base en los porcentajes presentados, la migración en El Salvador es generada por


                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                          El subempleo y la falta de acceso a servicios de salud que provocan en la sociedad un descontento social
                            <br><br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                            La inseguridad alimentaria y la violencia que producen en los salvadoreños problemas para mejorar su condición de vida                      
                        </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                            La violencia y el subempleo porque los salvadoreños no satisfacen sus necesidades básicas para vivir dignamente
                            </div> 
                        </div>
                        
                         <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                             El desempleo y la contaminación del ambiente porque hacen posible que los salvadoreños busquen mejores oportunidades
                            </div>
                        </div>
                        </div>    
                </div>
                </div>
          
           
           
          
                
            </div>
    </body>
</html>