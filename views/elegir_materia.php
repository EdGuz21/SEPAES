<?php
	session_start();
    require_once '../models/conexion.php';
	require_once '../controllers/funcs/funcs.php';

    if(isset($_SESSION["id_usuario"]) && $_SESSION['tipo_usuario']==1) {
        header("Location: users.php");
    } 
	
	if(!isset($_SESSION["id_usuario"])){ //Si no ha iniciado sesión redirecciona a index.php
		header("Location: index.php");
	}
	
	$idUsuario = $_SESSION['id_usuario'];
	
	$sql = "SELECT id, nombre FROM usuarios WHERE id = '$idUsuario'";
	$result = $mysqli->query($sql);
	
	$row = $result->fetch_assoc();
?>
<html>
    <head>
   <title>Materias</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css" />
        <link rel="stylesheet" type="text/css" href="css/menu_materias.css">
    </head>
    <body style="background-color: #FFFFFF">
    <div  id="cols" class="col-12 col-md-12 col-sm-12">
        <div>
            <div id="col-img" class="col-12 col-md-3 col-sm-3">
                <img  id="sepaes2" class="aling-middle" src="css/imagenes/sepaes2.png">
            </div>
            <div id="col-saludo" class="col-12 col-md-3 col-sm-3">
                <center><font color='white' class="text-center h3 text-center align-middle"><?php echo '¡Hola, '.utf8_decode($row['nombre'])."!";?></font></center>
            </div>
            <div id="col-btns" class="col-12 col-md-3 float-right">
                <input type="checkbox" id="btnmenu">
                <label for="btnmenu" id="homee"></label>
                <div class="menu">
                    <ul id="" class="">
                        <li><a id="" class="col-12" href="perfil.php">Perfil<span class="sr-only">(current)</span></a></li>
                        <li><a id="" class="col-12" href="ranking.php">Ranking<span class="sr-only">(current)></span></a></li>
                        <li><a id="" class="col-12" href="#">Ayuda<span class="sr-only">(current)</span></a></li>
                        <li><a id="" class="col-12" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a></li>
                    </ul>
                </div>
            </div>  
        </div>  
       </div> 
        <br><br><br><br>   
        <h3> <center> Has click sobre el icono de la asignatura a estudiar</center></h3>
               	<br><br><br>
        <div id="menu" class="">
        	<div class="col-12 col-md-12 col-sm-6">
        		<div id="espacio" class="col-1">-</div>
        		<div class="modal-content">
        			<div id="l" class="col-12">
        				<div onclick="location.href='principal.php'" class="box col-12">
                		<form action="principal.php"  id="frlen" title="Click para estudiar la materia de Lenguaje" alt="Click para estudiar la materia Lenguaje" class=""></form>
        				</div>
        			</div>
        		</div>
            </div>
            <div class="col-12 col-md-12 col-sm-6">
        		<div class="modal-content">
        			<div id="m" class="col-12">
						 <div onclick="location.href='matematicas.php'" class="box col-12">
                		<form action="matematicas.php" id="frmat" title="Click para estudiar la materia de Matemática" alt="Click para estudiar la materia Matemática"></form>
            			</div>
        			</div>
        		</div>
            </div>
            <div class="col-12 col-md-12 col-sm-6">
        		<div class="modal-content">
        			<div id="c" class="col-12">
							<div onclick="location.href='ccnn.php'" class="box col-12">
                			<form action="ccnn.php" id="frcc" title="Click para estudiar la materia de Ciencia" alt="Click para estudiar la materia Ciencia"></form>
            				</div>
        			</div>
        		</div>
            </div>
            <div class="col-12 col-md-12 col-sm-6">
        		<div class="modal-content">
        			<div id="e" class="col-12">
						<div onclick="location.href='eess.php'" class="box col-12">
               			 <form action="eess.php" id="fres"title="Click para estudiar la materia de Estudios Sociales" alt="Click para estudiar la materia Estudios Sociales"></form>
           			 	</div>
        			</div>
        		</div>
            </div>
        </div>
                   <?php if($_SESSION['tipo_usuario']==1) { ?>
							<ul class='nav navbar-nav'>
								<a href='#'>Administrar Usuarios</a>
							</ul>
						<?php } ?>
				<br>
                   <script src="AlertifyJS/alertify.min.js"></script>
        <script type="text/javascript" src="JS/alertas.js"></script>
    </body>
</html>