<html>
    <head>
        <title>Menú de Temas</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/menu_temas.css" type="text/css">
    </head>
    <body>
        <div  id="cols" class="col-12">
        <div id="col-img" class="">
            <button onclick="location.href='elegir_materia.php'" class="btn btn-primary"><img src="css/imagenes/back.png" id="imgBack"> <a href="elegir_materia.php" id="frback" >Materias</a></button>
        </div>
        <div id="col-saludo" class="">
            <button class="btn btn-primary" onclick="location.href='testL/test.php'" id="">Iniciar Test</button>
        </div>
        <div id="col-btns" class="">
            <a id="homee" class="btn btn-primary" href="perfil.php" title="Ir al Perfil">  <span class="sr-only">(current)</span></a>
            <a id="" class="btn btn-primary" href="ranking.html">Ranking<span class="sr-only">(current)</span></a>
            <a id="" class="btn btn-primary" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a>
        </div>
        </div>
        <br><br><br><br>
        <div id="frmateria">
             <p>Lenguaje y Literatura</p>   
        </div>
        <br>
        
        <div class="container" id="contenedor-cuerpo">
            <ul>
                <li onclick="location.href='lenguaje.php'"><a href="lenguaje.php" class="h6">Ver PAES</a></li>
                <li onclick="location.href='unidad_1.php'">
                    <text class="h6" onclick="location.href='unidad_1.php'">Unidad 1: Literatura de la América Precolombina</text>
                    <ul onclick="location.href='unidad_1.php'">Culturas precolombinas (fundamentos cosmovisión y organización sociopolítica).</ul>
                    <ul onclick="location.href='unidad_1.php'">Características de la literatura precolombina. </ul>
                    <ul onclick="location.href='unidad_1.php'">Figuras literarias de omisión: Elipsis, asíndeton, zeugma.</ul><ul>El lenguaje, la lengua y el habla.</ul>
                    <ul onclick="location.href='unidad_1.php'">Uso de la tilde en las palabras exclamativas, interrogativas y vocablos compuestos.</ul>
                    <ul onclick="location.href='unidad_1.php'">Los textos virtuales y su uso responsable.</ul>
                </li>
                <li onclick="location.href=''">
                    <text class="h6">Unidad 2: Literatura del descubrimiento, la conquista y la colonia</text>
                    <ul onclick="location.href=''">Contexto de la literatura del descubrimiento y la conquista.</ul>
                    <ul onclick="location.href=''">Contexto de la literatura colonial americana.</ul>
                    <ul onclick="location.href=''">El Barroco en América.</ul>
                    <ul onclick="location.href=''">Características recursos fónicos: Aliteración, paranomasia, calambur.</ul>
                    <ul onclick="location.href=''">El signo lingüístico: Significante y significado. Uso del asterisco.</ul>
                    <ul onclick="location.href=''">Los textos orales: La entrevista.</ul>
                    <ul onclick="location.href=''">El currículum vitae.</ul>
                </li>               
                <li onclick="location.href=''">
                    <text class="h6">Unidad 3: Literatura americana: Romanticismo y Modernismo</text>
                    <ul onclick="location.href=''">El romanticismo americano.</ul>
                    <ul onclick="location.href=''">El Modernismo literario en América.</ul>
                    <ul onclick="location.href=''">figuras literarias: exclamación, interrogación retórica, apóstrofe, optación.</ul>
                    <ul onclick="location.href=''">uso de homófonos y heterótrofos con h.</ul>
                    <ul onclick="location.href=''">El reporte.</ul>
                    <ul onclick="location.href=''">El acta de reunión.</ul>
                </li>
                <li onclick="location.href=''">
                    <text class="h6">Unidad 4: Literatura americana: El realismo regionalista, crítico y social.</text>
                    <ul onclick="location.href=''">El realismo. Contexto socio histórico.</ul>
                    <ul onclick="location.href=''">Figuras literarias lógicas: Máxima y refrán.</ul>
                    <ul onclick="location.href=''">Oposiciones fonológicas y rasgos distintivos.</ul>
                    <ul onclick="location.href=''">Uso de "tan poco" y "tampoco", "tan bien" y "también, "dónde", "adonde", "adónde".</ul>
                    <ul onclick="location.href=''">Los textos periodísticos (la entrevista: Finalidad, característica).</ul>
                </li>
                <li onclick="location.href=''">
                    <text class="h6">Unidad 5: Literatura del romanticismo.</text>
                    <ul onclick="location.href=''">Realismo mágico.</ul>
                    <ul onclick="location.href=''">El fenómeno del "boom" latinoamericano.</ul>
                    <ul onclick="location.href=''">Figuras literarias lógicas antítesis: epifonema, Símil, antítesis.</ul>
                    <ul onclick="location.href=''">Las consonantes (su clasificación).</ul>
                    <ul onclick="location.href=''">Palabras de difícil escritura: Palabras compuestas.</ul>
                    <ul onclick="location.href=''">Producción y comprensión de textos orales.</ul>
                    <ul onclick="location.href=''">Estrategias para la comprensión de textos orales.</ul>
                </li>
                <li onclick="location.href=''">
                    <text class="h6">Unidad 6: Literatura de la vanguardia latinoamericana.</text>
                    <ul onclick="location.href=''">La Vanguardia en Latinoamérica.</ul>
                    <ul onclick="location.href=''">Teatro vanguardista latinoamericano.</ul>
                    <ul onclick="location.href=''">Figuras literarias de intención: Perífrasis, alusión, eufemismo, reticencia.</ul>
                    <ul onclick="location.href=''">Las vocales (clasificación).</ul>
                    <ul onclick="location.href=''">Uso de comillas y paréntesis.</ul>
                    <ul onclick="location.href=''">Norma lingüística y variedad estándar de la lengua.</ul>
                    <ul onclick="location.href=''">El debate y la mediación.</ul>
                </li>
                <li onclick="location.href=''">
                    <text class="h6">Unidad 7: Literatura salvadoreña: Romanticismo y costumbrismo.</text>
                    <ul onclick="location.href=''">Función metalingüística y referencial del texto literario.</ul>
                    <ul onclick="location.href=''">La literatura salvadoreña.</ul>
                    <ul onclick="location.href=''">Figuras literarias: Ironía, sarcasmo (parábola, símbolo).</ul>
                    <ul onclick="location.href=''">Rasgos suprasegmentales y puntos suspensivos.</ul>
                    <ul onclick="location.href=''">Los tipos de lectura: Intensiva, extensiva y recreativa. Información visual y no visual.</ul>
                    <ul onclick="location.href=''">La lectura, su planificación y autovaloración de lector. El análisis del discurso publicitario.</ul>
                </li>
                <li onclick="location.href=''">
                    <text class="h6">Unidad 8: Literatura salvadoreña contemporánea.</text>
                    <ul onclick="location.href=''">El sujeto lírico y su función.</ul>
                    <ul onclick="location.href=''">Generaciones literarias del siglo XX en El Salvador.
                        <li onclick="location.href=''">1 generación de 1944.</li> 
                        <li onclick="location.href=''">2 la generación comprometida.</li> 
                        <li onclick="location.href=''">3 escritores de la actualidad.</li> 
                    </ul>
                    <ul onclick="location.href=''">Figuras literarias: Oxímoron, descripción.</ul>
                    <ul onclick="location.href=''">Uso de los hiatos.</ul>
                    <ul onclick="location.href=''">El párrafo de enumeración y de causa-efecto.</ul>
                </li>
            </ul>
        </div>
        
        <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
        <script src="bootstrap/js/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>