<?php
  $consulta=ConsultarProducto($_GET['id']);

  function ConsultarProducto( $id_users)
  {
   require_once '../models/conexion.php';
   $stmt="SELECT * FROM usuarios WHERE id='".$id_users."' ";
   $resultado= $mysqli->query($stmt) or die ("Error al consultar producto".mysqli_error($mysqli)); 
   $fila=$resultado->fetch_assoc();

   return [
    $fila['usuario'],
    $fila['nombre'],
    $fila['correo'],
    $fila['password']
    
   ];
  }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Modificar Usuario</title>
        <style type="text/css">
            @import url("css/mycss.css");
        </style>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" type="text/css">
        <link href="bootstrap-fileinput-master/css/fileinput.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/estilo_registro_modificar.css">
        <script src="bootstrap-fileinput-master/js/fileinput.min.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </head>
    <body style="background-color: #f4f1de">
        <div id="cols" class="col-12 align-middle">
            <img id="sepaes2-1" class="float-left" src="css/imagenes/sepaes2.png" onclick="location.href='users.php'">
            <button class="btn btn-dark btn-outline-warning float-right mr-5 mt-2" onclick="location.href='../controllers/users.php'">Cancelar</button>
        </div><br><br>
        <div class="container" id="cont-principal">
            <div id="signupbox" style="margin-top:50px" class="row justify-content-center">
                <div class="col-12 col-md-6 bg-white rounded">
                    <div class="container">
                        <div class="panel-body">
                            <div class="h2" id="lbl-user">Modificar Usuario</div>
                            <br>
                            <form action="../controllers/modificar2.php" class="form-horizontal justify-content-center" method="POST" style="border-collapse: separate; border-spacing: 10px 5px;" enctype="multipart/form-data">
                                <input type="hidden" name="id"  value="<?php echo $_GET['id']?>">
                                    
                                <label class="control-label">Usuario: </label>
                                <input type="text" id="usuario" name="usuario" value="<?php echo $consulta[0] ?>" class="form-control" placeholder="Usuario" id="textbox"><br>

                                <label class="control-label">Nombre: </label>
                                <input type="text" id="nombre" name="nombre" value="<?php echo $consulta[1] ?>" class="form-control" placeholder="Nombre" id="textbox"><br>

                                <label class="control-label">Correo: </label>
                                <input type="text" id="correo" name="correo" value="<?php echo $consulta[2] ?>" class="form-control" placeholder="Email" id="textbox"><br>
                                <label>Contraseña actual: </label>
                                <input type="password" id="password" name="password" value="<?php echo $consulta[3] ?>" class="form-control" placeholder="password" id="textbox"><br><br>
                                <label>Foto de perfil: </label><br>
                                <input type="file" name="file1" id="file1"><br><br>
                                <button type="submit" id="btn-signup1" class="btn btn-info">Guardar</button>
                            </form><br><br>
                        </div>	
                    </div>
                </div>
             </div>
        </div>
        <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
        <script src="bootstrap/js/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script>
            $("#file-1").fileinput({
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any"
            });
        </script>
    </body>
</html>