<?php
    //Aqui va el código PHP del Vídeo
session_start();
    require '../models/conexion.php';
    include '../controllers/funcs/funcs.php';
    
    if(!isset($_SESSION["id_usuario"]) AND $_SESSION['id_usuario'] ){ //Si no ha iniciado sesión redirecciona a index.php
        header("Location: index.php");
    }
    
    $idUsuario = $_SESSION['id_usuario'];
    
    $sql = "SELECT id, usuario,nombre, correo, logo_url FROM usuarios WHERE id = '$idUsuario'";
    $result = $mysqli->query($sql);
    
    $row = $result->fetch_assoc();
?>
<html>
    <head>
        
    <title>Perfil</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/estilo_perfil.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css" />
        
    </head>

    <body style="background-color: #f4f1de">
    
    <div  id="cols" class="col-12">
    <div id="col-img" class="col-3">
           <button onclick="location.href='elegir_materia.php'" class="btn btn-primary"><img src="css/imagenes/back.png" id="imgBack"> <a href="elegir_materia.php" id="frback" >Materias</a></button>
    </div>
    <div id="col-saludo" class="col-6">
            <h3><b><font color='white'><?php echo  '¡Hola, '  .utf8_decode($row['nombre'])."!"; ?></font></b></h3> 
    </div>
    <div id="col-btns" class="margin-left">
            
            <a id="" class="btn btn-primary" href="ranking.html">Ranking<span class="sr-only">(current)</span></a>
      		<a id="" class="btn btn-primary" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a>            
                      
   
    </div>
            
    </div>
           
    <div class="modal-dialog text-center" >
    <div class="row">
    <form method="post" id="perfil">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 toppad" >
   
   
    <div class="panel panel-success"><br>
   
    <br><br>
    <div class="panel-body">
    <div class="row">
              
    <div class="col-sm-3 col-md-5 " align="center"> 
    <div id="load_img">
    <img class="img-responsive" src="<?php echo $row['logo_url'];?>"  width="200" high="200" alt="Logo">          
    </div>
               
  
    </div>

    <div class="col-sm-3 col-md-6 col-lg-4"> 
    <table class="table table-condensed"  >
    <tbody >
    <tr>
    <td class='col-md-3'>Usuario:</td>
    <td><b><font color='black'><?php echo ($row['usuario']); ?></font></b></td>
    </tr>
    <tr>
    <td>Nombre:</td>
    <td><b><font color='black'><?php echo ($row['nombre']); ?></font></b></td>
    </tr>
    <tr>
    <td>Correo:</td>
    <td><b><font color='black'><?php echo ($row['correo']); ?></font></b></td>
    </tr>          
    </tbody>
    </table>
                     
    </div>
   
    </div>
    </div>
  
    </div>
    </div>
    </form>
     
    </div>
    </div>
    <div  class='col-md-12'></div>
    <div class="panel-footer text-center">                      
    
    </div>


 <?php
       require '../models/conexion.php';
        $stmt="SELECT * FROM usuarios WHERE id = '$idUsuario'";
        $resultado= $mysqli->query($stmt) or die (mysqli_error($mysqli));
        while($fila=$resultado->fetch_assoc())
        {
            echo "<td><a href='modifica.php?id=".$fila['id']."'> <button type='button' class='btn btn-success'>Clic para editar perfil</button> </a></td>";
        }
      ?>

</body>
</html>






        






