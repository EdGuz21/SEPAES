<!DOCTYPE html>
<html>
	<head>
		<title>Apartado de Estudio</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	</head>
	<body>
        <div class="container cont-barra col-12 col-md-12 col-sm-12">
             <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='elegir_materia.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Materias</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <text class="h2 text-white">Lenguaje y Literatura</text>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container cont-btn col-12 col-md-12 col-sm-12">
            <table class="table">
                <th class="btnU col-3 col-md-3 col-sm-3 " onclick="location.href='unidad_1.php'">
                    <img class="imgU" src="css/imagenes/l1.png " onclick="location.href='unidad_1.php'">
                </th>
                <th class="btnU col-3 col-md-3 col-sm-3 " onclick="location.href='unidad_2.php'">
                <img class="imgU" src="css/imagenes/l2.png " onclick="location.href='unidad_2.php'">
                </th>
                <th class="btnU col-3 col-md-3 col-sm-3 " onclick="location.href='unidad_3.php'">
                    <img class="imgU" src="css/imagenes/l3.png " onclick="location.href='unidad_3.php'">
                </th>
                <th class="btnU col-3 col-md-3 col-sm-3 " onclick="location.href='unidad_4.php'">
                    <img class="imgU" src="css/imagenes/l4.png " onclick="location.href='unidad_4.php'">
                </th>
                <th class="btnU col-3 col-md-3 col-sm-3 " onclick="location.href='unidad_5.php'">
                    <img class="imgU" src="css/imagenes/l5.png " onclick="location.href='unidad_5.php'">
                </th>
                <th class="btnU col-3 col-md-3 col-sm-3 " onclick="location.href='unidad_6.php'">
                    <img class="imgU" src="css/imagenes/l6.png " onclick="location.href='unidad_6.php'">
                </th>
            </table>
        </div>
        <header class="container col-10 col-md-10 col-sm-10">
            <div class="menu">
                <ul>
                    <li class=""><a href="unidad_1.php">Unidad 1 <img class="img-btn" src="css/imagenes/l1.png"></a></li>
                    <li class=""><a href="unidad_2.php">Unidad 2 <img class="img-btn" src="css/imagenes/l2.png"></a></li>
                    <li class=""><a href="unidad_3.php">Unidad 3 <img class="img-btn" src="css/imagenes/l3.png"></a></li>
                    <li class=""><a href="unidad_4.php">Unidad 4 <img class="img-btn" src="css/imagenes/l4.png"></a></li>
                    <li class=""><a href="unidad_5.php">Unidad 5 <img class="img-btn" src="css/imagenes/l5.png"></a></li>
                    <li class=""><a href="unidad_6.php">Unidad 6 <img class="img-btn" src="css/imagenes/l6.png"></a></li>
                </ul>
            </div>
        </header>

        <script src="menu.js"></script>
        <script src="bootstrap/css/bootstrap.min.css"></script>
        <script src="bootstrap/js/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>