<?php
    require_once '../models/conexion.php';
    
    $sQuery = mysqli_query($mysqli,"SELECT usuarios.nombre, test.nota FROM test,usuarios WHERE test.id = usuarios.id ORDER BY nota DESC");
    $tests = mysqli_fetch_array($sQuery);
    
    $usuario=array();
    $nota=array();
    foreach($tests as $usuarios => $notas)
    {
        $usuario=$usuarios;
        $nota=$notas;
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ranking</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="css/ranking.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>
<body class="">
    <div class="container">
        <div class="row">
            <div class="container col-12 col-md-12 col-sm-12 cont-barra"></div>
            <div class="container col-12 col-md-12 col-sm-12 cont-rank">
                <div class="swiper-container bg-dark">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div id="first">
                            <div class="crownBox"><img src="css/imagenes/medalla-de-oro%20(1).png"></div>
                            <div class="userBox"><h4><?php echo $usuarios[0]; ?></h4></div>
                        </div>
                        <div class="imgBox"><img src="css/imagenes/user.png"></div>
                        <div class="noteBox"><h5>Nota: <?php echo $notas[0]; ?></h5></div>
                    </div>
                    <div class="swiper-slide">
                        <div id="second">
                            <div class="crownBox"><img src="css/imagenes/segundo%20(1).png"></div>
                            <div class="userBox"><h4><?php echo $usuarios[1]; ?></h4></div>
                        </div>
                        <div class="imgBox"><img src="css/imagenes/user.png"></div>
                        <div class="noteBox"><h5>Nota: <?php echo $notas[1]; ?></h5></div>
                    </div>
                    <div class="swiper-slide">
                        <div id="third">
                            <div class="crownBox"><img src="css/imagenes/tercero.png"></div>
                            <div class="userBox"><h4><?php echo $usuarios[2]; ?></h4></div>
                        </div>
                        <div class="imgBox"><img src="css/imagenes/user.png"></div>
                        <div class="noteBox"><h5>Nota: <?php echo $notas[2];?></h5></div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                    
                </div>
            </div>
            <div class="container col-12 col-md-12 col-sm-12 cont-rankall">
                <table class="table">
                    <?php
                        echo '<thead>';
                        echo '<tr>';
                        echo '<th>Nombre</th>';
                        echo '<th>Nota</th>';
                        echo '</tr>';
                        echo '</thead>';
                        echo '<tbody>';
                        foreach($usuarios as $valor){
                            echo '<tr>';
                            echo '<th>'.$valor.'</th>';
                        }
                        foreach($notas as $valor){
                            echo '<th>'.$valor.'</th>';
                            echo '<tr>';
                        }
                        echo '</tbody>';
                    ?>
                </table>
            </div>
            </div>
        </div>  
    </div>
    
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../controllers/JavaScript/swiper.min.js"></script>
    <script type="text/javascript" src="../controllers/JS/rank.js"></script>
</body>
</html>