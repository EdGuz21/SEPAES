<?php
	require_once '../models/conexion.php';
	require_once '../controllers/funcs/funcs.php';
	
	session_start();
	
	if(isset($_SESSION["id_usuario"])){
		header("Location: elegir_materia.php");
	}
	
	
	
?>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">   
    <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css" />
    <link rel="stylesheet" href="AlertifyJS/css/themes/semantic.min.css" />
    <link rel="stylesheet" type="text/css" href="css/recuperacion.css">
		<title>Recuperar Contraseña</title>
		</head>
     <body>
         <script src="AlertifyJS/alertify.min.js"></script>
         <script type="text/javascript" src="JS/alertas.js"></script>
         <?php require_once '../controllers/restore.php'?>
    
        <div class="modal-dialog text-center">
        	<div class="col-sm-12 main-section">
        		<div class="modal-content">
        			<div class="col-12">
        				<h3>Reestablecer contraseña</h3>
        				<hr>
        				<p>Para reestablecer la contraseña, por favor ingrese la dirección de correo electrónico asosiada a su nombre de usuario</p>

        			</div>
        			
        				<form class="col-12" id="loginform"  role="form" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" autocomplete="off">
        					<div class="form-group">
        						<input class="form-control" type="email" name="email" placeholder="Correo Electrónico" id="txtemail">
        					</div>
             			<input class="btn btn-primary" type="submit" name="entrar" value="Enviar Correo Electrónico" id="btnenviar">
             			<hr>
             			<label for="usuario" id="">Regresar a <a href="../index.php" >Inicio de Sesi&oacute;n</a></label>
             			</form>
        		</div>
        	</div>
        </div>

        <div id="notis" class="col-12">
                    <div ></div>
                    <div class="col-4" id="noti-no-user">
                        <?php
                        if (isset($error)) {
                            echo $no_user;
                         } 
                        
                        ?>
                    </div> 
                </div>
  
										
	<script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>							