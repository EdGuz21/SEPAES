<?php
	require_once '../models/conexion.php';
	require_once '../controllers/funcs/funcs.php';
	require_once '../controllers/newRegister.php';
?>
<html>
	<head>
		<title>Registro</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/estilo_registro_modificar.css">
	</head>
	
	<body style="background-color: #f4f1de">
        <div  id="cols" class="col-12">
            <img id="sepaes2-1" class="float-left" src="css/imagenes/sepaes2.png" onclick="location.href='index.php'">
            <div id="title-welcome" class="h1">Bienvenido</div>
        </div><br><br>
            
        <?php echo resultBlock($errors); ?>
        <div class="container" id="cont-principal">
            <div id="signupbox" style="margin-top:50px" class="row justify-content-center">
                <div class="col-12 col-md-6 bg-white justify-content-center">
					
                    <div class="panel-body justify-content-left"> <br>
                        <h2 id="lbl-user">Nuevo Usuario</h2><br><br>
                        <form id="signupform" class="form-horizontal" role="form" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" autocomplete="off">
							
                            <div id="signupalert" style="display:none" class="alert alert-danger">
                                <p>Error:</p>
                                <span></span>
                            </div>
							
                            <div class="form-group">
                                <label for="nombre" class="col-md-3 control-label" id="lbl">Nombre:</label>
                                <div class="col-md-9" id="textbox">
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?php if(isset($nombre)) echo $nombre; ?>" required >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="usuario" class="col-md-3 control-label" id="lbl">Usuario:</label>
                                <div class="col-md-9" id="textbox">
                                    <input type="text" class="form-control" name="usuario" placeholder="Usuario" value="<?php if(isset($usuario)) echo $usuario; ?>" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="col-md-3 control-label" id="lbl">Contraseña:</label>
                                <div class="col-md-9" id="textbox">
                                    <input type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                </div>
                            </div>
							
                            <div class="form-group">
                                <label for="con_password" class="col-md-6 control-label" id="lbl">Confirmar Contraseña:</label>
                                <div class="col-md-9" id="textbox">
                                    <input type="password" class="form-control" name="con_password" placeholder="Confirmar Contraseña" required>
                                </div>
                            </div>
							
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label" id="lbl">Email:</label>
                                <div class="col-md-9" id="textbox">
                                    <input type="email" class="form-control" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>" required>
                                </div>
                            </div>
							
                            <div class="form-group">                                      
                                <div class="col-md-offset-3 col-md-9">
                                    <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Registrar</button>
                                    <button class="btn btn-info" onclick="location.href='../users.php'">Cancelar</button>
                                </div> <br>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
		<script src="bootstrap/js/jquery-3.4.1.min.js"></script>
        <script src="bootstrap/js/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>															