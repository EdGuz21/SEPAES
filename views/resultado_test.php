<!doctype html>
<?php  
 require_once  '../controllers/funciones-testL/funciones-test.php';
 require_once '../controllers/sesion.php';
 include '../controllers/noCache.php';
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Resultado del Test</title>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilo_resultado_test.css" type="text/css">
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
        <script src="../controllers/JS/noBack.js" type="text/javascript"></script>
    </head>
    
    <body onload="nobackbutton()">
        <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button class="logo btn btn-info" onclick="location.href='elegir_materia.php'"><i class="fas fa-book"></i>Materias</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <h4>Resultado de test</h4>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="container cont-results">
                <div id="div-result">
                    <?php echo "". $resultadoTest= verificarRespuestas();?>
                </div>
                <div class="cont-respuestas">
                    <br><br><br>
                    <table class="table table-sm table-bordered bg-white top-50">
                        <thead class="bg-success">
                            <tr>
                                <th scope="col">Pregunta</th>
                                <th scope="col">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach(estado() as $pregunta => $estado){
                                echo "<tr>";
                                echo "<td>$pregunta</td>";
                                echo "<td>$estado</td>";
                                echo "<tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="" class="cont-detalle container text-center">
                <text class="h3">Respuestas correctas: </text>
                <text class="h3 text-success"><?php echo $respuestas= (verificarRespuestas()*10)/4;?></text><br>
                <text class="h3">Respuestas incorrectas: </text>
                <text class="h3 text-danger"><?php echo $respuestasI= 25-((verificarRespuestas()*10)/4);?></text>
            </div>
            <div class="main">
                <fieldset>
                    <div  class="sugerencias">
                        <center>
                            <h3>Competencias logradas</h3>
                            <?php
                                foreach (estado() as $pregunta => $estado) {
                                    if ($estado == "Correcta") {
                                        echo 'Comprensión lectora<br>';
                                    }
                                }
                            ?>
                        </center>
                    </div>
                    <div  class="material_recomendado">
                        <center>
                            <h3>Competencias a mejorar</h3>
                            <?php
                                foreach (estado() as $pregunta => $estado) {
                                    if ($estado == "Incorrecta") {
                                        echo 'Comprensión lectora<br>';
                                    }

                                }
                            ?>
                        </center>
                    </div>
                    <div class="temas_mejorar">
                        <center>
                            <h3>Material recomendado</h3>
                            <?php
                                foreach (estado() as $pregunta => $estado) {
                                    if ($estado == "Incorrecta") {
                                        echo 'Obras literarias<br>';
                                    }

                                }
                            ?>
                        </center>
                    </div>
                </fieldset>
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </div>
        
        <script src="views/bootstrap/js/jquery-3.4.1.min.js"></script>
        <script src="views/bootstrap/js/popper.min.js"></script>
        <script src="views/bootstrap/js/bootstrap.min.js"></script>
        <script src="../controllers/JS/noBack.js" type="text/javascript"></script>
    </body>
</html>