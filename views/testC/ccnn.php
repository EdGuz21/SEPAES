<div id="contenedor-lenguaje">
    <div id="contenedor-cuerpo">
        <div id="div-cuerpo"> <br>
        <div class="textos">            
            <div class="titulos">
                Observa y analiza las estructuras moleculares que componen las dos sustancias que se presentan.  
            </div>
            <div class="cuerpo-textos">
                <img src="css/imagenes/ccnn/imagen_pregunta1_ccnn.png" id="imgpregunta1_ccnn">
            </div>
        </div>
                        
        <div class="pregunta">
            <div class="numero-padre">
                <p class="numero-hijo">1</p>
            </div> 
            <div class="pregunta-padre">
                <div class="contenido-pregunta">
                    ¿Cuál de las siguientes afirmaciones es correcta?
                </div>
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                    <div class="opcion-respuesta">
                        El amoníaco y el oxígeno están constituidos por moléculas compuestas que tienen más de un átomo
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                    <div class="opcion-respuesta">
                        El oxígeno se compone de dos moléculas iguales y el amoníaco de cuatro átomos iguales.
                        <br><br>
                    </div> 
                </div>
                            
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"><div class="literal-hijo">C</div></div>
                    <div class="opcion-respuesta">
                        El amoníaco y el oxígeno están compuestos por moléculas simples que contienen dos elementos. 
                        <br><br> 
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"><div class="literal-hijo">D</div></div>
                    <div class="opcion-respuesta">
                        El oxígeno se compone por una molécula simple de un elemento y el amoníaco por una molécula compuesta de dos        elementos.
                    </div> 
                </div>
            </div>     
        </div>
    </div>
        <div id="div-cuerpo"> <br>
        <div class="pregunta">
            <div class="numero-padre">
                <p class="numero-hijo">2</p>
            </div> 
            <div class="pregunta-padre">
                <div class="contenido-pregunta">
                    El propano es un hidrocarburo, utilizado en los hogares y tiene las siguientes propiedades:
                </div>
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                    <div class="opcion-respuesta">
                        es incoloro, está compuesto de carbono e hidrógeno y es utilizado como combustible.
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                    <div class="opcion-respuesta">
                        es incompresible, tiene brillo metálico, es utilizado como conductor de electricidad.
                    </div> 
                </div>
                            
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"><div class="literal-hijo">C</div></div>
                    <div class="opcion-respuesta">
                        es resistente a temperaturas altas, por lo general no arde, es buen conductor del calor. 
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"><div class="literal-hijo">D</div></div>
                    <div class="opcion-respuesta">
                        forma estructuras cristalinas, se ioniza y conduce la electricidad, tiene reacción rápida.
                    </div> 
                </div>
            </div>     
        </div>
    </div>
        <div id="div-cuerpo"> <br>
        <div class="textos">
            <div class="titulos">
                Indicación: Lee el siguiente texto y responde el ítem. 
            </div>
            <div class="cuerpo-textos">
                <div id="texto">
                    <h4>¿Qué es el fuego?</h4>
                    <p>En el fenómeno de quemar un pedazo de papel ocurre una reacción química en el que moléculas que forman el papel se   combinan con el oxígeno convirtiéndose en ceniza, gases como dióxido de carbono y vapor de agua. Esta reacción libera   energía en forma de calor, a lo que llamamos fuego. </p>
                    <p>Una ecuación química que puede describir este fenómeno es:</p>
                    <p id="paraph">C6H10O5(s) + 6O2(g) <img src="css/imagenes/ccnn/flecha.png" id="imgflecha"> 6CO2(g) + 5H2O(g) + ∆</p>
                </div>
                <div id="imagen">
                    <img src="css/imagenes/ccnn/imagen_preguntasfuego.png" id="imgpregunta-fuego">
                </div>
            </div>                 
        </div>
            
        <div class="pregunta">
            <div class="numero-padre">
                <p class="numero-hijo">3</p>
            </div> 
            <div class="pregunta-padre">
                <div class="contenido-pregunta">
                    A partir de la lectura, ¿cómo definirías una reacción química?
                </div>
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                    <div class="opcion-respuesta">
                        Es una representación que utiliza una serie de símbolos para identificar una sustancia.
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                    <div class="opcion-respuesta">
                        Es la descripción simbólica de la cantidad de elementos que contienen las sustancias.
                    </div> 
                </div>
                
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"><div class="literal-hijo">C</div></div>
                    <div class="opcion-respuesta">
                        Es el proceso en que una o dos sustancias se transforman en otras sustancias nuevas. 
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"><div class="literal-hijo">D</div></div>
                    <div class="opcion-respuesta">
                        Es la conversión de la materia donde se crean los átomos que forman las sustancias. 
                    </div> 
                </div>
            </div>
        </div>
    </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="titulos">
                    Observa el siguiente experimento donde se estima la capacidad de un recipiente con pelotas, obteniendo 16 pelotas como  medida.  
                </div>
                <div class="cuerpo-textos">
                    <img src="css/imagenes/ccnn/imagen_pregunta4_ccnn.png" id="imgpregunta1_ccnn">
                </div>
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">4</p>
                </div>
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        ¿Cuál de las siguientes afirmaciones es correcta?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            La masa que contienen las pelotas es la magnitud física que se midió.
                            <br><br>
                        </div> 
                    </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                    <div class="opcion-respuesta">
                                        Las pelotas son el patrón de medición e indican la unidad de la medida.
                                </div>  
                            </div>
                            
                                <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    La unidad utilizada es el recipiente e indica el tamaño de la magnitud. 
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    El recipiente es la magnitud física que se midió en este experimento.
                                </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="titulos">
                    Observa y analiza la masa de los siguientes pasteles:  
                </div>
                <div class="cuerpo-textos">
                    <img src="css/imagenes/ccnn/imagen_pregunta5_ccnn.png" id="imgpregunta1_ccnn">
                </div>
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">5</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        Respecto a la masa de los pasteles se puede concluir que:
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Las unidades de medida en los pasteles indican que tienen diferente cantidad de masa.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            Los pasteles tienen diferente forma, por consiguiente, tienen diferente cantidad de masa.
                        </div> 
                    </div>
                            
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            Los pasteles tienen la misma cantidad de masa, pero con diferentes unidades de medida. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            La cantidad de masa en los pasteles es igual, ya que la medida se expresa en gramos.
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
        <div class="textos">
            <div class="titulos">
                Indicación: Lee el siguiente texto y responde el ítem. 
            </div>
            <div class="cuerpo-textos">
                <h4>¡Un asteroide roza la Tierra!</h4>
                <p>En el año 2012 un asteroide pasó a una distancia de 230 000 km de la Tierra, lo que significa que este cuerpo rocoso ha  pasado más cerca de la Tierra que la distancia a la que la Luna realiza su órbita, que es de 382 000 km. Si un objeto se acerca a la Tierra puede verse afectado por la gravedad del planeta, por ello parte de la comunidad científica se encarga de vigilar con mucha atención aquellos objetos que se acercan a nuestro planeta a una distancia que represente peligro.</p>
            </div>                 
        </div>

        <div class="pregunta">
            <div class="numero-padre">
                <p class="numero-hijo">6</p>
            </div> 
            <div class="pregunta-padre">
                <div class="contenido-pregunta">
                    La gravedad de la Tierra puede influir sobre el asteroide ya que:
                </div>
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                    <div class="opcion-respuesta">
                        Es la aceleración que provocaría mayor atracción entre la Tierra y el asteroide debido a la corta distancia que habría  entre ellos.
                        <br><br>
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                    <div class="opcion-respuesta">
                        Es una fuerza de atracción que actuaría sobre la masa del asteroide, atrayéndolo hacia la Tierra y aumentaría si hay mayor cercanía entre los dos cuerpos.
                    </div> 
                </div>

                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"><div class="literal-hijo">C</div></div>
                    <div class="opcion-respuesta">
                        Es una fuerza de acción a distancia que podría atraer o repeler los cuerpos que se acerquen demasiado al planeta Tierra. 
                    </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"><div class="literal-hijo">D</div></div>
                    <div class="opcion-respuesta">
                        Es la aceleración que permitiría que los cuerpos que pasen cerca de la Tierra aumenten su velocidad debido a la     interacción entre sus masas. 
                    </div> 
                </div>
            </div>
        </div>
    </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="titulos">
                    Observa y analiza la siguiente ilustración y responde el ítem  
                </div>
                <div class="cuerpo-textos">
                    <img src="css/imagenes/ccnn/imagen_pregunta7_ccnn.png" id="imgpregunta1_ccnn">
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">7</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        En qué se fundamentó la investigación de esta prueba de la evolución.
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            En el estudio comparado del desarrollo embrionario de distintos seres vivos.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            En las pruebas basadas en las semejanzas anatómicas de distintas especies.
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            En el estudio de las semejanzas y diferencias entre los diferentes embriones. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            En la transformación de una especie después de la fase embrionaria.
                            <br><br>
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                           
                <div class="titulos">
                    Observa detenidamente el ciclo del carbono y responde el ítem.  
                </div>
                <div class="cuerpo-textos">
                    <img src="css/imagenes/ccnn/imagen_pregunta8_ccnn.png" id="imgpregunta1_ccnn">
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">8</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        ¿Qué importancia tiene la función que cumplen los hongos en el ciclo del carbono?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Permiten que los animales durante el proceso de respiración liberen CO2, para utilizarlo por las plantas en la  fotosíntesis.
                                
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            Intervienen en la desintegración de los restos de plantas, animales muertos y excretas, regresando el carbono a la  tierra.
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            Facilitan la descomposición del carbono orgánico para ser utilizado en el proceso de respiración. 
                            <br><br>
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Actúan desintegrando el CO2 atmosférico, para ser transformado en materia orgánica que será utilizada por los seres vivos.
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
        <div class="pregunta">
            <div class="numero-padre">
                <p class="numero-hijo">9</p>
            </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        <p>Sembrar árboles es una tarea que todos debemos practicar, pues la deforestación en el país es cada vez mayor. Ante   esta problemática una investigación científica propone plantar 100,000 árboles en una hora usando drones.</p> 
                        <p>Si se necesitan 60, 000,000 de árboles para cubrir aproximadamente el 70% de la superficie deforestada de El     Salvador, calcula el tiempo que llevaría a estos drones reforestar el país.</p>
                        
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            18 días. 
                            <br><br>
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            25 días.
                            <br><br>
                        </div> 
                    </div>
                            
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            420 días.
                            <br><br> 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            600 días.
                            <br><br>
                        </div> 
                    </div>
            </div>     
        </div>
    </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="titulos">
                    Lee el siguiente texto, observa la imagen y responde el ítem  
                </div>
                <div class="cuerpo-textos">
                    <h4>¡Cuidado con tu cuello!</h4>
                    <p>La columna soporta el peso de la cabeza que es de 45 N, o sea 5 kg de masa;  pero cuando se inclina a un ángulo de 60° al usar un celular, los músculos del cuello ejercen una fuerza para mantener la cabeza en posición (ver imagen). Esto ocasiona que la cabeza pese más, pues al sumar estos vectores la columna vertebral soporta ahora una nueva fuerza de 265 N o sea 27 kg de masa… ¡Aproximadamente el peso de un niño de 6 años! </p>
                    <p class="citas">Tomado de Engaginscience (adaptación)</p>
                    <img src="css/imagenes/ccnn/imagen_pregunta10_ccnn.png" id="imgpregunta1_ccnn">
                    <h4 class="titulo_">Considere:</h4>
                    <p class="texto_">Vector <img src="css/imagenes/ccnn/vertor_PC.png" id="imgvectores"> : El peso de la cabeza.<br> 
                        Vector <img src="css/imagenes/ccnn/vertor_FC.png" id="imgvectores">: La Fuerza que ejercen los músculos del cuello.<br> 
                        Vector <img src="css/imagenes/ccnn/vertor_FSC.png" id="imgvertores">: La Fuerza que soporta la columna vertebral.</p>
                </div>  
            </div>
                                
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">10</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        ¿Qué gráfico representa correctamente la suma de los vectores PC y  FC para obtener la fuerza que soporta la columna vertebral?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            <img src="css/imagenes/ccnn/imagen_pregunta10_opcionA_ccnn.png" id="imgopcion">
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            <img src="css/imagenes/ccnn/imagen_pregunta10_opcionB_ccnn.png" id="imgopcion">
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            <img src="css/imagenes/ccnn/imagen_pregunta10_opcionC_ccnn.png" id="imgopcion">
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            <img src="css/imagenes/ccnn/imagen_pregunta10_opcionD_ccnn.png" id="imgopcion">
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="cuerpo-textos">
                    <p>Se realizó un experimento sobre electroimanes, utilizándose tres tornillos, alambre de cobre y tres baterías como se muestra en cada imagen. La maestra sugiere a sus estudiantes que a un tornillo le den 50 vueltas con el alambre, al otro 100 vueltas y al último 150 vueltas (ver    imagen). Al hacerlo se dan cuenta que los imanes atraen las cosas con diferente intensidad y los que tienen más alambre pueden atraer cosas más pesadas</p>
                    <img src="css/imagenes/ccnn/imagen_pregunta11_ccnn.png" id="imgpregunta1_ccnn">
                                
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">11</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        De acuerdo con las variaciones hechas en el experimento, ¿qué característica de los
                        electroimanes se está comprobando?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            La fuerza magnética relacionada con el número de espiras de los electroimanes. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            La dirección de las líneas del campo magnético generado por las espiras. 
                            <br><br>
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            El tiempo que tienen los electroimanes para conservar su magnetismo. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Los polos magnéticos norte-sur, que tienen los electroimanes.
                            <br><br>
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="cuerpo-textos">
                    <p class="texto">Analiza el esquema que presenta una demostración experimental sobre las propiedades de los gases, donde se     realiza  el siguiente proceso:</p>
                    <p>1.   Se mide el volumen de un gas contenido en la jeringa.</p>
                    <img src="css/imagenes/ccnn/imagen_pregunta12_1_ccnn.png" id="imgpregunta12_ccnn">
                        <p>2.   Luego se presiona el émbolo de la jeringa comprimiendo el gas dentro de ella.</p>
                    <img src="css/imagenes/ccnn/imagen_pregunta12_2_ccnn.png" id="imgpregunta12_ccnn">
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">12</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        Si la densidad es la relación de masa entre el volumen <img src="css/imagenes/ccnn/imagen_pregunta12_formula_ccnn.png" id="imgformula">, al comprimir el gas se puede afirmar que:
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            la masa del gas se mantiene constante pero su volumen ha disminuido, por lo tanto, su densidad disminuye.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            <br>
                            la masa del gas disminuye al igual que su volumen y por lo tanto, su densidad se mantiene constante.
                            <br> 
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            la masa del gas disminuye al igual que su volumen y por lo tanto, la densidad del gas también disminuye.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Los polos magnéticos norte-sur, que tienen los electroimanes.
                            <br><br>
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="cuerpo-textos">
                    <p>Milton realizó un experimento para observar la relación que existe entre la temperatura y la velocidad de reacción de    unas sustancias, para la actividad realizó los siguientes pasos:</p>
                    <p>1. Partió una tableta de Alka-seltzer en tres partes iguales y numeró tres tubos.<br>
                        2. A cada tubo de ensayo le agrego 5ml de agua a diferente temperatura y les colocó un tercio de Alka-seltzer a cada    uno.</p>
                    <h4>Observa en la siguiente tabla los resultados obtenidos:</h4>
                    <img src="css/imagenes/ccnn/imagen_pregunta13_tabla_ccnn.png" id="imgpregunta1_ccnn">
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">13</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        A partir de los resultados del experimento anterior y de la teoría de las colisiones entre partículas, Milton concluye  que el aumento de temperatura en las reacciones provoca:
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            que los choques de las moléculas disminuyan y se separen rápidamente, dejando inhabilitados a los átomos para   reaccionar.
                            
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            que las moléculas se muevan más rápido en menor cantidad de tiempo, pues el reactivo se agota rápidamente. 
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                                    que cambie la concentración de los reactivos involucrados en la reacción disminuyendo el movimiento de las  moléculas.
                        </div>  
                    </div>  
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            que disminuya la energía con la que se forman los nuevos enlaces, por lo que la reacción se realiza rápidamente.
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
        <div class="pregunta">
            <div class="numero-padre">
                <p class="numero-hijo">14</p>
            </div> 
            <div class="pregunta-padre">
                <div class="contenido-pregunta">
                    <p class="texto_">Se te presentan ejemplos de reacciones endotérmicas y exotérmicas.</p>
                    <p class="texto_">I. Proceso de explosión de la dinamita.<br> 
                        II. Cocción de un pastel en un horno.<br> 
                        III. Proceso de bronceado de la piel.<br> 
                        IV. Combustión del gas propano de las cocinas.</p>
                    Selecciona la clasificación correcta del tipo de reacción en cada par de fenómenos presentados.
                </div>
                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                    <div class="opcion-respuesta">
                        II y IV son reacciones exotérmicas que liberan energía al ambiente por el calor generado en ambos procesos.
                    </div> 
                </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            I y IV corresponden a reacciones endotérmicas porque en ambos procesos van ganando energía del ambiente en forma de calor.
                        </div> 
                </div>

                <div class="contenedor-respuestas-izq">
                    <div class="literal-padre"><div class="literal-hijo">C</div></div>
                    <div class="opcion-respuesta">
                        I y III son reacciones exotérmicas pues en ambas hay un aumento de temperatura provocado por el calor liberado. 
                        </div> 
                </div>
                <div class="contenedor-respuestas-der">
                    <div class="literal-padre"><div class="literal-hijo">D</div></div>
                    <div class="opcion-respuesta">
                        II y III corresponden a reacciones endotérmicas por la absorción de calor, provenientes del horno y del Sol     respectivamente. 
                    </div> 
                </div>
            </div>
        </div>
    </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="cuerpo-textos">
                    <p>Observa y analiza el siguiente experimento sobre el proceso de fotosíntesis, donde se presentan dos montajes, uno en     presencia de luz (A) y el otro en ausencia de luz o en la oscuridad (B).</p>
                    <img src="css/imagenes/ccnn/imagen_pregunta15_ccnn.png" id="imgpregunta1_ccnn">
                    <h4>Considera la siguiente reacción:</h4>
                    <p id="paraph">6CO2   +   12H2O <img src="css/imagenes/ccnn/imagen_flechaluz_ccnn.png" id="imgflechaluz"> C6H12O6   +   6H2O    +   6O2 </p>
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">15</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        ¿Qué se puede concluir respecto a los resultados del experimento?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            En presencia o ausencia de luz se realiza el proceso de fotosíntesis y se da la obtención de O2.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            La planta que está en la oscuridad libera H2O y la planta que está en presencia de luz libera CO2. 
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            La planta en presencia de luz libera O2 y la que está en la oscuridad no realiza el proceso fotosintético.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            La planta que está en presencia de luz libera CO2, y la planta que está en la oscuridad libera O2.
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                          
                <div class="cuerpo-textos">
                    <p>La siguiente imagen muestra el movimiento de una planta como respuesta ante un estímulo.</p>
                    <img src="css/imagenes/ccnn/imagen_pregunta16_ccnn.png" id="imgpregunta1_ccnn">
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">16</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        ¿A qué característica de los seres vivos corresponde el comportamiento de la planta?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Adaptación, debido a que la planta va creciendo según la forma que va adquiriendo el tallo para lograr curvarse.
                            
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            Metabolismo, porque la luz solar provoca los cambios del crecimiento por reacciones químicas. 
                            <br><br>
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            Crecimiento, ya que el desarrollo y respuesta natural de la planta es de forma vertical hacia arriba.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Irritabilidad, debido a que la planta crece y se orienta en dirección al estímulo de la luz que recibe.
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                    Lee el siguiente texto sobre los resultados de una investigación científica y responde el ítem.  
                </div>
                <h4>Un virus gigante «revive»</h4>
                <div class="cuerpo-textos">
                    <p>En pleno siglo XXI, La científica Chantal Albert descubrió el virus Pithovirus sibericum y divulgó
                    pruebas de laboratorio que muestran un virus cien veces más grande que la mayoría y que puede infectar unos     organismos llamados amebas, pero es incapaz de alojarse en organismos que tienen más de una célula. <br>
                    La investigación ha sufrido críticas diciendo que, si se observó que los virus infectan a las amebas, existe riesgo de que pueda contagiar otros organismos e iniciarse una epidemia global.</p>
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">17</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        Según la investigación, ¿es posible que este virus cause una epidemia global? 
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Sí, ya que, según la investigación, las células humanas no tienen resistencia natural a los virus. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            No, ya que no supone un riesgo para personas y animales pues son organismos pluricelulares.
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            No, porque, aunque infecta a organismos con células parecidas a las del humano, es un virus de laboratorio. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            . Sí, porque este virus es más grande que los demás, por lo que podría infectar células humanas.
                            <br><br>
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                          
                        <div class="cuerpo-textos">
                            <p class="texto_1">El plan de protección de la tortuga marina en nuestro país delega a las autoridades a decomisar los huevos de tortuga a vendedores, debido a que juega un papel importante en el océano, como se observa en la siguiente red trófica.</p>
                            <img src="css/imagenes/ccnn/imagen_pregunta18_ccnn.png" id="imgpregunta1_ccnn">
                        </div>  
                    </div>
                        
                    <div class="pregunta">
                          <div class="numero-padre">
                            <p class="numero-hijo">18</p>
                        </div>
                        <div class="pregunta-padre">
                            <div class="contenido-pregunta">
                                Según la red trófica, ¿qué efecto tendría la extinción de las tortugas? 
                            </div>
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                                <div class="opcion-respuesta">
                                     Habría una sobrepoblación de medusas, causando desequilibrio del ecosistema marino.
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                <div class="opcion-respuesta">
                                    La población de medusas aumentaría, lo que ocasionaría que el zooplancton aumente.
                                </div> 
                            </div>
                            
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    Habría un aumento en la población de tiburones debido a la escases de tortugas.
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    La población de peces grandes aumentaría alterando toda la cadena alimenticia.
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
        <div id="div-cuerpo"> <br>
                    <div class="textos">
                      
                        <div class="cuerpo-textos">
                            <p class="texto_1">En El Salvador se generan alrededor de 3,400 toneladas de desechos sólidos por día; éstos son dispuestos en 14 rellenos sanitarios autorizados. La composición de la basura en el área metropolitana de San Salvador, se muestra en la siguiente gráfica:</p>
                            <img src="css/imagenes/ccnn/imagen_pregunta19_ccnn.png" id="imgpregunta1_ccnn">
                        </div>  
                    </div>
                        
                    <div class="pregunta">
                        <div class="numero-padre">
                            <p class="numero-hijo">19</p>
                        </div>
                        <div class="pregunta-padre">
                            <div class="contenido-pregunta">
                                Teniendo en cuenta la información de la gráfica, ¿qué medida debe implementarse para el tratamiento de la basura orgánica producida? 
                            </div>
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                                <div class="opcion-respuesta">
                                     Reciclarla pues todos los desechos mencionados en la gráfica pueden convertirse en nuevos productos.
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                <div class="opcion-respuesta">
                                    Depositarla en bolsas biodegradables ya que estas tienen la capacidad de descomponerse más rápido.
                                </div> 
                            </div>
                            
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    Separar todos los plásticos que se encuentran en la basura de las casas, de esta manera se podrían reutilizar. 
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    Enterrarla pues de esta manera se disminuye la cantidad de basura y se genera compostaje natural
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
        <div id="div-cuerpo"> <br>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">20</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            <p>Sergio y Paula se encuentran casados. Hace dos meses, Paula dio a luz un bebé y Sergio duda si es su hijo, por lo que Paula le propone que se realice una prueba de paternidad.</p> 
                            <p>¿Qué procedimiento debe llevarse a cabo en el laboratorio para la prueba de paternidad?</p>
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Tomar una muestra de saliva de la parte interna de la mejilla de Sergio y del bebé, para hacer un estudio del ADN, de cada muestra y demostrar la relación filial.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Extraer una pequeña muestra de sangre de Paula y del bebé para analizar los fragmentos de ADN, y verificar los resultados.
                                <br><br>
                            </div> 
                        </div>
                        
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Tomar una prueba de tejido bucal tanto de Sergio como del bebé, para determinar si las células del hijo son idénticas a las  del padre. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Realizar un examen de sangre de Paula y Sergio para analizar los fragmentos de ADN en cada muestra y comparar los resultados.
                            </div> 
                        </div>
                    </div>     
                </div>
            </div>
        <div id="div-cuerpo"> <br>
                    <div class="textos">
                       
                        <div class="titulos">
                            Lee y analiza el siguiente texto sobre El lago Suchitlán, y responde el ítem. 
                        </div>
                        <div class="cuerpo-textos">
                            El lago Suchitlán es un embalse, es decir un lago artificial que se formó sobre el cauce del río Lempa para la construcción de la central hidroeléctrica Cerrón Grande, éste produce la mayor cantidad de energía eléctrica del país. Posee una extensión de 135 km y una profundidad media de 30 m. Con la construcción del embalse se estima que fueron inundados más de 100 km2 de tierra dedicados a la agricultura y algunos sitios de interés arqueológicos.
                        </div>  
                    </div>
                        
                    <div class="pregunta">
                         <div class="numero-padre">
                            <p class="numero-hijo">21</p>
                        </div> 
                        <div class="pregunta-padre">
                            <div class="contenido-pregunta">
                                ¿Qué aspectos ambientales se deben evaluar al obtener energía eléctrica a través de centrales hidroeléctricas?
                            </div>
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                                <div class="opcion-respuesta">
                                     La cantidad de agua almacenada en el embalse del lago artificial para obtener mejor producción de energía.
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                <div class="opcion-respuesta">
                                    El balance de los impactos positivos y negativos en la obtención de energía, para buscar alternativas de desarrollo sostenible.
                                </div> 
                            </div>
                            
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    Los niveles de contaminación del agua del embalse, para garantizar el funcionamiento de la presa hidroeléctrica. 
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    La extensión y profundidad del embalse para determinar la cantidad de energía eléctrica que se logrará producir.
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
        <div id="div-cuerpo"> <br>
                    <div class="textos">
                      
                        <div class="cuerpo-textos">
                            <p class="texto_1">Las fuentes de energía son los recursos de los que se obtiene la energía utilizable para las actividades cotidianas. A continuación, se muestra un esquema de la obtención de energía en una presa hidroeléctrica</p>
                            <img src="css/imagenes/ccnn/imagen_pregunta22_ccnn.png" id="imgpregunta1_ccnn">
                        </div>  
                    </div>
                        
                    <div class="pregunta">
                          <div class="numero-padre">
                            <p class="numero-hijo">22</p>
                        </div>
                        <div class="pregunta-padre">
                            <div class="contenido-pregunta">
                                ¿Cuál es la fuente de la que se obtiene la energía que se genera en la central hidroeléctrica? 
                            </div>
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                                <div class="opcion-respuesta">
                                     El agua en movimiento en el cauce, debido a que genera la energía cinética que es aprovechada para la obtención de electricidad. 
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                <div class="opcion-respuesta">
                                    El generador eléctrico, ya que permite que la electricidad circule hasta los transformadores la cual es utilizada por medio de la red eléctrica.
                                </div> 
                            </div>
                            
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    La turbina, porque hace girar y pone en movimiento al generador eléctrico para que pueda producir la electricidad. 
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    La presa debido a que, al almacenar agua, provoca una acumulación de energía hidroeléctrica.
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
        <div id="div-cuerpo"> <br>
                    <div class="textos">
                       
                        <div class="titulos">
                           Lee y analiza la siguiente noticia y responde el ítem. 
                        </div>
                        <div class="cuerpo-textos">
                            <h4>Contaminación por plomo en suelos del cantón Sitio del Niño.</h4>
                            <p class="texto_">En agosto de 2010 se declaró estado de emergencia Ambiental, en el contorno de una fábrica de baterías en la zona del Cantón Sitio del Niño, municipio de San Juan Opico, debido a la contaminación por plomo detectada en la zona. De 2010 a 2012 se registraron 99 personas con más de 10 microgramos de plomo en la sangre. La peligrosidad del plomo reside en que no puede ser degradado químicamente ni biológicamente, provocando efectos en el ser humano como dolores crónicos, problemas sanguíneos, ansiedad, entre otros.</p>
                            <p class="citas">Informan en Sitio del Niño sobre emergencia ambiental (adaptación)</p>
                        </div>  
                    </div>
                        
                    <div class="pregunta">
                         <div class="numero-padre">
                            <p class="numero-hijo">23</p>
                        </div> 
                        <div class="pregunta-padre">
                            <div class="contenido-pregunta">
                                Según la noticia, la peligrosidad del plomo se debe a que es...
                            </div>
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                                <div class="opcion-respuesta">
                                     una molécula radiactiva que produce trastornos físicos en el ser humano.
                                    <br><br>
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                <div class="opcion-respuesta">
                                    una sustancia que al aumentar su volumen en la sangre se vuelve tóxica.
                                </div> 
                            </div>
                            
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    un compuesto creado de forma industrial para producir las baterías.
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    un elemento que por su composición química, las células no pueden eliminarlo.
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
        <div id="div-cuerpo"> <br>
                    <div class="textos">
                        
                        <div class="cuerpo-textos">
                            <p class="texto_1">Si los electrodomésticos no se desconectan del tomacorriente continúan consumiendo energía a pesar de estar apagados, a esto se le llama consumo fantasma. El termino kWh indica la energía consumida por un aparato que tiene una potencia de 1kW en una hora, en El Salvador 1 kWh cuesta $0.19</p>
                            <h4>Analiza el consumo fantasma de los siguientes electrodomésticos:</h4>
                            <img src="css/imagenes/ccnn/imagen_pregunta24_ccnn.png" id="imgpregunta24_ccnn">
                        </div>  
                    </div>
                        
                    <div class="pregunta">
                        <div class="numero-padre">
                            <p class="numero-hijo">24</p>
                        </div>
                        <div class="pregunta-padre">
                            <div class="contenido-pregunta">
                                Si una familia salvadoreña, decide desconectar los electrodomésticos enlistados en la tabla durante un año, calcula cuánto dinero aproximadamente, estará ahorrando en la factura de electricidad. 
                            </div>
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                                <div class="opcion-respuesta">
                                     $ 53
                                     <br><br>
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                                <div class="opcion-respuesta">
                                    $ 120
                                    <br><br>
                                </div> 
                            </div>
                            
                            <div class="contenedor-respuestas-izq">
                                <div class="literal-padre"><div class="literal-hijo">C</div></div>
                                <div class="opcion-respuesta">
                                    $ 277 
                                    <br><br>
                                </div> 
                            </div>
                            <div class="contenedor-respuestas-der">
                                <div class="literal-padre"><div class="literal-hijo">D</div></div>
                                <div class="opcion-respuesta">
                                    $ 300
                                    <br><br>
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                            
                <div class="titulos">
                    Lee el siguiente texto y responde el ítem. 
                </div>
                <div class="cuerpo-textos">
                    <p class="texto_">Un operario de una fábrica de papel, fue asignado a verificar las soldaduras en unos tubos de metal   utilizando el isótopo radiactivo iridio-192, es decir, un átomo del mismo elemento que es muy inestable y se desintegra     liberando energía en forma de radiación. Debido a la inexperiencia del trabajador, la cápsula de Iridio cayó al suelo y 13 horas después otro operario lo guardó en el bolsillo trasero de su pantalón. Los dos trabajadores sufrieron quemaduras en el muslo y manos, problemas de la vista e incluso se les prohibió tener hijos.</p>
                    <p class="citas">Adaptado de Grupo Editoria Editec, 2007</p>
                </div>  
            </div>
                            
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">25</p>
                </div> 
                <div class="pregunta-padre">
                    <div class="contenido-pregunta">
                        ¿Qué característica del isótopo provocó que el accidente en la planta de papel fuese peligroso?
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Tener una masa diferente al resto de isótopos de los elementos de la tabla periódica.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            Originarse de un átomo de iridio con características similares a las de un metal.
                        </div> 
                    </div>
                                
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            Ser un átomo altamente inestable pues libera la energía en forma de radiación ionizante.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Producir con facilidad efectos graves en los tejidos del cuerpo expuestos.
                            <br><br>
                        </div> 
                    </div>
                </div>     
            </div>
        </div>
    </div>
</div>