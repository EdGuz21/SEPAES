<div id="contenedor-lenguaje">
    <div id="contenedor-cuerpo">
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicación: Lee el siguiente texto y responde los ítems del 1 al 5. 
                </div>
                <div class="cuerpo-textos">
                        <p>
                        Don Apolinar Moscote, alquiló un cuartito, a dos cuadras de la casa de los Buendía. Clavó en la pared
                        un escudo de la república, y pintó en la puerta el letrero: Corregidor. Su primera disposición fue ordenar
                        que todas las casas se pintaran de azul para celebrar el aniversario de la independencia nacional.
                        Esto molestó a José Arcadio Buendía, quien fue a preguntarle: «¿Usted escribió este papel?». Don
                        Apolinar Moscote, un hombre maduro, tímido, de complexión sanguínea, contestó que sí. «¿Con qué
                        derecho?», volvió a preguntar José Arcadio Buendía. Don Apolinar Moscote buscó un papel en la
                        gaveta de la mesa y se lo mostró: «He sido nombrado corregidor de este pueblo».
                        <br><br>
                        —En este pueblo no mandamos con papeles —dijo sin perder la calma —. Y para que lo sepa de una
                        vez, no necesitamos ningún corregidor porque aquí no hay nada que corregir.
                        <br><br>
                        Entonces, hizo recuento de cómo habían fundado la aldea, de cómo se habían repartido la tierra,
                        sin haber molestado a gobierno alguno, logrando así ser un pueblo autónomo. «Somos tan pacíficos
                        que ni siquiera nos hemos muerto de muerte natural» — dijo — «Ya ve que todavía no tenemos
                        cementerios».
                        <br><br>
                        —De modo que, si usted se quiere quedar aquí, como otro ciudadano común y corriente, sea muy
                        bienvenido —concluyó José Arcadio Buendía—. Pero si viene a implantar el desorden obligando a
                        la gente que pinte su casa de azul, puede largarse por donde vino. Porque mi casa ha de ser blanca
                        como una paloma.
                        <br><br>
                        Don Apolinar Moscote, dio un paso atrás y dijo con una cierta aflicción: —Quiero advertirle que
                        estoy armado.
                        <br><br>
                        José Arcadio Buendía no supo en qué momento se le subió a las manos la fuerza juvenil con que
                        derribaba un caballo. Agarró a don Apolinar Moscote, lo levantó a la altura de sus ojos y lo sacó
                        del pueblo.
                        <br>
                        </p>
                        <p class="p-bold">
                        Una semana después, Don Apolinar estaba de regreso con seis soldados, armados con
                        escopetas, y una carreta de bueyes donde viajaban su mujer y sus siete hijas. Los fundadores
                        de Macondo, resueltos a expulsar a los invasores, fueron con sus hijos mayores a ponerse a
                        disposición de José Arcadio Buendía. Pero él se opuso, según explicó, porque don Apolinar
                        Moscote había vuelto con su mujer y sus hijas, y no era cosa de hombres humillar a otros
                        delante de su familia. Así que decidió arreglar la situación por las buenas  
                        </p>
                </div>
                <div class="citas">
                        Cien años de Soledad <br>
                        Gabriel García Márquez <br>
                        (Adaptación)
                </div>                    
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">1</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            Según el texto, ¿en qué momento José Arcadio expresa a Don Apolinar Moscote que
                            puede quedarse en Macondo?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Cuando el corregidor regresa con su familia y con soldados armados
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Luego de narrar cómo fundaron el pueblo sin molestar al gobierno
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Después que Don Apolinar ha sido nombrado corregidor del pueblo. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Cuando don Apolinar retrocede y le advierte que está armado. 
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">2</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            En el texto, ¿cuál es la intención de José Arcadio al narrar la fundación de Macondo?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Argumentar sobre lo innecesario de un corregidor. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Explicar cómo se repartieron las tierras y mejoraron el pueblo.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                 Comunicar lo indebido de pintar las casas de azul. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Aclarar que él tenía la fuerza juvenil para derribar un caballo. 
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">3</p>
                </div> 
                
                <div class="pregunta-padre">
                        
                    <div class="contenido-pregunta">
                        <p>En el fragmento: «—De modo que, si usted se quiere quedar aquí, como otro ciudadano común y corriente, sea muy bienvenido —concluyó José Arcadio Buendía—. Pero si viene a implantar el desorden obligando a la gente que pinte su casa de azul, puede largarse por donde vino. Porque mi casa ha de ser blanca como una paloma».</p>
                        <p>¿Cuál es la intención de José Arcadio?</p>
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Expresar la hospitalidad de los Buendía hacia Don Apolinar. 
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            Manifestar interés de que Moscote se quedara en Macondo.
                        </div> 
                    </div>

                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            Mostrar su desaprobación por la presencia del corregidor.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Exponer el deseo de pintar su casa blanca como una paloma. 
                        </div>
                    </div>
                </div>    
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">4</p>
                    </div> 
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            <p>De la parte destacada en negrita, se deduce que José Arcadio</p>
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                sentía temor por los soldados que lo acompañaban.   
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                decidió arreglar la llegada del corregidor por las buenas.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                 planeaba expulsar al corregidor con toda su familia.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                reconocía la reputación del responsable de una familia. 
                            </div>
                        </div>
                </div>    
            </div>
            <div class="pregunta">
                <div class="numero-padre">
                    <p class="numero-hijo">5</p>
                </div> 
                
                <div class="pregunta-padre">
                    
                    <div class="contenido-pregunta">
                        <p>Selecciona la opción que presenta una oración simple.</p>
                    </div>
                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                        <div class="opcion-respuesta">
                            Dio un paso atrás y dijo con una cierta aflicción: —Quiero advertirle que estoy armado.
   
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                        <div class="opcion-respuesta">
                            Don Apolinar Moscote, alquiló un cuartito, a dos cuadras de la casa de los Buendía.
                        </div> 
                    </div>

                    <div class="contenedor-respuestas-izq">
                        <div class="literal-padre"><div class="literal-hijo">C</div></div>
                        <div class="opcion-respuesta">
                            Fueron con sus hijos a ponerse a disposición de Arcadio Buendía, pero él se opuso.
                        </div> 
                    </div>
                    <div class="contenedor-respuestas-der">
                        <div class="literal-padre"><div class="literal-hijo">D</div></div>
                        <div class="opcion-respuesta">
                            Agarró a don Apolinar Moscote, lo levantó a la altura de sus ojos y lo sacó del pueblo. 
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicacion:Lee la siguiente estrofa del poema «Como tú» de Roque Dalton y responde el ítem 6.
                </div>
                <div class="cuerpo-textos">
                    <div class="imagen">
                        <img src="css/imagenes/lenguaje/imagen_pregunta6_lenguaje.png" id="imgpregunta6">
                    </div>
                    <div class="contenedor-texto">
                        <h3>COMO TÚ</h3> 
                        <p>Yo, como tú,</p>
                        <p>amo el amor, la vida, el dulce encanto de las cosas, el paisaje celeste de los días de enero.</p>
                        […]
                        <p id="autor">Roque Dalton</p>
                        <p class="citas-1">Roque no ha muerto<br> 
                            David Duke Mental,<br> 
                        mixta sobre lienzo, 100 x 120 cm</p>
                    </div>
                </div>               
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">6</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            En la estrofa predomina la figura literaria llamada elipsis, ¿cuál es el efecto que produce este recurso literario en el poema?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                El texto se vuelve breve y sencillo por la supresión de un sustantivo. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Reduce el significado del poema por la eliminación de los adjetivos.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Añade rapidez y pasión al texto a partir de la supresión de un verbo. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                El texto se comprende mejor por la eliminación de las conjunciones. 
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicación: Lee el siguiente texto y responde los ítems del 7 al 10. 
                </div>
                <div class="cuerpo-textos">
                    <h3>Preservemos el planeta</h3>    
                    <p>
                        El calentamiento global es el incremento de la temperatura en la superficie terrestre, debido a la concentración de gases efecto invernadero en la atmósfera, compuestos principalmente por: vapor de agua, dióxido de carbono (CO2), ozono troposférico y metano, provenientes del consumo de combustibles fósiles como carbón, petróleo y gas. La concentración de estos gases actúa de manera similar a un techo de vidrio, ya que atrapa el calor, y provoca alteraciones en el clima del planeta, tales como: el aumento del nivel del mar, cambios en los modelos de precipitación (lluvias que producen inundaciones y sequías), y la diseminación de enfermedades transmitidas por microorganismos como la malaria.
                    </p>
                    <p>
                        Con respecto al aumento de las temperaturas, los científicos advierten que si el desarrollo mundial, el crecimiento demográfico y el consumo energético, basado en combustibles fósiles, siguen aumentando al ritmo actual, antes del año 2050, las concentraciones de dióxido de carbono se habrán duplicado, por lo que, puede haber consecuencias climáticas devastadoras.
                    </p>
                    <p>
                        No obstante, si la humanidad desea preservar el planeta, se debe reducir las emisiones de CO2 hacia la atmósfera, empezando por dejar de quemar combustibles fósiles, utilizar otras fuentes de energía renovables como la energía solar y eólica, así como mejorar las prácticas agrícolas y forestales en todo el mundo. Sin embargo, aún si las emisiones de gases de efecto invernadero cesan de inmediato, la temperatura seguirá subiendo por unas cuantas décadas más, ya que los efectos del calentamiento global tardan en manifestarse plenamente. 
                    </p>
                </div>
                <div class="citas">
                    Revista digital Conciencia Eco<br>
                    (Adaptación)<br>
                    http://www.concienciaeco.com Recuperado el 20 de abril de 2018
                </div>                    
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">7</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿Cuál es el tema central expuesto en el texto?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                El efecto invernadero en la atmósfera.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                El cambio climático en el mundo.
                                <br><br>
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                El daño a la capa de ozono. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                El calentamiento global. 
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">8</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál es la relación que existe entre el párrafo uno y dos del texto
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Aclaración, porque explica la situación planteada inicialmente.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Contraste, porque hay una contraposición de ideas entre párrafos.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Consecuencia, porque añade efectos de la temática planteada. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Adición, porque agrega nuevos datos al desarrollo del tema. 
                            </div>
                        </div>
                    </div>    
                </div>
                
            <div id=""> <br><br><br>
                
            <div class="textos">
                <div class="titulos">
                       Indicación: Lee las siguientes ideas del texto y responde el ítem 9.  
                </div>
                <div class="cuerpo-textos">   
                    <p>
                        <text class="p-bold">a)</text> El crecimiento demográfico y consumo energético duplica el dióxido de carbono en la atmósfera.<br> 
                        <text class="p-bold">b)</text> La concentración de compuestos en la atmósfera incrementa la temperatura en la tierra. <br>
                        <text class="p-bold">c)</text> La temperatura seguirá aumentando, aunque dejen de usarse combustibles fósiles. <br>
                        <text class="p-bold">d)</text> La reducción de emisiones de dióxido de carbono permitirá preservar el planeta. <br>
                        <text class="p-bold">e)</text> El calentamiento global es la causa de inundaciones y muchas enfermedades.
                    </p>
                </div>                   
            </div>
            <div class="pregunta">
                <div class="numero-padre">
                        <p class="numero-hijo">9</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Selecciona la opción que presenta ordenadamente las ideas principales de los párrafos 1, 2 y 3
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                1-b, 2-a y 3-d 

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                1-e, 2-c y 3-a
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                1-c, 2-d y 3-b 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                1-a, 2-e y 3-c 
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">10</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            De la lectura del texto, se deduce que la intención del autor es:
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                exponer la concentración de gases en la atmósfera.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                detallar las causas y consecuencias del cambio climático.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                describir acciones que originan y causan el calentamiento global. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                concientizar a las personas sobre los daños producidos al planeta. 
                            </div>
                        </div>
                </div>    
            </div>
        </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicación: Analiza el siguiente texto propagandístico y responde los ítems 11 y 12.  
                </div>
                <div class="cuerpo-textos">   
                    <img src="css/imagenes/lenguaje/imagen_preguntas11-12_lenguaje.png" id="imgpreguntas11-12">
                </div> 
                <div class="citas">
                    (Adaptación)<br> 
                    Retomado de https://static0.misionesonline.net/wp-content/uploads/2017/01/Dengue1-800x600-3o1iik3mp5i0.jpg
                </div>
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">11</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál es la idea central del texto anterior? 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                La población debe combatir los criaderos de zancudos.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                El zancudo se reproduce en las escuelas y las casas.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Los zancudos deben eliminarse en la casa y escuela.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Los depósitos que retengan agua deben ser eliminados.
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">12</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            El propósito del texto propagandístico anterior es: 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                informar a la comunidad educativa sobre las consecuencias del dengue.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                explicar cómo eliminar los criaderos de zancudo en la escuela.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                motivar a las personas a unirse para eliminar las causas del dengue.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                describir la forma cómo se reproducen los zancudos en la casa.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicación: Lee la siguiente entrevista realizada por el periodista Juan Pablo del programa radial «El deportista joven» al entrenador de la Universidad Nacional de Deportes, y responde los ítems del 13 al 15. 
                </div>
                <div class="cuerpo-textos"> 
                    <p>
                        <text class="p-bold">Juan Pablo:</text> Buenas tardes, entrenador. Queremos que nos hable sobre los resultados de sus deportistas, así como de los proyectos frente al encuentro deportivo que se avecina.<br>
                        <text class="p-bold">Entrenador:</text> ¡Hola! Sí, como se avecinan los juegos entre las universidades, hemos redoblado nuestros esfuerzos para que los jóvenes mejoren.<br>
                        <text class="p-bold">Juan Pablo:</text> ¿Qué nos puede decir sobre los problemas con las pruebas de dopaje que han tenido algunos de los jugadores?<br>
                        <text class="p-bold">Entrenador:</text> Mira, es claro que ante tanta presión los jóvenes buscan formas fáciles para mejorar sus capacidades, y por más que los revisemos no podemos controlarlos fuera de los planteles; sé que somos sus entrenadores, pero siempre existe un momento en el que se encuentran solos y ahí solo se pueden cobijar con su libre albedrío.<br>
                        <text class="p-bold">Juan Pablo:</text> ¿Con esto nos quiere decir que no puede hacer nada?<br>
                        Entrenador: Puedo decir que esto nos ha obligado a tomar medidas más estrictas, como son revisiones semanales y la ayuda de nutriólogos y psicólogos que nos asesoren para poder corregir los problemas y poder participar en los juegos.<br>
                        <text class="p-bold">Juan Pablo:</text> Y respecto a las penalizaciones que sus deportistas sufrieron, ¿qué puede decir?<br>
                        <text class="p-bold">Entrenador:</text> Los jóvenes que infringieron, fueron dos y ya cumplieron las penas impuestas por los dirigentes deportivos; y se demostró que se produjo por consumir alimentos que contenían clembuterol, los cuales fueron proporcionados en el centro deportivo de la universidad en la que competíamos, en donde también se presentaron penalizaciones. Así que podrán jugar, aunque se les retiraron las medallas. Quedaron exentos de responsabilidad y limpios en su expediente.<br>
                        <text class="p-bold">Juan Pablo:</text> ¿Qué dijeron los muchachos sobre esto?<br>
                        <text class="p-bold">Entrenador:</text> Solo dicen que hay ocasiones en que pagan justos por pecadores, pero que esperan mejorar esto en los juegos de este año.<br>
                        <text class="p-bold">Juan Pablo:</text> Ya para terminar, ¿cuántos estudiantes enviarán al encuentro deportivo?<br>
                        <text class="p-bold">Entrenador:</text> Serán un total de treinta y dos (32), ya cuentan con todos sus documentos y exámenes físicos cumplidos. Tienen los mejores tiempos en todo el plantel.<br>
                        <text class="p-bold">Juan Pablo:</text> Bien, esperemos que tengan la mejor de las suertes y que nos veamos después de los juegos deportivos con buenas noticias. Muchas gracias.<br>
                        <text class="p-bold">Entrenador:</text> Al contrario, gracias a ti y a «El deportista joven» por habernos visitado. 
                    </p>
                </div>
                <div class="citas">
                    (Adaptación)<br> 
                    Recuperado de: http://ejemplosde.con/11-escritos/908-ejemplo_de_entrevista.atml
                </div>                    
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">13</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿Cuál es la idea principal en el texto sombreado?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Los entrenadores revisan a los deportistas dentro del plantel de manera periódica.  
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Fuera del plantel los deportistas realizan menos actividades de entrenamiento.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Los entrenadores tienen dificultades para vigilar a los jóvenes fuera del plantel. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                            	<br>
                                El calentamiento global.
                                <br><br>
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">14</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿Qué quiere decir el entrenador con la frase subrayada en el texto?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                El comportamiento juvenil puede ser controlado.  
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Los deportistas son autónomos en lo que hacen.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                El esfuerzo de los jóvenes fortalece su desempeño.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Los deportistas pueden rendir más estando libres.
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">15</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿Cuál es la intención del entrevistador en el texto?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Presentar información sobre el comportamiento juvenil.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Informar acerca del encuentro que tendrán los deportistas.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Opinar sobre el rendimiento de los jóvenes en los juegos.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Indagar sobre el desempeño de estudiantes deportistas.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicación: Lee el siguiente texto y responde los ítems del 16 al 20. 
                </div>
                <div class="cuerpo-textos">
                    <h3>El impacto del internet en la vida diaria </h3>    
                    <p>
                        El internet ha revolucionado muchos ámbitos y especialmente el de las comunicaciones de una manera radical en nuestras vidas, hasta el punto de llegar a convertirse en un medio global de comunicación. Lo utilizamos para casi todo, desde compartir un momento con un amigo enviando una foto a través de mensajería instantánea hasta pedir una pizza o comprar un televisor. Antes, si queríamos leer un periódico, debíamos comprar una edición local en papel cuando abría el quiosco de prensa con las noticias del día anterior. Hoy, con un solo clic podemos leer nuestro periódico local, y también el periódico de cualquier parte del mundo. 
                    </p>
                    <p>
                        El internet ha evolucionado muchísimo desde su creación, sin embargo, es muy reciente desde la perspectiva de la historia y poco ha quedado de la primera red estática concebida para transportar unos cuantos bytes o para enviar un pequeño mensaje entre dos terminales. 
                    </p>
                    <p>
                        El desarrollo del internet ha puesto en marcha un debate sobre el modo en el que la comunicación, a través de la red, afecta las relaciones sociales, ya que libera al individuo de las restricciones geográficas y une a las personas en torno a nuevas comunidades de interés que no están atadas a un lugar concreto, por lo que plantea nuevos retos para la privacidad y la seguridad.
                    </p>
                    <p>
                        <text class="p-bold">Así pues</text>, las tecnologías de la información han forjado cambios fundamentales en toda la sociedad, posibilitando el paso de la era industrial a la era de redes. Vivimos en una sociedad en la que las redes de información global resultan ser infraestructuras esenciales, pero ¿cómo han afectado estos cambios a las relaciones humanas?…
                    </p>
                </div>
                <div class="citas">
                    (Adaptación)<br> 
                    Recuperado de: https://www.bbvaopenmind.com/articulo/el-impacto-de-internet-en-la-vida-diaria/
                </div>                    
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">16</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            Selecciona la opción que contiene un conector discursivo.
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                […] El internet ha evolucionado muchísimo desde su creación, sin embargo, es muy reciente desde la perspectiva de la historia […] 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                […] las tecnologías de la información han forjado cambios fundamentales en toda la sociedad, posibilitando el paso de la era industrial […]
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                […] si queríamos leer un periódico, debíamos comprar una edición local en papel cuando abría el quiosco de prensa […]
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                […] Vivimos en una sociedad en la que las redes de información global resultan ser infraestructuras esenciales […] 
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">17</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            Según la información del primer párrafo, ¿cuál es el principal efecto del internet en la sociedad? 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Logró modificar las relaciones interpersonales.

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Propició la compra y venta a través de las redes.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Modificó los ambientes en la vida de las personas.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                               Generó avances importantes en las comunicaciones. 
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">18</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            Selecciona la opción que presenta la relación correcta entre la idea con el párrafo correspondiente.<br><br>
                            <img src="css/imagenes/lenguaje/imagen_preguntas18_lenguaje.png" id="imgpregunta18_lenguaje">
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                1C, 2A, 3D, 4B  
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                1D, 2A, 3C, 4B
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                1D, 2C, 3B, 4A
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                               1C, 2B, 3A, 4D 
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">19</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿Para qué se utilizó el marcador discursivo destacado en negrita en el último párrafo?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Comparar la información presentada en los párrafos tres y cuatro.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                            	<br>
                                Vincular lo dicho en los párrafos anteriores con lo que expresará.
                                <br>
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Ampliar la información planteada en los párrafos anteriores.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                               Argumentar lo que será expresado en el párrafo cuatro.
                            </div>
                        </div>
                    </div>    
                </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">20</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            Selecciona la opción en la que todas las palabras se clasifican correctamente.
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/lenguaje/imagen_preguntas20_opcionA_lenguaje.png" id="imgopcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/lenguaje/imagen_preguntas20_opcionB_lenguaje.png" id="imgopcion">
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/lenguaje/imagen_preguntas20_opcionC_lenguaje.png" id="imgopcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                               <img src="css/imagenes/lenguaje/imagen_preguntas20_opcionD_lenguaje.png" id="imgopcion">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        <div id="div-cuerpo"> <br>
            <div class="textos">
                <div class="titulos">
                       Indicación: Lee el siguiente texto y responde los ítems del 21 al 25. 
                </div>
                <div class="cuerpo-textos">
                    <h3>Marianela</h3>    
                    <p>
                        —¡Oh!, sí, lo entiendo[...] todo lo tenemos dentro. El sol, las yerbas, y la luna y el cielo grande y azul, lleno siempre de estrellas; todo, todo lo tenemos dentro; además de las cosas divinas que hay fuera, nosotros llevamos otras dentro [...] Aquí tienes una flor. ¿A que no sabes tú lo qué son las flores?
                    </p>
                    <p>
                        —Pues las flores —dijo el ciego, algo confuso, acercándolas a su rostro— son[...] unas como sonrisillas que echa la tierra[...]
                    </p>
                    <p> —Madre Divinísima, —exclamó María, —las flores son las estrellas de la tierra. </p>
                    <p>—Vaya un disparate. ¿Y las estrellas, qué son? </p>
                    <p>—Las estrellas son las miradas de los que se han ido al cielo.</p>
                    <p>—Entonces las flores... </p>
                    <p>
                        —Son las miradas de los que se han muerto y no han ido todavía al cielo —afirmó la Nela, —los muertos son enterrados en la tierra. Como allá abajo no pueden estar sin echar una miradilla a la tierra, echan una cosa que sube en forma y manera de flor. Cuando en un prado hay muchas flores es porque allá[...] en tiempos de atrás, enterraron en él muchos difuntos.
                    </p>
                    <p>—No, no —replicó Pablo con seriedad—. No creas desatinos.</p>
                    <p>
                        —¡Tú has querido hacerme creer que el Sol está quieto y que la Tierra da vueltas a la redonda![...] ¡Cómo se conoce que no lo ves! ¡Madre del Señor! Que me muera en este momento, si la Tierra no se está quieta, y el Sol va corre que corre. Señorito mío, no se la eche de tan sabio, que yo he pasado muchas horas de noche y de día mirando al cielo, y sé cómo está gobernada toda esa máquina[...] La Tierra está abajo, toda llena de islitas grandes y chicas. El Sol sale por allá y se esconde por allí. Es el palacio de Dios.
                    </p>    
                </div>
                <div class="citas">
                    Marianela<br>
                    Benito Pérez Galdós<br>
                    (Adaptación)
                </div>                    
            </div>
            <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">21</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            Según lo expresado por Pablo, ¿qué son las flores?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                 «son las estrellas de la tierra».
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                «miradas de los que se han muerto».
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                «sonrisillas que echa la tierra».
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                «miradas de los que han ido al cielo». 
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">22</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿En qué momento Nela afirma que la Tierra permanece inmóvil y que el Sol corre?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                 Antes de evidenciar que Pablo nunca ha visto el Sol.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Después de expresar que ha pasado horas observando el cielo.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Mientras señala el lugar por donde sale y se esconde el Sol.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Cuando Pablo resta seriedad a su argumento sobre las flores.
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">23</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                             ¿Cuál es el tema central del fragmento presentado?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                 Una descripción de las estrellas.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Las cosas que llevamos dentro.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Las creencias de los personajes.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                El movimiento de la Tierra y el Sol. 
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">24</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            ¿A qué se refiere Nela cuando dice: «además de las cosas divinas que hay fuera, nosotros llevamos otras dentro»?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                 Virtudes y cualidades que todos poseemos.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                La imaginación que tiene cada persona.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Los elementos maravillosos de la naturaleza.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Las cosas divinas que llevamos dentro. 
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">25</p>
                    </div> 
                
                
                    <div class="pregunta-padre">
                    
                        <div class="contenido-pregunta">
                            En el enunciado «El sol, las yerbas, y la luna y el cielo grande y azul, lleno siempre de  estrellas», ¿cuál es la intención del narrador al repetir la conjunción «y»?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                 Desplazar el significado de los términos con finalidad estética.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Repetir un sonido para crear musicalidad en el fragmento.
                            </div> 
                        </div>

                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Disminuir el ritmo y proporcionar intensidad a la expresión.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Transmitir el mensaje de forma indirecta, sin dejarlo claro. 
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
    </div>
</div>        
