<div id="contenedor-lenguaje">
    <div id="contenedor-cuerpo">
        <div class="textos">
            <div class="cuerpo-textos">
                <p>Tablas necesarias para resolver algunos ejercicios</p>
                <img src="css/imagenes/matematica/tabla1.png" id="imgtabla_matematica">
                <br><br>
                <img src="css/imagenes/matematica/tabla2.png" id="imgtabla_matematica">
                <br><br>
                <img src="css/imagenes/matematica/tabla3.png" id="imgtabla_matematica">
            </div>
        </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 1. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            En la construcción de inmuebles el porcentaje de inclinación de pendiente “z %”, se calcula mediante la fórmula siguiente.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta1_mat.png" id="imgpregunta1_matematica">
                        <p>
                            Según las normas internacionales de accesibilidad para las personas con discapacidad motora, las rampas de acceso en cualquier edificio deben tener un porcentaje máximo de inclinación de pendiente del 10 %.
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">1</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál debe ser la distancia horizontal de una rampa para que cumpla con las normas internacionales de accesibilidad, si debe alcanzar una altura de 80 cm?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                460.70 cm
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                793.73 cm
                            </div> 
                        </div>
                    
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                800.00 cm
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                806.23 cm
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="titulos">
                        Indicación: Lee la siguiente información y contesta el ítem 1. 
                    </div>
                    <div class="cuerpo-textos">
                        <p>
                            Juan, Jonathan y Luis juegan fútbol. En un momento del partido se ubican como muestra la figura.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta2_mat.png" id="imgpregunta2_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">2</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de las siguientes igualdades, permite determinar correctamente la distancia a la que se encuentran Juan y Jonathan?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta2_opcionA_mat.png" id="imgpregunta2_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta2_opcionB_mat.png" id="imgpregunta2_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta2_opcionB_mat.png" id="imgpregunta2_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta2_opcionB_mat.png" id="imgpregunta2_opcion">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Los costos de producción de marcos para fotografías siguen un comportamiento lineal de acuerdo a los datos mostrados en la tabla:
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta3_mat.png" id="imgpregunta3_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">3</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de las siguientes igualdades, permite determinar correctamente la distancia a la que se encuentran Juan y Jonathan?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta3_opcionA_mat.png" id="imgpregunta2_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta3_opcionB_mat.png" id="imgpregunta2_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta3_opcionC_mat.png" id="imgpregunta2_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta3_opcionD_mat.png" id="imgpregunta2_opcion">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">4</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            <p>Cada una de las secuencias abajo mostradas, está conformada por una cantidad de figuras semejantes que siguen diferentes patrones de construcción.</p> 
                            ¿Cuál de las siguientes secuencias modela una sucesión geométrica?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta4_opcionA_mat.png" id="imgpregunta4_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta4_opcionB_mat.png" id="imgpregunta4_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta4_opcionC_mat.png" id="imgpregunta4_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta4_opcionD_mat.png" id="imgpregunta4_opcion">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Luis asistió a un parque de diversiones, observó desde el suelo a sus amigos que se encontraban en uno de los juegos y decidió calcular la distancia “a”. Tomando en cuenta los datos mostrados.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta5_mat.png" id="imgpregunta5_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">5</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál es resultado del cálculo de Luis?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                3.99 m
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                4.24 m
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                5.02 m
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                6.67 m
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">6</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            <p>Un fabricante de artículos tecnológicos, afirma que la función <img src="css/imagenes/matematica/imagen_pregunta6_formula_mat.png" id="imgpregunta6_formula"> representa el porcentaje de productos que siguen funcionando después de “ x ” años.</p>
                            ¿Cuál de los siguientes procesos determina correctamente la cantidad de años “ x ”, en los que el 20 % de los productos del fabricante siguen funcionando?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta6_opcionA_mat.png" id="imgpregunta6_opcionA">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <br><br>
                                <img src="css/imagenes/matematica/imagen_pregunta6_opcionB_mat.png" id="imgpregunta6_opcionB">
                                <br><br>
                            </div> 

                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta6_opcionC_mat.png" id="imgpregunta6_opcionC">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <br>
                                <img src="css/imagenes/matematica/imagen_pregunta6_opcionD_mat.png" id="imgpregunta6_opcionD">
                                <br><br>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Karla dispone de las siguientes aplicaciones para descargar en su celular.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta7_mat.png" id="imgpregunta5_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">7</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Si ella decide instalar en su celular siete aplicaciones distintas, ¿cuál de las siguientes expresiones, permite calcular los diferentes grupos de aplicaciones que podrá descargar?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta7_opcionA_mat.png" id="imgpregunta4_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta7_opcionB_mat.png" id="imgpregunta4_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta7_opcionC_mat.png" id="imgpregunta4_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta7_opcionD_mat.png" id="imgpregunta4_opcion">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Un topógrafo necesita calcular la longitud “ x ” del puente representado en la figura, por lo que, hace las siguientes mediciones.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta8_mat.png" id="imgpregunta5_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">8</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Si ella decide instalar en su celular siete aplicaciones distintas, ¿cuál de las siguientes expresiones, permite calcular los diferentes grupos de aplicaciones que podrá descargar?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta8_opcionA_mat.png" id="imgpregunta8_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta8_opcionB_mat.png" id="imgpregunta8_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta8_opcionC_mat.png" id="imgpregunta8_opcion">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta8_opcionD_mat.png" id="imgpregunta8_opcion">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">9</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Una empresa especializada en la construcción de pozos, cobra $1,000 por perforar el primer metro de un pozo e incrementa en 10 % el precio de cada metro que se perfora respecto al costo del metro anterior. ¿Cuánto cobrará la empresa por perforar un pozo de 5 metros?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                $5,010.00

                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                $5,500.00
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                $6,105.10
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                $6,856.40
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Observa el siguiente gráfico e interpreta.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta10_mat.png" id="imgpregunta10_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">10</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de las siguientes afirmaciones es la correcta?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                En la semana 1 desde el día martes la temperatura aumentó, mientras que en la semana 2 tendió a bajar a partir del día jueves.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                
                                En la semana 1 el día domingo se registró la mayor temperatura, mientras que en la semana 2 se registró el día sábado.
                                
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Las temperaturas en la semana 1 desde el día domingo tuvieron un comportamiento idéntico a las presentadas en la semana 2 a partir del mismo día.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Las temperaturas en la semana 1 desde el día martes tuvieron un comportamiento contrario a las presentadas en la semana 2 a partir del mismo día.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Una madre de familia motiva a su hija al hábito de ahorrar, sugiriéndole un plan progresivo de ahorro de monedas de un centavo, como el mostrado en la imagen.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta11_mat.png" id="imgpregunta10_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">11</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuántos centavos habrá ahorrado en 30 días?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                900
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                930
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                960
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                990
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Una empresa dispone de una guía de luces de 20 m para decorar un árbol navideño gigante, como el que se muestra en la figura.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta12_mat.png" id="imgpregunta12_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">12</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Alcanzará la guía de luces para decorar el árbol?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Sí, porque el perímetro es 18.79 m.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Sí, porque el perímetro es 19.87 m.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                No, porque el perímetro es 20.58 m.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                No, porque el perímetro es 21.00 m.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            En la siguiente tabla, se presenta la cantidad de calorías que aportan al cuerpo el consumo de alimentos en las porciones mostradas.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta13_mat.png" id="imgpregunta13_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">13</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Según el Ministerio de Salud (MINSAL) en una dieta balanceada, para el almuerzo deberían consumirse un mínimo de 720 calorías y como máximo 1,000 calorías; de acuerdo a esta información y la presentada en la tabla, ¿cuál de las siguientes propuestas de almuerzo cumple los requerimientos de una dieta balanceada?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                4 panes, ½ taza de arroz y 1 taza de frijoles. 
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                1 pechuga de pollo, 1 taza de arroz, 2 tortillas y ½ taza de frijoles.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                1 pechuga de pollo, ½ taza de arroz, 2 tortillas y 1 mango.
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                4 panes, ½ taza de frijoles y 1 naranja.
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            En la siguiente tabla, se presenta la cantidad de calorías que aportan al cuerpo el consumo de alimentos en las porciones mostradas.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta14_mat.png" id="imgpregunta14_matematica">
                        <p>
                            Si una oración con el lenguaje de señas tiene la siguiente estructura:<br>
                            <text class="cen-bold">Sujeto – objeto – verbo</text><br>
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">14</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de las siguientes expresiones permite determinar la cantidad de oraciones que se pueden formar con los elementos de la imagen?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                15 + 14 + 13
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                6 × 4 × 5
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                6 + 4 + 5
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                15 × 14 × 13
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            En una escuela se construirá una rampa con el fin de facilitar la movilidad a personas con discapacidad motora. Para que cumpla con las medidas de seguridad debe tener un ángulo como el mostrado en la figura.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta15_mat.png" id="imgpregunta14_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">15</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Qué expresión permite encontrar la altura “ x ” que debe tener la rampa?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta15-opcionA_mat.png" id="imgpregunta15_opcion_matematica">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <br>
                                <img src="css/imagenes/matematica/imagen_pregunta15-opcionB_mat.png" id="imgpregunta15_opcion_matematica">
                                <br>
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta15-opcionC_mat.png" id="imgpregunta15_opcion_matematica">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta15-opcionD_mat.png" id="imgpregunta15_opcion_matematica">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">16</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            En una cafetería 6 de cada 10 clientes, prefieren desayunar pupusas. Si en una hora llegan siete comensales, ¿cuál es la probabilidad que cinco desayunen pupusas?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                0.0774
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                0.0941
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                0.2613
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                0.7143
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Observa las siguientes figuras construidas con cuadrados de lado uno.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta17_mat.png" id="imgpregunta14_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">17</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de los siguientes términos <img src="css/imagenes/matematica/imagen_pregunta17_an_mat.png"> modela la cantidad de cuadrados en cada figura?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta17_opcionA_mat.png" id="imgpregunta17_opcion_matematica">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta17_opcionB_mat.png" id="imgpregunta17_opcion_matematica">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta17_opcionC_mat.png" id="imgpregunta17_opcion_matematica">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta17_opcionD_mat.png" id="imgpregunta17_opcion_matematica">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Adonay hizo una acrobacia con su bicicleta, como se muestra en la figura.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta18_mat.png" id="imgpregunta18_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">18</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            La medida del ángulo θ que se forma al realizar la acrobacia es:
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                28.44°
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                32.80°
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                57.20°
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                61.56°
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            En una fábrica el sueldo medio mensual es de $300. El empleador dará un incremento a sus trabajadores, ofreciéndoles la alternativa que sea por $30 al sueldo o el 10 % del sueldo.<br><br>
                            Los trabajadores se reúnen para discutir <text class="p-bold">propuestas</text> de conveniencia del incremento de acuerdo al sueldo, como se muestra: 
                        </p>
                        <p>
                            I. El empleado que gana más del sueldo medio le conviene el incremento de $30.<br>
                            II. El empleado que gana menos del sueldo medio le conviene el incremento del 10 %.<br> 
                            III. El empleado que gana menos del sueldo medio le conviene el incremento de $30. <br>
                            IV. El empleado que gana el sueldo medio, cualquiera de los incrementos le conviene.
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">19</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de los siguientes pares de propuestas discutidas son beneficiosas simultáneamente?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                I y II
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                II y III
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                III y IV
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                I y IV
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Los estudiantes de 2º año de bachillerato organizaron la semana de lectura y registraron la asistencia de cada día, como se muestra en el gráfico.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta20_mat.png" id="imgpregunta20_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">20</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál fue la cantidad media de asistentes por día?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                104
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                110
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                120
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                160
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Un depósito se llenó con 1200 m3 de agua como se muestra en la parte izquierda de la figura. En la parte derecha se traza la gráfica de la función que relaciona el volumen alcanzado en el depósito al transcurrir el tiempo.
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta21_mat.png" id="imgpregunta21_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">21</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Encuentra el dominio y recorrido de la función, cuando el volumen del depósito se llenó entre 400 y 900 m^3.
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                Dom: [ 0, 1200 ]  ;  Rango: [ 0, 60 ]
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                Dom: [ 20, 50 ]  ;  Rango: [ 400, 900 ]
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                Dom: [ 400, 900 ]  ;  Rango: [ 20, 50 ]
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                Dom: [ 0, 60 ]  ;  Rango: [ 0, 1200 ]
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Los resultados obtenidos por un estudiante en la asignatura de matemática a lo largo de cinco meses son los siguientes:
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta22_mat.png" id="imgpregunta22_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">22</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Si el estudiante se propone para el próximo mes incrementar en un 10 % su promedio actual, ¿qué promedio deberá obtener?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                6.10
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                6.20
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre-correcta"><div class="literal-hijo">C</div></div>
                            <div class="opcion-correcta">
                                6.71
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                6.75
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Un recipiente con agua a 15°C se coloca en el fuego, aumentando su temperatura constantemente hasta 80°C, durante los primeros 10 minutos. Luego de este tiempo la temperatura se mantuvo constante.
                        </p>
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">23</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál de los siguientes gráficos representa adecuadamente el fenómeno descrito?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta23_opcionA_mat.png" id="imgpregunta23_opcion_mat">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                               <img src="css/imagenes/matematica/imagen_pregunta23_opcionB_mat.png" id="imgpregunta23_opcion_mat">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                               <img src="css/imagenes/matematica/imagen_pregunta23_opcionC_mat.png" id="imgpregunta23_opcion_mat">
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                <img src="css/imagenes/matematica/imagen_pregunta23_opcionD_mat.png" id="imgpregunta23_opcion_mat">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">24</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            Una institución de beneficencia hará la rifa de un carro y solo se venderán 300 boletos. Si una persona quiere tener un 30 % de posibilidades de sacarse el premio, ¿qué cantidad mínima de números debe comprar?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                 10
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                30
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                90
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                100
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div id="div-cuerpo"> <br>
                <div class="textos">
                    <div class="cuerpo-textos">
                        <p>
                            Los resultados obtenidos por un estudiante en la asignatura de matemática a lo largo de cinco meses son los siguientes:
                        </p>
                        <img src="css/imagenes/matematica/imagen_pregunta25_mat.png" id="imgpregunta25_matematica">
                    </div>
                </div>
                <div class="pregunta">
                    <div class="numero-padre">
                        <p class="numero-hijo">25</p>
                    </div> 
                    <div class="pregunta-padre">
                        <div class="contenido-pregunta">
                            ¿Cuál es la probabilidad que un grupo de ocho personas que entran en un ascensor pese más de 1016 lb?
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"> <div class="literal-hijo">A</div></div>
                            <div class="opcion-respuesta">
                                0.1788
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"> <div class="literal-hijo">B</div> </div>
                            <div class="opcion-respuesta">
                                0.3212
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-izq">
                            <div class="literal-padre"><div class="literal-hijo">C</div></div>
                            <div class="opcion-respuesta">
                                0.6424
                            </div> 
                        </div>
                        <div class="contenedor-respuestas-der">
                            <div class="literal-padre"><div class="literal-hijo">D</div></div>
                            <div class="opcion-respuesta">
                                0.8212
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
</div>