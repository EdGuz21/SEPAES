<!DOCTYPE html>

<?php//  require  'funciones-test/funciones-test.php' ?>
<html>
    <head>
        <title>Test Estudios Sociales</title>
        
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/estilo_eess.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/test-lenguaje.css">
    </head>
        <body>

 <div  id="cols" class="col-12">
    <div id="col-img" class="col-3">
              <div id="" >
              <p id="clock">TIEMPO</p>
            </div>
    </div>
    <div id="col-saludo" class="col-6">
            <h3><b><font color='white'>Test de Estudios Sociales </font></b></h3>
             
    </div>
    <div id="col-btns" class="margin-left">
             <button class="btn btn-success"  form="contenedor-respuestas" type="submit" name="btn-finalizar" id="btn-finalizar">Finalizar</button>        
    </div>    
    </div>

         <br><br><br>
    
<?php include 'eess.php' ?>


    <form id="contenedor-respuestas" action="../resultado_test.php" method="POST">
    <h4 id="lbl-respuestas">Respuestas</h4>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 01  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-1" id="A1" value="a">
                <label for="A1">A</label>
        
                <input type="radio" name="respuesta-1" id="B1" value="b">
                <label for="B1">B</label>
        
                <input type="radio" name="respuesta-1" id="C1" value="c">
                <label for="C1">C</label>

                <input type="radio" name="respuesta-1" id="D1" value="d">
                <label for="D1">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 02  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-2" id="A2" value="a">
                <label for="A2">A</label>
        
                <input type="radio" name="respuesta-2" id="B2" value="b">
                <label for="B2">B</label>
        
                <input type="radio" name="respuesta-2" id="C2" value="c">
                <label for="C2">C</label>

                <input type="radio" name="respuesta-2" id="D2" value="d">
                <label for="D2">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 03  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-3" id="A3" value="a">
                <label for="A3">A</label>
        
                <input type="radio" name="respuesta-3" id="B3" value="b">
                <label for="B3">B</label>
        
                <input type="radio" name="respuesta-3" id="C3" value="c">
                <label for="C3">C</label>

                <input type="radio" name="respuesta-3" id="D3" value="d">
                <label for="D3">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 04  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-4" id="A4" value="a">
                <label for="A4">A</label>
        
                <input type="radio" name="respuesta-4" id="B4" value="b">
                <label for="B4">B</label>
        
                <input type="radio" name="respuesta-4" id="C4" value="c">
                <label for="C4">C</label>

                <input type="radio" name="respuesta-4" id="D4" value="d">
                <label for="D4">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 05  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-5" id="A5" value="a">
                <label for="A5">A</label>
        
                <input type="radio" name="respuesta-5" id="B5" value="b">
                <label for="B5">B</label>
        
                <input type="radio" name="respuesta-5" id="C5" value="c">
                <label for="C5">C</label>

                <input type="radio" name="respuesta-5" id="D5" value="d">
                <label for="D5">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 06  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-6" id="A6" value="a">
                <label for="A6">A</label>
        
                <input type="radio" name="respuesta-6" id="B6" value="b">
                <label for="B6">B</label>
        
                <input type="radio" name="respuesta-6" id="C6" value="c">
                <label for="C6">C</label>

                <input type="radio" name="respuesta-6" id="D6" value="d">
                <label for="D6">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 07  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-7" id="A7" value="a">
                <label for="A7">A</label>
        
                <input type="radio" name="respuesta-7" id="B7" value="b">
                <label for="B7">B</label>
        
                <input type="radio" name="respuesta-7" id="C7" value="c">
                <label for="C7">C</label>

                <input type="radio" name="respuesta-7" id="D7" value="d">
                <label for="D7">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 08  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-8" id="A8" value="a">
                <label for="A8">A</label>
        
                <input type="radio" name="respuesta-8" id="B8" value="b">
                <label for="B8">B</label>
        
                <input type="radio" name="respuesta-8" id="C8" value="c">
                <label for="C8">C</label>

                <input type="radio" name="respuesta-8" id="D8" value="d">
                <label for="D8">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 09  </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-9" id="A9" value="a">
                <label for="A9">A</label>
        
                <input type="radio" name="respuesta-9" id="B9" value="b">
                <label for="B9">B</label>
        
                <input type="radio" name="respuesta-9" id="C9" value="c">
                <label for="C9">C</label>

                <input type="radio" name="respuesta-9" id="D9" value="d">
                <label for="D9">D</label>
      </div>
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 10 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-10" id="A10" value="a">
                <label for="A10">A</label>
        
                <input type="radio" name="respuesta-10" id="B10" value="b">
                <label for="B10">B</label>
        
                <input type="radio" name="respuesta-10" id="C10" value="c">
                <label for="C10">C</label>

                <input type="radio" name="respuesta-10" id="D10" value="d">
                <label for="D10">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 11 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-11" id="A11" value="a">
                <label for="A11">A</label>
        
                <input type="radio" name="respuesta-11" id="B11" value="b">
                <label for="B11">B</label>
        
                <input type="radio" name="respuesta-11" id="C11" value="c">
                <label for="C11">C</label>

                <input type="radio" name="respuesta-11" id="D11" value="d">
                <label for="D11">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 12 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-12" id="A12" value="a">
                <label for="A12">A</label>
        
                <input type="radio" name="respuesta-12" id="B12" value="b">
                <label for="B12">B</label>
        
                <input type="radio" name="respuesta-12" id="C12" value="c">
                <label for="C12">C</label>

                <input type="radio" name="respuesta-12" id="D12" value="d">
                <label for="D12">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 13 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-13" id="A13" value="a">
                <label for="A13">A</label>
        
                <input type="radio" name="respuesta-13" id="B13" value="b">
                <label for="B13">B</label>
        
                <input type="radio" name="respuesta-13" id="C13" value="c">
                <label for="C13">C</label>

                <input type="radio" name="respuesta-13" id="D13" value="d">
                <label for="D13">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 14 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-14" id="A14" value="a">
                <label for="A14">A</label>
        
                <input type="radio" name="respuesta-14" id="B14" value="b">
                <label for="B14">B</label>
        
                <input type="radio" name="respuesta-14" id="C14" value="c">
                <label for="C14">C</label>

                <input type="radio" name="respuesta-14" id="D14" value="d">
                <label for="D14">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 15 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-15" id="A15" value="a">
                <label for="A15">A</label>
        
                <input type="radio" name="respuesta-15" id="B15" value="b">
                <label for="B15">B</label>
        
                <input type="radio" name="respuesta-15" id="C15" value="c">
                <label for="C15">C</label>

                <input type="radio" name="respuesta-15" id="D15" value="d">
                <label for="D15">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 16 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-16" id="A16" value="a">
                <label for="A16">A</label>
        
                <input type="radio" name="respuesta-16" id="B16" value="b">
                <label for="B16">B</label>
        
                <input type="radio" name="respuesta-16" id="C16" value="c">
                <label for="C16">C</label>

                <input type="radio" name="respuesta-16" id="D16" value="d">
                <label for="D16">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 17 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-17" id="A17" value="a">
                <label for="A17">A</label>
        
                <input type="radio" name="respuesta-17" id="B17" value="b">
                <label for="B17">B</label>
        
                <input type="radio" name="respuesta-17" id="C17" value="c">
                <label for="C17">C</label>

                <input type="radio" name="respuesta-17" id="D17" value="d">
                <label for="D17">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 18 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-18" id="A18" value="a">
                <label for="A18">A</label>
        
                <input type="radio" name="respuesta-18" id="B18" value="b">
                <label for="B18">B</label>
        
                <input type="radio" name="respuesta-18" id="C18" value="c">
                <label for="C18">C</label>

                <input type="radio" name="respuesta-18" id="D18" value="d">
                <label for="D18">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 19 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-19" id="A19"  value="a">
                <label for="A19">A</label>
        
                <input type="radio" name="respuesta-19" id="B19"  value="b">
                <label for="B19">B</label>
        
                <input type="radio" name="respuesta-19" id="C19"  value="c">
                <label for="C19">C</label>

                <input type="radio" name="respuesta-19" id="D19"  value="d">
                <label for="D19">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 20 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-20" id="A20"  value="a">
                <label for="A20">A</label>
        
                <input type="radio" name="respuesta-20" id="B20"  value="b">
                <label for="B20">B</label>
        
                <input type="radio" name="respuesta-20" id="C20"  value="c">
                <label for="C20">C</label>

                <input type="radio" name="respuesta-20" id="D20"  value="d">
                <label for="D20">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 21 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-21" id="A21"  value="a">
                <label for="A21">A</label>
        
                <input type="radio" name="respuesta-21" id="B21"  value="b">
                <label for="B21">B</label>
        
                <input type="radio" name="respuesta-21" id="C21"  value="c">
                <label for="C21">C</label>

                <input type="radio" name="respuesta-21" id="D21"  value="d">
                <label for="D21">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 22 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-22" id="A22"  value="a">
                <label for="A22">A</label>
        
                <input type="radio" name="respuesta-22" id="B22"  value="b">
                <label for="B22">B</label>
        
                <input type="radio" name="respuesta-22" id="C22"  value="c">
                <label for="C22">C</label>

                <input type="radio" name="respuesta-22" id="D22"  value="d">
                <label for="D22">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 23 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-23" id="A23"  value="a">
                <label for="A23">A</label>
        
                <input type="radio" name="respuesta-23" id="B23"  value="b">
                <label for="B23">B</label>
        
                <input type="radio" name="respuesta-23" id="C23"  value="c">
                <label for="C23">C</label>

                <input type="radio" name="respuesta-23" id="D23"  value="d">
                <label for="D23">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 24 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-24" id="A24"  value="a">
                <label for="A24">A</label>
        
                <input type="radio" name="respuesta-24" id="B24"  value="b">
                <label for="B24">B</label>
        
                <input type="radio" name="respuesta-24" id="C24"  value="c">
                <label for="C24">C</label>

                <input type="radio" name="respuesta-24" id="D24"  value="d">
                <label for="D24">D</label>
      </div> 
    </div>
    <div id="" class="col-12 frm-respuesta">
      <div class="numero-respuesta" > 25 </div>
      <div class="radio-respuestas"> 
                <input type="radio" name="respuesta-25" id="A25" value="a">
                <label for="A25">A</label>
        
                <input type="radio" name="respuesta-25" id="B25" value="b">
                <label for="B25">B</label>
        
                <input type="radio" name="respuesta-25" id="C25" value="c">
                <label for="C25">C</label>

                <input type="radio" name="respuesta-25" id="D25" value="d">
                <label for="D25">D</label>
         </div> 
        
      </form>
</form>
   



<script type="text/javascript" src="javaScript/tiempo.js"></script>

        <script src="..bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="..bootstrap/js/popper.min.js"></script>
    <script src="..bootstrap/js/bootstrap.min.js"></script>
        </body>
</html>