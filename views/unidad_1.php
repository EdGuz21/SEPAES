<!DOCTYPE html>
<html>
<head>
    <title>Lenguaje: Unidad 1</title>
    <link rel="stylesheet" type="text/css" href="css/unidades.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../controllers/JS/arriba.js"></script>
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body class="bg-white">
    <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='principal.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Unidades</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <button class="btn btn-primary" onclick="location.href='testL/test.php'">Iniciar Test</button>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <header>
        <center><h1 class="text-primary">Unidad 1: América Precolombina</h1></center>
        <div class="menu">
            <ul>
                <li class="subMenu">
                    <a>Culturas precolombinas (fundamentos cosmovisión y organización sociopolítica).<span></span></a>
                    <ul>
                        <li class="">
                            <div class="titulo-tema"><h3>Principales civilizaciones precolombinas de América</h3></div>
                            <div class="contenido">
                                <p>La palabra "precolombino" significa "antes de colón" por lo tanto civilizaciones precolombinas de América son aquellas civilizaciones que existían en América antes de la llegada de Cristóbal Colón. las más importantes son las azteca, la maya y la inca.</p>
                            </div>
                            <div class="titulo-tema subMenu">
                                <h5>La civilización maya.</h5>
                                <ul class="contenido">
                                    <p>De acuerdo con estudios arqueológicos, la cultura maya tuvo su origen en 1500 antes de Cristo, y su periodo clásico comprende del 300 al 900 después de Cristo. Durante este periodo construyeron grandes centros ceremoniales como uaxactún y tikal (Guatemala), demás se extendieron por muchas zonas de América. Por razones aún no comprendidas, los mayas abandonaron algunos centros ceremoniales A mediados del siglo IX. Algunos historiadores creen que fue por guerras e invasiones.</p>
                   
                                    <p> En la actualidad, los indígenas mayas se encuentran en parte de México, en la mayor parte de Guatemala y en regiones de Belice, Honduras y El Salvador, como lo muestra el mapa, tenis algo por ejemplo, tenemos mayas todavía. El pueblo más conocido, el maya propiamente dicho que da nombre a todo el grupo, ocupa la península de Yucatán, México.</p>
          
                                    <p>Los pueblos mayas formaban una sociedad muy jerarquizada, estaban gobernados por una autoridad política cuya dignidad era era hereditaria por línea masculina. Este delegaba la autoridad sobre las comunidades de poblados a jefes locales, que cumplían funciones civiles, militares y religiosas.</p>
           
                                    <p>La cultura maya produjo una arquitectura Monumental, de la que se conservan grandes ruinas en Palenque, Uxmal, Mayapán, Copán (Honduras), Tikal, Uaxactún, Quiriguá, bonampak y Chichén Itzá (Mexico), entre muchas otras, estos lugares eran enormes centros de ceremonias religiosas.</p>
                    
                                    <h6>La religión Maya</h6> 
                                    <p>Se centraba en el culto a un gran número de dioses de la naturaleza los que se distinguían por ser antropomorfos (forma humana), fitomorfos (forma de planta), zoomorfos (forma de animal) y forma astral. La figura más importante del panteón maya era Hunab Ku (Que significa un "solo Dios"), era el dios creador, señor del fuego y el corazón;  Itzamná el dios de la noche y el día, y era hijo de Hunab Ku; Kukulkán representaba al dios viento, y era llamado Gukumatz por los quichés, otros dioses eran: Kinich Ahua, el dios sol; Ixchel, la diosa luna.</p>
           
                                    <h6>Cosmogonía maya.</h6>
                                    <p>La cosmogonía Maya que encontramos en un libro titulado Popol vuh (libro de la comunidad) en este libro leemos que "en un principio todo estaba en suspenso, todo en calma" leemos ahí mismo la creación del hombre. De la creación se encargan los progenitores, entre los que se encuentran Gucumatz y Huracán, el corazón del cielo, además Ixpiyacoc e Ixmucané abuelos del Alba.</p>
                    
                                    <h6>El tiempo para los mayas.</h6> 
                                    <p>Para los mayas,el mundo finalizaría el sábado 22 de diciembre del 2012. según la primera profecía, en esta fecha el sol recibirá un rayo sincronizador del centro de la galaxia con el que se iniciará un nuevo ciclo. será el fin del mundo del materialismo y destrucción en que vivimos y el inicio de una nueva etapa de respeto y armonía.</p>
                                </ul>
                            </div>
                            
                        <div class="titulo-tema subMenu">
                            <h5>La civilización Azteca.</h5>
                            <ul class="contenido">
                                <p>El pueblo Azteca también llamado Méxica, fue de origen náhuatl y de carácter nómada, que sucedieron y vencieron a otros pueblos de ese mismo origen. como los chichimecas, toltecas y tepanecas. los aztecas fundaron la ciudad de Tenochtitlán o México, Qué significa "del lugar de Las garzas". Los aztecas dominaron el centro y el sur de México (en mesoamérica) desde el siglo XIV hasta el siglo XVI. son famosos por haber establecido un vasto imperio altamente organizado. Fueron destruidos por Los Conquistadores españoles y sus aliados mexicanos.</p>

                            <p>Los aztecas-mexicas ,en muy malas condiciones, consolidaron un imperio poderoso en sólo dos siglos; esto Gracias a su creencia en ciertas leyendas según la cual fundarían una gran civilización en una zona pantanosa en la que vieran un nopal (cactus) sobre una roca y sobre él un águila devorando una serpiente. los sacerdotes afirmaron haber visto todo eso al llegar a esta zona. hoy en día esta imagen representa el símbolo oficial de México que aparece, entre otros, en los billetes y monedas.</p>

                            <p>Al final del reinado Moctezuma II, en 1520 se habían establecido 38 provincias tributarias, estos pueblos tributarios luchaban por su independencia, de manera que estos conflictos internos facilitaron su derrota frente a Hernán Cortés en 1521, ya que muchos pueblos se aliaron con los españoles. Además de que el emperador Moctezuma cometió el error de instalar a Hernán Cortés en los mejores Palacios del imperio, desde donde se hicieron con la ciudad, es posible que lo hizo al interpretar antiguos presagios sobre el regreso del dios Quetzalcóatl o por su interés colmar de regalos a los españoles para que se retiraran.</p>

                            <p>La sociedad Azteca estaba dividida en tres clases: <b>esclavos, plebeyos y nobles.</b></p>

                            <h6>Esclavos</h6>
                            <p>el estado de esclavo era similar al de un criado contratado. aunque los hijos de los hombres podían ser vendidos como esclavos, solía hacerse un periodo determinado. los esclavos podían comprar su libertad, y los que lograron escapar de sus amos y llegar hasta el Palacio real sin que lo atraparan, obtenían la libertad inmediatamente.</p>

                            <h6>Plebeyos</h6>
                            <p>Los plebeyos se les otorgaba la propiedad vitalicia de un terreno en el que construye su casa pero a las capas más bajas no les permitía tener propiedades.</p> 

                            <h6>Nobleza</h6>
                            <p>la nobleza estaba compuesta por los nobles de nacimiento,los sacerdotes y los que se habían ganado el derecho a serlo (especialmente los guerreros).</p>

                            <p>Los sacrificios, humanos y de animales, eran parte integrante de la religión Azteca. para el guerrero el honor máximo conquista en caer en la batalla u ofrecerse como voluntario para el sacrificio en las ceremonias importantes. las mujeres que morían en el parto compartían el honor de los guerreros. también se realizaban las llamadas guerras floridas con el fin de conseguir prisioneros para el sacrificio.</p>

                            <h6>Mitología Azteca.</h6> 
                            <p>
                                Los aztecas eran politeístas, es decir, creían en muchos dioses. Tezcatlipoca era una de las deidades principales, señor del cielo y de la tierra. 
                                Quetzalcóatl, la serpiente emplumada, era considerado como "padre de los toltecas". Quetzalcóatl aparece enfrentarlo a Tezcatlipoca quien, según la leyenda, le hizo beber varios Tragos de pulque (bebida alcohólica), supuestamente beneficioso para su salud, pero quetzalcoatl, avergonzado por haber perdido su entereza se ocultó y finalmente desapareció, prometiendo que volvería (Moctezuma quizás creyó que Hernán Cortés era Quetzalcóatl) huitzilopochtli, dios de la guerra, representaba los dardos y lanzas del guerrero, la sabiduría y El poder. Tláloc, dios de la lluvia, que vivía en un paraíso de aguas llamados tlalocan, donde iban los que habían muerto en inundaciones, fulminados por un rayo en enfermos de hidropesía, y allí disfrutaban de una felicidad eterna. A tláloc le ofrecían niños y doncellas en sacrificio.
                            </p>
                            </ul>
                        </div>
                        <div class="titulo-tema subMenu">
                            <h5>La civilización inca.</h5>
                            <ul class="contenido">
                            <p>El vasto imperio inca (en quechua inka significa Rey o príncipe) se estableció En Los Andes en el siglo XV, muy poco antes de la conquista por los españoles, poseían un tipo de "escritura" muy rudimentario (el quipu).</p>
                            <p>Los incas, originariamente una pequeña y Billy y belicosa tribu del del Sur de las tierras altas de Perú, comenzaron, en torno al 1100 después de Cristo a desplazarse hacia el valle de Cusco o cuzco, conquistando e imponiendo tributos sobre los pueblos vecinos, con viracocha inca (siglo XV) se produce una gran expansión territorial, la cual continuó su hijo pachacútec inca yupanqui,  considerado por algunos historiadores como uno de los mayores conquistadores y gobernantes del mundo.Otra expacion se dio con Tupac Inca yupanqui hijo de pachacútec. El Imperio, no obstante, alcanzó su mayor extensión con el reinado del hijo de topa huayna cápac. Hacia 1525 el territorio bajo control inca se extendía por la zona más meridional del actual Colombia, por Ecuador, Perú y Bolivia, y por zonas del norte de Argentina y chile.</p>
                        </ul>
                        </div>
                        
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Características de la literatura precolombina.<span></span></a>
                    <ul>
                        <li>
                            <div class="" style="height='320'">
                                <iframe class="video" src="https://www.youtube.com/embed/85QbTe6MXG0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Figuras literarias de omisión: Elipsis, asíndeton, zeugma.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p>La elipsis, el asíndeton y el zeugma son figuras literarias de omisión, se las llama de omisión por que resulta de omitir una o más palabras.</p>
                            <div class="titulo-tema subMenu">
                                <h5><li>La elipsis.</li></h5>
                                <ul class="contenido">
                                <p>
                                    Esta figura literaria se forma cuando se omite, una frase u oración, una o más palabras sin alterar el sentido de la frase, un ejemplo es el siguiente: Ella habla castellano, pero el no. Si eliminamos la elipsis, la expresión queda así: Ella habla castellano, pero él no habla castellano. o también: Ella habla castellano pero él no habla. observemos que la elipsis evita la redundancia. otros casos de elipsis.
                                </p>
                                <h6>Ejemplos:</h6>
                                <p>
                                    -Armida llegó temprano a la escuela, pero felipe no.<br>
                                    -La nina coopero con el aseo, Antonio también.<br>
                                    -El chino dibujo un caballo, el japonés un coyote,el alemán una serpiente. <br>
                                    -A la niña le gusta dibujar, pero al niño no.<br>
                                </p>
                                </ul>
                            </div>
                            <div class="titulo-tema subMenu">
                                <h5><li>El asíndeton.</li></h5>
                                <ul class="contenido">                     
                                <p>
                                    Recordemos el polisíndeton. En la unidad 7 del primer año dijimos que el polisíndeton era una figura que podría considerarse de repetición: y consiste en dar repeticiones con el fin de dar más expresividad a la frase. Uno de los ejemplos, de aquella ocasión, era: no deseo ni verlo, ni escucharlo, ni recordarlo. El asíndeton por el contrario, consiste en eliminar conjunciones entre los términos que deben ir unidos. se usa mucho en el lenguaje literario y coloquial y produce un efecto de rapidez. Un ejemplo asíndeton muy conocido es la frase de Julio César: Veni, vidi, vici (vine, vi, venci). Esta frase equivale a la siguiente: Vine, vi y vencí.observamos la conjunción y recordemos que una conjunción es una palabra , o conjunto de ellas, que enlaza oraciones o palabras. De hecho, según su etimología, conjunción significa “conjuntar”: por la tanto significa “que enlaza o que une con”.<br> 
                                    Son conjunciones: y, e, ni, que, mas, pero, aunque, sin embargo, empero, con todo, a pesar de, no obstante, más bien, sino, sino que, o, u. Las conjunciones mas usadas en la lengua coloquial son: y, pero, sino, o, ni.
                                </p>
                                <h6>Veamos otros casos de asíndeton:</h6>
                                <p>
                                    -No sabe tocar guitarra, toca piano. = esta expresión equivale a: no sabe tocar guitarra, pero toca piano.<br>
                                    -Volvió a fracasar en el negocio, se lo advirtieron. = esta expresión equivale a: volvió a fracasar en el negocio aunque se lo advirtieron.<br> 
                                    -No quiero agua, fresco, jugo. = esta expresión equivale a: no quiero agua, ni fresco, ni jugo.
                                </p>
                            </ul>
                            </div>
                            <div class="titulo-tema subMenu">
                                <h5><li>El zeugma.</li></h5>
                                <ul class="contenido">
                                <p>Hay zeugma cuando se omite una palabra, pero que se sobre enetiendo que está expresada.</p>
                                <h6>Veamos algunos casos de zeugma:</h6>
                                <p>
                                    - “Era de complexión recia, seco de carnes, enjuto de rostro, gran madrugador y amigo de la caza” en el periodo anterior, extraído de Don Quijote de la Mancha, la palabra era se ha omitido en los otros mienbros del periodo, Sin esta omisión, el periodo quedaria asi: Era de complexión recia, era seco de carne, era enjuto de rostro, eran gran madrugador y era amigo de la caa. Observemos que sin el zeugma, el periodo pierde belleza literaria. Adquiere la lectura cierto grado de pesadez.<br> 
                                    -”Tengo dedos de seda, palmas de coco, dos dedos de frente y frente popular” En el periodo anterior, extraído de la novela, Un año nefando ( de Felipe Guzman).<br>
                                    La palabra tengo se omitido en los otros tiempos del periodo. sin esta omisión el periodo quedaria asi: Tengo dedos de seda, tengo palmas de coco,tengo dos dedos de frente y tengo frente popular. observen que sin el zeugma, el periodo pierde belleza literaria, adquiriendo la lectura cierto grado de pesadez.<br>
                                    -Era de avanzada edad, de mediana inteligencia y de escasa belleza.<br> 
                                    -Juan repara radios, Ana licuadoras, Pedro carros y Guadalupe bicicletas
                                </p>
                            </ul>
                            </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El lenguaje, la lengua y el habla.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p> 
                                El lenguaje es la capacidad humana para comunicarse mediante signos orales (y escritos); la lengua es un código de signos(imágenes acústicas y gráficas  en la memoria) que permite entender el mensaje.<br>
                                Habla es la manifestación de  dicho código. Más específicamente, el lenguaje es el medio de comunicación entre los seres humanos por medio de signos orales y escritos que poseen un significado.<br>
                                En un sentido más amplio se entiende por lenguaje cualquier procedimiento que sirva para comunicarse.
                            </p>
                            <p>
                                También podemos decir que la lengua es un conjunto ordenado y sistemático de formas orales, escritas y grabadas qué sirven para la comunicación entre las personas que constituyen una comunidad lingüística.<br><br>
                                Hablando de una manera informal puede decirse que es lo mismo que idioma, aunque este último término tiene más significado de lengua oficial o dominante de un pueblo o nación. Es por esto que a veces resultan sinónimos las expresiones lengua española o idioma español.
                            </p>
                            <p> Desde un punto de vista  científico, a partir de Ferdinand de saussure, se entiende por lengua el sistema de signos orales y escritos del que disponen los miembros de una comunidad para realizar los actos lingüísticos cuando hablan y escriben. La lengua es un inventario que los hablantes emplean a través del habla.</p>
                            <p> En cuanto al habla diremos Qué es el uso individual que cada persona hace de la lengua. Habla es propia de los seres humanos. Cuando una persona expresa algo oralmente o por escrito, está realizando un acto de habla.</p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Uso de la tilde en las palabras exclamativas, interrogativas y vocablos compuestos.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Recordemos que existen dos acentos: el acento prosódico y el acento ortográfico. también llamado tilde. Todas las palabras llevan acento prosódico, pero no todas llevan tilde. El acento denota la intensidad de la voz. en las palabras siguientes se señala la sílaba que lleva la mayor intensidad de voz: torreja, camioneta, dormilón, mariposa, público, diagnóstico, vender, alimentar, conseguir, naranja, camaronero...
                                </p>
                            </div>
                            <div class="titulo-tema subMenu">
                                <h5>Existe una tilde denominadas tilde diacrítica.</h5>
                                <ul class="contenido">
                                <p> 
                                    La tilde diacrítica se utiliza para diferenciar ciertas palabras de otras de igual escritura pero con diferente categoría gramatical. para el caso tenemos que como puede llevar tilde o no llevarla. En general las partículas interrogativas y exclamativas (de admiración) llevan siempre tilde diacrítica. estas partículas son: qué, quién, quiénes, cuál, cuáles, cuánto, Cuánta, cuántos, cuántas, dónde, cuándo, cómo. con la tilde, estas palabras se distinguen de los pronombres y adjetivos relativos correspondientes.
                                </p>
                                <h6>A continuación te presentamos una serie de frases en las que encontrarás las partículas anteriores con tilde o sin ella.</h6>
                                <p class="font-weight-bold text-center">
                                !Qué lindo está el día!<br>
                                Que lo pinté cuanto antes<br>
                                No sé quién ganó<br>
                                Volvió cuál desconocido<br>
                                ¿Dónde está?<br>
                                !Cuánto deseo estudiar¡<br>
                                vive como un rey<br>
                                ¿Quiénes son los ganadores?<br>
                                Descubriré cuál es la clave<br>
                                Que vuelva cuando quiera<br>
                                Que me importa la fiesta<br>
                                ¿Cuánto cuesta la casa?<br>
                                Ellos son quienes ganaron<br>
                                Estaré donde mi hija<br>
                                No sé cuándo llegará<br>
                                Quiero que lo pinté<br>
                                No sé cómo pintar<br>
                                Vi quien lo tomó<br>
                                </p>
                            </ul>
                            </div>
                            <div class="titulo-tema subMenu">
                                <h5><li>Uso de la tilde en los vocablos compuestos.</li></h5>
                                <ul class="contenido">
                                <p>
                                    Existen vocablos que se forman con dos palabras unidas por un guión. estos vocablos se denominan vocablos compuestos. En estos casos cada palabra conserva su tilde, de manera que puede ocurrir que haya dos tildes, estos casos no son muy abundantes veamos algunos: teórico-práctico, Franco-italiano, físico-químico, político-militar, físico-matemático, músico-poeta, austro-húngaro, árabe-israelí, histórico-geográfico, hispano-alemán.
                                </p>
                                <p>
                                    Las palabras unidas porción son en realidad palabras en formación. ocurre que con el tiempo pierden el guión.<br> 
                                    Las palabras compuestas que no llevan - se consideran palabras simples para efectos de tildación; por lo tanto se tildan conforme las reglas de acentuación (atendiendo a su sonoridad): parabién, puntapié, paracaídas, cortacesped, geometría, hazmerreír, espantapájaros, balompié, cortauñas, decimoquinto... En todo caso, aquí está una regla sencilla.
                                </p>
                                <p>
                                    Si un vocablo simple es el primer elemento en una palabra compuesta perderá
                                    la tilde (si originalmente la tiene). veamos unos casos. <br>
                                    ➢ Décimo + quinto = decimoquinto. la palabra décimo a perdió su tilde. <br>
                                    ➢ Físico + químico = fisicoquímico. la palabra físico a perdió su tilde.
                                </p> 
                                <p>
                                    Una excepción son los adverbios terminados en mente: comúnmente, Próximamente. En estos ejemplos las palabras común y próxima conservan su tilde.<br>
                                    Por otra parte, cuando la segunda palabra no lleva tilde originalmente, entonces la palabra compuesta aparece tilde; tal es el caso de guardarropa o pararrayos.
                                </p>
                            </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Los textos virtuales y su uso responsable.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p>Los textos virtuales. La palabra virtual significa "implícito". También podemos interpretar dichos palabras como "aparente". En nuestros días la palabra virtual se aplica a los sistemas cibernéticos (las computadoras); más específicamente a la internet (red). Así es como hablamos de enciclopedias virtuales y de libros virtuales. Es decir, de enciclopedias y de libros aparentes. Por lo tanto,  el texto de esos libros, enciclopedias, revistas o periódicos, es un texto virtual. </p>
                            <p>Es increíble como en un disco Compacto podemos guardar cientos de libros. Pero no sólo libros, también podemos Guardar imágenes, cómo Películas y fotografías. Por ejemplo, puedes guardar en un disco compacto cientos de imágenes de museos, capitales del mundo, mares, ríos, lagos... Pero todo eso es una realidad virtual que es, desde luego, está muy bien ajustada a la realidad.</p>
                            <div class="titulo-tema"><h3><li>Uso responsable de los textos virtuales.</li></h3></div>
                            <div class="contenido">
                                <h6>¿Podemos confiar ciegamente en los textos encontramos en la internet?</h6>
                                <p>... Es increíble la cantidad de información que, sobre un tema determinado, podemos encontrar en la red. Mucha de esa información es basura, mientras que también encontramos información confiable. ¿ cómo discriminar una información encontrada en la red? En otras palabras, ¿ que no puede asegurar que determina la información es confiable? Aquí conoceremos algunos parámetros que nos pueden servir de base para presumir que determinada información es confiable. Estos parámetros conforman un análisis crítico del discurso virtual.</p>
                            </div>
                            <div class="titulo-tema"><h3><li>La ortografía.</li></h3></div>
                            <div class="contenido">
                                <p>Quiero bajar determinar información por la internet que das cuenta que presenta errores de ortografía, puede sospechar tu información más confiable. La base de tal  sospecha es la siguiente: Si alguien no se cuida de la ortografía(y redacción en general) podemos sospechar que tampoco se cuido de representar una información verdadera.</p>
                            </div>
                            <div class="titulo-tema"><h3><li>Incongruencia del texto con conocimientos previos.</li></h3></div>
                            <div class="contenido">
                                <p>Casi siempre, cuando buscamos información sobre un tema, sin conocimientos previos poseemos sobre el mismo. Si el sexo virtual encuentras un gato que está seguro de que es falso, puedes sospechar que el texto en general posee más datos erróneos; por lo tanto puedes despertarme y estar por otra fuente.</p>
                            </div>
                            <div class="titulo-tema"><h3><li>Comparación de textos.</li></h3></div>
                            <div class="contenido">
                                <p>Y al comparar dos textos sobre un tema no hay contradicción entre los datos que presentan, podemos presumir que el texto es confiable. Pero si al comparar los difieren en los datos, entonces podemos sospechar que uno de los dos son falsos. Esto nos lleva a buscar más información en otra cuenta.</p>
                            </div>
                            <div class="titulo-tema"><h3><li>Confiabilidad de la Fuente.</li></h3></div>
                            <div class="contenido">
                                <p>La experiencia nos dice que algunas Fuentes son más confiables que otras; es decir que ciertas páginas virtuales son más responsables al presentar información. Busquemos la información que necesitamos en estas fuentes. </p>
                            </div>
                        </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <script src="../controllers/JS/menu1.js"></script>
    <script src="bootstrap/css/bootstrap.min.css"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>