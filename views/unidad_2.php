<!DOCTYPE html>
<html>
<head>
    <title>Lenguaje: Unidad 2</title>
    <link rel="stylesheet" type="text/css" href="css/unidades.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../controllers/JS/arriba.js"></script>
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body class="bg-white">
    <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='principal.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Unidades</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <button class="btn btn-primary" onclick="location.href='testL/test.php'">Iniciar Test</button>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <header>
        <center><h1 class="text-primary">Unidad 2: Descubrimiento(conquista y la colonia)</h1></center>
        <div class="menu">
            <ul>
                <li class="subMenu">
                    <a>La literatura del descubrimiento y conquista de América.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p>
                                La expansión atlántica de Europa. Los españoles descubrieron América. Con el paso del tiempo, todo el continente fue llenándose de españoles y su cultura. México y Lima, las capitales de los virreinatos de Nueva España y Perú, respectivamente, se convirtieron en los centros de toda la actividad intelectual del siglo XVII. 
                            </p>
                            <p>
                                La mezcla de sátira y realidad que dominaba la literatura española llegó también al Nuevo Mundo, y allí aparecieron, entre otras obras, la colección satírica Diente del Parnaso, del poeta peruano Juan del Valle Caviedes, y la novela Infortunios de Alonso Ramírez (1690), del humanista y poeta mexicano Carlos de Sigüenza y Góngora.
                            </p>

                            <iframe class="video" src="https://www.youtube.com/embed/0bZhsGOl_tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>La literatura colonial de América.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p> 
                                    La sociedad colonial americana. La sociedad colonial de América es una mezcla de español e indio. Se generan aquí ciertas peculiaridades en lo relativo a la literatura. Aunque la novela de caballería tuvo gran auge en España, en América se prohibiría tanto su lectura como su escritura. Por esta razón aparece muy tardíamente: en el siglo XVIII. Sin embargo la poesía sí es cultivada. Así como un nuevo grupo de crónicas distintas a las de la conquista. 
                                </p>
                              <iframe class="video" src="https://www.youtube.com/embed/WQNJZ6qQFQI?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                          </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Fonemas, sonidos y letras.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Fonología y fonética. Los fonemas son los sonidos ideales que todos guardamos en la mente. Para el caso, se tiene que todos nosotros (excepto los sordomudos) guardamos en la mente el sonido ideal viento. Sin embargo, al pronunciar con nuestra boca el sonido viento estamos materializando dicho fonema. Es decir que el sonido es la materialización del fonema.
                                </p>
                               <iframe class="video" src="https://www.youtube.com/embed/MOu5AEtAoMc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Oposiciones fonológicas y rasgos distintivos.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Entre la gran variedad de sonidos que puede emitir un hablante, es posible reconocer los que representan el mismo sonido, aunque las formas de pronunciarlo resulten distintas desde el punto de vista acústico; a la vez se pueden distinguir los sonidos que señalan una diferencia de significado. Cada vez que se emite una palabra, no se realiza de la misma manera, porque cada emisión depende de los otros sonidos que la rodean. Los sonidos adquieren valores distintos según la función que ocupen en un contexto dado, sin embargo existen unos rasgos que no varían y que permiten reconocerlos sin confusiones en cualquier posición. Por otro lado, los sonidos que componen una palabra son las unidades mínimas que la hacen diferente de otra. Una prueba sencilla que lo demuestra es la comparación de lo que se llama segmentos portadores de significado de los llamados pares mínimos: los sonidos que forman la palabra más pueden ser sustituidos por otros y al hacerlo se forman palabras diferentes: vas, mes y mar. Por este procedimiento se pueden aislar las unidades mínimas que distinguen los significados, es decir, los fonemas.
                                </p>
                             
                                <p>
                                    Cada fonema se describe siguiendo unos criterios físicos y articulatorios, en función del punto de articulación o de su carácter de sonoro o sordo. Cada uno de los componentes que define un sonido es un rasgo distintivo. /mas/ es distinto de /vas/ en función de los fonemas /m/ y /b/; se definen, /m/ como [+bilabial], [+sonoro], [+nasal]; y /b/, como [+bilabial], [+sonoro], [-nasal]; el único rasgo que los diferencia es la condición de nasalidad. Lo mismo podría hacerse al comparar /a/ y /e/, /s/ y /r/ y cuantas oposiciones revelen sonidos diferentes. Por rasgos distintivos se describen todos los sonidos que constituyen una lengua. La teoría de los rasgos distintivos se formuló en primer lugar dentro de la escuela estructuralista; está incorporada a la teoría generativa que trata de construir una explicación fonológica dentro de la teoría general de la gramática.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Las consonantes.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    Existen en español dos clases de sonidos: vocálicos y consonánticos. Cuando al salir el aire procedente de los pulmones, tras pasar por la tráquea, laringe y las cuerdas vocales hacia el exterior, no encuentra ningún obstáculo en la cavidad bucal se produce un sonido vocálico: /a/, /e/, /i/, /o/, /u/; cuando, por el contrario, la columna de aire encuentra algún obstáculo, el sonido es consonántico: /b/, /g/, /m/… Las vocales presentan una mayor abertura de los órganos articulatorios que las consonantes y un mayor número de vibraciones de las cuerdas vocales. Las vocales pueden formar sílabas, mientras que las consonantes necesitan de una vocal para hacerlo.
                                </p>
                              
                                <p>
                                    Las consonantes pueden clasificarse atendiendo varios criterios: por el punto de articulación, por el modo de articulación y atendiendo a la vibración o no de las cuerdas vocales.
                                </p>

                                <div class="titulo-tema subMenu">
                                    <h5>Punto de articulación</h5>
                                    <ul class="contenido">
                                        <p>
                                            Por el punto de articulación (zona en la que un órgano activo entra en contacto con otro pasivo o activo, produciéndose un estrechamiento o cierre en el canal) los sonidos pueden ser: bilabiales, labiodentales, interdentales, dentales y palatales.
                                        </p>
                                    
                                        <p>    
                                            Los sonidos bilabiales se articulan uniendo los labios para impedir momentáneamente la salida del aire por la boca. Son sonidos bilabiales p, b y m. Para producir sonidos labiodentales se unen los incisivos superiores al labio inferior, tal como ocurre en la f. En los sonidos interdentales la lengua se sitúa entre los dientes superiores e inferiores: z. En los dentales o linguodentales el ápice de la lengua se coloca en la parte interior de los incisivos superiores: t y d. En las palatales la lengua se apoya en el paladar, como ocurre con las consonantes r, rr y la ñ; y con la vocal i.
                                        </p>
                                    </ul>
                                </div>
                                <div class="titulo-tema subMenu">
                                    <h5>Modo de articulación</h5>
                                    <ul class="contenido">
                                        <p>
                                            Por el modo de articulación (forma especial de realización de cada sonido, independientemente del punto de articulación), los sonidos son: oclusivos, fricativos y africados, líquidos y no líquidos.
                                        </p>

                                        <p>
                                            Los sonidos oclusivos son también llamados explosivos o momentáneos. Se producen cuando para su articulación se cierra por un instante la salida al aire con los labios, la lengua y el paladar, para abrirla súbitamente, produciéndose una pequeña explosión causada por el aire acumulado. Producen sonidos oclusivos las consonantes p, t, k, b, d y g.
                                        </p>

                                        <h6>Los sonidos fricativos</h6>
                                        <p> 
                                            Se producen si se acercan los órganos articulatorios mucho, pero no llegan a obstruir totalmente la salida al aire. Producen sonidos fricativos las consonantes y, f, s, g y j. Reciben igualmente el nombre de espirantes, constrictivas o continúas.
                                        </p>

                                        <h6>Los sonidos africados o semioclusivos</h6>
                                        <p>
                                            Se generan cuando momentáneamente se produce una interrupción en la salida del aire (momento oclusivo), para pasar gradualmente hacia una fricación. Se produce con ch.
                                        </p>

                                        <h6>Los sonidos líquidos</h6>
                                        <p>
                                            Tienen a la vez rasgos comunes con las vocales y las consonantes; dentro de ellos se distinguen: laterales, cuando el aire sale por uno o por los dos laterales de la lengua: l y ll; y vibrantes, si la punta de la lengua vibra en el momento de su pronunciación: r y rr. Todas las demás consonantes son no líquidas.
                                        </p>
                                    </ul>
                                </div>
                                <div class="titulo-tema subMenu">
                                    <h5>Atendiendo a la vibración o no de las cuerdas vocales</h5>
                                    <ul class="contenido">
                                        <p>
                                            Atendiendo a la vibración o no de las cuerdas vocales, los sonidos son: sonoros y sordos. Son sonoros si al pasar el aire a través de las cuerdas vocales éstas están tensas y la presión del aire las hace vibrar con mucha rapidez: m y d; si no vibran, los sonidos son sordos: f y t.
                                        </p>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Las vocales.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p>
                                El español presenta cinco fonemas vocálicos: a, e, i, o, u. Las vocales pueden clasificarse conforme al lugar de articulación y al modo de articulación. Conforme al lugar de articulación tenemos vocales anteriores: e, i; vocal media: a; y posteriores: o, u. Las vocales e, i reciben también el nombre de palatales por articularse en la zona del paladar duro; las vocales o, u son llamadas velares por articularse en la zona del velo del paladar; estas últimas están labializadas, aunque la o en menor grado que la u.
                            </p> 
                            <p>
                                Conforme al modo de articulación (abertura de la boca en el momento de articularlas) la vocal puede ser abierta (o alta), si la lengua se encuentra bastante alejada de la bóveda palatal. Es el caso de la a. La vocal puede ser semicerrada o semiabierta, si la lengua se encuentra separada de la cavidad palatal. Es el caso de la e y la o. Por último, la vocal puede ser cerrada (o baja), si la lengua permanece muy próxima a ésta. 
                            </p>
                        </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>La oración simple.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Estructura y funciones del sintagma nominal. Recordemos que una oración puede ser dividida en bloques con cierto sentido si se les considera aisladamente. Estos bloques son conocidos como sintagmas. Analicemos la oración siguiente:
                                </p>
                                <p>
                                    Yo visité a mi novia Virginia en el parque el dos de junio a las tres de la tarde En la anterior oración encontramos cinco sintagmas. Estos son: Yo visité a mi novia Virginia en el parque el dos de junio a las tres de la tarde.
                                </p>
                               
                                <p>
                                    <iframe class="video" src="https://www.youtube.com/embed/K_bFNniZh0M?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <script src="../controllers/JS/menu1.js"></script>
    <script src="bootstrap/css/bootstrap.min.css"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>