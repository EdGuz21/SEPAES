<!DOCTYPE html>
<html>
<head>
    <title>Lenguaje: Unidad 3</title>
    <link rel="stylesheet" type="text/css" href="css/unidades.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../controllers/JS/arriba.js"></script>
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body class="bg-white">
    <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='principal.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Unidades</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <button class="btn btn-primary" onclick="location.href='testL/test.php'">Iniciar Test</button>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <header>
        <center><h1>Unidad 3: literatura americana: romantismo y modernismo.</h1></center>
        <div class="menu">
            <ul>
                <li class="subMenu">
                    <a>El romanticismo en América.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Las jóvenes repúblicas latinoamericanas. Latinoamérica es, en sentido amplio, todo el territorio americano al sur de Estados Unidos. En sentido más estricto comprende todos los países que fueron colonias de España, Portugal y Francia. Dado que los idiomas de estos países provienen del latín, el término Latinoamérica ha servido para designar a las naciones que fueron sus colonias en el Nuevo Mundo. Las repúblicas latinoamericanas son relativamente jóvenes (América fue descubierta en 1492). Entre las nuevas tierras conquistadas, el principal elemento unificador en cuanto a organización fue la Iglesia católica. A través del clero la población indígena adquirió una nueva religión, a la vez que se les construía hospitales y otras instituciones caritativas. Tanto españoles como portugueses se mezclaron con los aborígenes, dando lugar a una nueva raza: los mestizos.  
                                </p>
                                <p>
                                    Pasado algún tiempo, en el siglo XIX, Latinoamérica busca su independencia. La filosofía de la Ilustración y la difusión de las ideas liberales tuvieron gran influencia sobre las clases altas de las colonias, pero fue la invasión napoleónica (1807-1808) de la península Ibérica (España y Portugal) la que actualizó las ideas de independencia de Latinoamérica. Hacia 1825, toda la América española, excepto Cuba y Puerto Rico, se había independizado de la metrópoli, dando lugar a la proclamación de repúblicas criollas. Nacen así las repúblicas latinoamericanas independientes. Pero con la independencia no se acaban las penas: la población rural continuaría viviendo en la más profunda pobreza y opresión.
                                </p>
                                <iframe class="video" src="https://www.youtube.com/embed/rutuGppBNfk?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El modernismo hispanoamericano.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Las sociedades americanas en la puerta del nuevo siglo XX. Recordemos que las luchas de independencia de la mayor parte de las repúblicas de América se desarrollaron durante el primer cuarto del siglo XIX. La mayor parte del siglo XIX se caracterizó por las luchas entre liberales y conservadores, especialmente en Centroamérica. Los caudillos habían tomado dos rutas: unos estaban del lado del liberalismo y otros estaban del lado del conservadurismo. Estas luchas estaban encaminadas al control del poder político más que a la implantación de un sistema de gobierno. A finales del siglo XIX las sociedades americanas habían entrado completamente a la vida liberal. Como anhelo de sistema de gobierno, era indiscutible que los pueblos americanos deseaban que imperase un sistema liberal sobre el tradicional conservadurismo. En este sentido debe entenderse que el liberalismo había triunfado sobre el conservadurismo.
                                </p>
                                <iframe class="video" src="https://www.youtube.com/embed/p1dwb1mVGMc?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Rasgos suprasegmentales.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Cuando hablamos con alguien resulta fácil determinar si nos está preguntando algo o si está irritado o contento. Todo esto lo sabemos por medio de la entonación con que se expresa. Pero por escrito la entonación deberá expresarse de diferente manera. Lo hacemos mediante los rasgos suprasegmentales. Los rasgos suprasegmentales son: la tilde, los signos de interrogación y admiración.Analiza la conversación siguiente.
                                </p>
                                <p class="font-weight-bold text-center">
                                    ▬ ¿Dónde está tu hermano? ─preguntó el padre a su hijo.<br>
                                    ▬ Está donde el primo Juan… Están terminando una tarea de química─ contestó el muchacho.<br>
                                    ▬ ¿Con el primo Juan? El primo Juan es mala compañía… ¡Maldición! ─exclamó muy irritado el padre.<br>
                                    ▬ El dijo que como el primo Juan sabe mucho de química; terminará rápido la tarea ─respondió el muchacho.<br>
                                    ▬ ¡Mentiras! Ese muchacho no es más que un callejero. ¡Callejero! ─exclamó─. ¿Es buena compañía el primo Juan?... ¡Claro que no! ¿Cuándo regresará?<br>
                                    ▬ No lo sé.<br>
                                    ▬ Cuando regrese, me lo haces saber ─dijo el padre.<br>
                                </p>
                                <p>
                                    En la conversación anterior podemos apreciar cuándo el padre está preguntando y cuándo está enojado. Esto lo conseguimos por medio de los rasgos suprasegmentales.
                                </p>
                            
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Acento y tilde.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Ya sabemos que existen dos acentos: el acento prosódico y el acento ortográfico, también llamado tilde. Todas las palabras llevan acento prosódico, pero no todas llevan tilde. En las palabras siempre existe una sílaba tónica, las demás son átonas. En las palabras siguientes se señala la sílaba tónica (la que lleva la mayor intensidad de voz): colega, camiseta, camastrón, mariposa, público, diagnóstico, vender, alimentar, conseguir, naranja, camaronero...
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Entonación.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    En la escritura, la entonación la conseguimos mediante los signos de interrogación y admiración. Aunque basta un signo de entonación al principio y otro al final, algunos escritores utilizan dobles o triples para enfatizar. Leamos la siguiente conversación:
                                </p>
                              
                                <p class="font-weight-bold text-center">
                                    ▬ ¿Cómo está tu perro?... ¿Lo llevaste con el veterinario?─preguntó el tío.<br>
                                    ▬ Sí… Lo llevé.<br>
                                    ▬ ¿Qué dijo el médico? ¡Dímelo! ─exige el tío.<br>
                                    ▬ Está muy enfermo… No come… ¡Y sufre fuertes dolores murió!<br>
                                    ▬ ¡Pobre Caramelo! Buscaremos otro médico. Uno bueno. ¡Ya verás que se curará!<br>
                                    ▬ ¡Salve a Caramelito, tío! ¡¡Lo quiero mucho!! ─dijo la niña y se echó a llorar.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Síntesis y esquema.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    Días después de leer un libro ocurre con frecuencia que hemos olvidado buena parte de él. Esto sucede porque a menudo no conseguimos diferenciar los puntos más importantes durante la lectura. Para obtener el mejor provecho de una lectura es necesario valernos de la síntesis y el esquema. El esquema puede considerarse como la armazón de la lectura, que contiene los puntos más importantes. Un ejemplo de esquema es el índice. Es por medio del índice que nos enteramos del contenido del libro; es decir, de la idea central (o ideas centrales) de la lectura. A la hora de leer, el esquema lo conseguimos subrayando lo que consideramos que forma parte de la idea central. Una vez que tenemos el esquema (el esqueleto), procedemos a sintetizar (resumir) lo leído.
                                </p>
                                <iframe class="video" src="https://www.youtube.com/embed/xeeWiOzTO5A?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Ortografía.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <iframe class="video" src="https://www.youtube.com/embed/WOWUPoQVg44?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <script src="../controllers/JS/menu1.js"></script>
    <script src="bootstrap/css/bootstrap.min.css"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>