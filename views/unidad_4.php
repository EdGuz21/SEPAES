<!DOCTYPE html>
<html>
<head>
    <title>Lenguaje: Unidad 4</title>
    <link rel="stylesheet" type="text/css" href="css/unidades.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../controllers/JS/arriba.js"></script>
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body class="bg-white">
    <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='principal.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Unidades</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <button class="btn btn-primary" onclick="location.href='testL/test.php'">Iniciar Test</button>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <header>
        <center><h1>Unidad 4: literatura americana: el realismo regionalista, critico y social.</h1></center>
        <div class="menu">
            <ul>
                <li class="subMenu">
                    <a>Peculiaridades de la poesía latinoamericana.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p>Una nueva realidad frente a la realidad mecanizada y convulsa del nuevo siglo. Nuestros pueblos latinoamericanos, desde México hacia el sur, se han caracterizado por las injusticias sociales. Injusticias derivadas, en parte, por gobiernos con poco o ningún interés por mejorar las condiciones de vida de la población. Estas condiciones de inestabilidad social se vuelven fácilmente un caldo de cultivo para doctrinas socialistas y humanistas, doctrinas que atraen a sus adeptos con el ofrecimiento de un mundo mejor, un mundo libre de injusticias y de prosperidad económica para todos. Estas doctrinas, junto con nuevas corrientes estéticoliterarias, hacen surgir en nuestra América la denominada poesía realista-social. Un representante de mucho peso de esta poesía es Pablo Neruda. También deben ser mencionados otros como Vicente Huidobro, Octavio Paz, César vallejo, Nicanor Parra… Estos poetas suavizan la situación convulsa del nuevo siglo. Un nuevo siglo maquinista y mecanicista, producto del avance vertiginoso de la industrialización.</p>
                            </div>
                            <div class="titulo-tema"><h5>El influjo de las vanguardias.</h5></div>
                            <div class="contenido">
                                <p>
                                    La poesía latinoamericana ha sido influenciada en buena medida por las corrientes vanguardistas (ultraísmo, creacionismo, cubismo, surrealismo...), y muy directamente por los escritores españoles.
                                </p>
                                <p>
                                    Recordemos que en literatura el término Vanguardismo o Literatura de Vanguardia se refiere a un conjunto de movimientos ismos (tendencia de orientación innovadora, principalmente en las artes, que se opone a lo ya existente) surgidos durante las primeras décadas del siglo XX, especialmente después de la primera guerra mundial. Dichos movimientos surgen como una reacción contra el racionalismo y contra las corrientes realistas. El Vanguardismo aparece en las letras europeas, ejerciendo en ellas gran influencia hasta fines de la primera mitad del siglo XX.
                                </p>
                                <p>
                                    Los autores vanguardistas, en sus creaciones, gozan de una amplia libertad. El uso del absurdo carece de límites. Esto genera que los autores y las obras vanguardistas presenten muchas variantes y aspectos que impiden una delimitada clasificación. Sin embargo, existen entre los vanguardistas ciertas características comunes.
                                </p>
                                <p>
                                    Pero la poesía latinoamericana adquiere peculiaridades formales, tal es el verso libre; en el que aparece el ritmo interno frente a la métrica clásica.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Los fundadores de la poesía latinoamericana contemporánea.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    En esta sección estudiaremos la figura y la obra de los principales fundadores de la nueva poética latinoamericana.
                                </p>
                                <iframe class="video" src="https://www.youtube.com/embed/I1GBsWm_ANA?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El concepto de norma lingüística y de corrección lingüística.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Hemos aprendido que la lengua es un sistema de signos y que el habla es el uso concreto y personal de dichos signos. Por lo tanto hay una diferencia entre lengua y habla. Entendamos esto. Los argentinos, mexicanos, salvadoreños... tenemos una misma lengua: el español; sin embargo, la entonación y el uso exclusivo de ciertas palabras distinguen a unos de otros. Esto se debe a que, aunque poseemos la misma lengua, poseemos distinta habla. Por lo tanto, hay una mediación social que marca la diferencia entre unos y otros. A esto le llamamos norma lingüística.
                                </p>
                               
                                <p>
                                    Por lo tanto se tiene que la norma lingüística es la mediación social que determina cómo, en qué circunstancias, con qué connotaciones, con qué énfasis, etc. debemos de utilizar las palabras
                                </p>
                                <p>
                                    La norma lingüística determina la pronunciación de una palabra o consonante de un grupo de personas en una sociedad específica. Debe quedar claro que esta pronunciación responde a factores culturales, y nada tiene que ver con una pronunciación correcta o incorrecta. Entendamos esto. Todos conocemos la forma peculiar con que algunos españoles pronuncian la z (y también la c y la s). Es una pronunciación distinta a la de nosotros; pero ambas pronunciaciones son correctas, simplemente que obedecen a contextos sociales diferentes. Aquí simplemente ocurre que las normas regionales varían con respecto al español estándar. Aclaro esto porque algunas personas creen que la forma de pronunciación correcta es la de los españoles. NO: ambas son correctas. Hay quienes que, presumiendo de mucha cultura, tratan de imitar la entonación de los españoles. No se dan cuenta que caen en el ridículo.
                                </p>
                                <p>
                                    Es el momento propicio para hablar de la corrección lingüística. ¿Deberá corregir un grupo determinado su pronunciación hasta conseguir una semejante a la del español estándar? No hay razón para sujetarnos a los criterios de corrección “oficiales”, es decir, al que se considera correcto: el criterio español. No. Tampoco deben fijarse criterios de corrección con respecto a una norma que responde a necesidades comunicativas específicas de un contexto.
                                </p>
                                <div class="list-group">
                                    <ol class="list-group-item active">Por lo anterior se tiene que:</ol>
                                    <ol class="list-group-item">no estamos obligados a pronunciar la ce, la zeta y la ese como se pronuncian en algunas regiones de España.</ol>
                                    <ol class="list-group-item">podemos utilizar el vos en lugar del tú; aunque hay que señalar que el tú, en nuestro medio, se acepta como una forma intermedia entre el tú y el usted, en cuanto a confianza.</ol>
                                    <ol class="list-group-item">La consonante elle la podemos pronunciar como la ye. Esto significa que podemos decir gayo (gallo), y no estamos obligados a decir galyo.</ol>
                                </div>
                                <p>
                                    En resumen, se deben respetar las legítimas diferencias regionales; aunque exista una unidad lingüística sobre la base de un español estándar.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Las ventajas de una lengua común.<span></span></a>
                    <ul>
                        <li>
                             <div class="contenido" style="height='320'">
                                <p>
                                    Todas las personas que hablan el castellano, sin importar de qué país sean, podrán entenderse con suma facilidad. Esta es una de las ventajas de poseer una lengua común. Por supuesto que, por la entonación, podemos darnos cuenta si estamos hablando con un español, un argentino, un guatemalteco, un chileno... Pero, además de la entonación, existen otras diferencias, como lo son ciertas palabras de uso exclusivo llamados regionalismos. Por ejemplo, son los argentinos los que usan palabras como ché, boludo, pibe... Y en los mejicanos encontramos las palabras órale, pinche güey, cuate... Sumado a lo anterior, ocurre que el significado de una palabra puede cambiar de un país a otro. Sin ir muy lejos, tenemos que en Guatemala a los muchachos se les llama patojos.
                                </p>
                             
                                <p>
                                    Por otra parte, tenemos la pobreza de vocabulario que, dada su condición social, presentan algunas personas. Este es el caso de la gente del campo, que utilizan una lengua rural.
                                </p>

                                <p>
                                    Encontramos por otra parte una lengua culta y una lengua vulgar. La lengua culta es aquella que muestra un ajuste permanente a las normas de corrección. En cambio en la lengua vulgar observamos numerosas desviaciones de las normas de corrección. Así para el caso se tiene que es un vulgarismo decir maistro en lugar de maestro.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>La lengua oral y la lengua escrita.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    El lenguaje oral es el que se desarrolla por medio de la voz (lenguaje sonoro). El lenguaje escrito es el que utiliza signos (reunidos en el alfabeto). Aquí de nuevo volvamos a lo que es el habla. Dos personas pronuncian de diferente forma una determinada palabra, pero la escribirán de igual forma. La palabra blanco se escribirá siempre así.
                                </p>
                              
                                <p>
                                    Veamos algunas características de la lengua oral y de la lengua escrita. La lengua oral se aprende natural y fácilmente, se utiliza en combinación con otros códigos (gestos, entonación), las ideas se construyen en el mismo momento en que se expresan y regularmente no queda constancia de lo dicho. Para este último caso, el único testimonio de lo dicho es la memoria de nuestro interlocutor. Hay que tener en cuenta que cuando lo que se dice es de suma importancia, se buscan medios para constatarlo; en este caso se utilizan grabadoras de sonido.
                                </p>
                                <p>
                                    En la lengua escrita encontramos que requiere un proceso sistemático de aprendizaje, las ideas se construyen antes de escribirlas, es un proceso consciente y deja una constancia (lo escrito)
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El verbo.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <h5>¿Qué es un verbo?...</h5>
                                <p>
                                    Parte de la oración o del discurso que tiene formas personales adaptadas a las circunstancias de voz, modo, tiempo, número y persona; y formas no personales: infinitivo, gerundio y participio, con los caracteres del nombre, el adverbio y el adjetivo respectivamente.
                                </p>
                               
                                <iframe class="video" src="https://www.youtube.com/embed/3FDojt9wShs?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El párrafo.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    Imagínate un libro cuyo contenido no esté dividido en párrafos... Evidentemente su lectura se volvería incómoda y podría insinuar al lector que debe leerlo de un tirón: a tragarse todas las letras de golpe. Los párrafos son el resultado de la necesidad de dividir un texto con el fin de comprenderlo mejor. Pero debe quedar claro que los párrafos no son simples divisiones del texto de un libro. Su estructura obedece a criterios de contenido. Todo párrafo es en sí una especie de subtema; es decir que en él se consigna una idea que es parte de la idea general que se está tratando. De aquí resulta que se debe escribir un nuevo párrafo cuando ya una idea ha sido expuesta, aunque puede ocurrir que en el siguiente párrafo se continúe la misma idea o subidea. Por lo tanto, en un mismo párrafo no deben incluirse temas dispares; todo en él debe tener cohesión, de manera que cuando se aborde un nuevo perfil del tema principal, es la hora de abrir un nuevo párrafo, cerrando el actual con un punto y aparte. Esto significa que un párrafo está siempre ligado al anterior; no es un caso aislado.
                                </p>
                                <p>
                                    <center><h5>¿Qué tamaño debe tener un párrafo?</h5></center><br>
                                    El tamaño de un párrafo está relacionado con la idea que se trata; pero también con el estilo del escritor. Hay escritores que son partidarios de los períodos y de los párrafos largos; otros, en cambio, prefieren expresar sus ideas en forma breve, sin dejar de lado los puntos que estiman necesarios. Debe también tenerse en consideración el destinatario del libro. Es obvio que un libro de cuentos para niños debe estar estructurado con párrafos breves. En cambio un tratado de filosofía puede contener párrafos de más de una página.
                                </p>
                               
                                <p>
                                    Se acostumbra dejar un espacio mayor entre párrafos que entre líneas. Cuando no se hace esto, se acostumbra usar sangría.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <script src="../controllers/JS/menu1.js"></script>
    <script src="bootstrap/css/bootstrap.min.css"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>