<!DOCTYPE html>
<html>
<head>
    <title>Lenguaje: Unidad 5</title>
    <link rel="stylesheet" type="text/css" href="css/unidades.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../controllers/JS/arriba.js"></script>
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body class="bg-white">
    <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='principal.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Unidades</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <button class="btn btn-primary" onclick="location.href='testL/test.php'">Iniciar Test</button>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <header>
        <center><h1>Unidad 5: literatura americana: el realismo mágico.</h1></center>
        <div class="menu">
            <ul>
                <li class="subMenu">
                    <a>Sociedad y cultura en la primera mitad del siglo XX.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido subMenu">
                                <h5>Los grandes conflictos sociales y políticos.</h5>
                                <ul>
                                    <p>
                                        El siglo XX se ha caracterizado por grandes conflictos sociales y políticos; y además por muchos avances tecnológicos. Estos elementos, aunque lentamente, conducen a la humanidad hacia la democracia. Los conflictos sociales estuvieron presentes en el siglo XX. Dos grandes conflictos mundiales (las dos guerras mundiales) hablan claramente de la tendencia guerrerista del ser humano. Pero más que los conflictos mundiales, los conflictos sociales de cada país han generado caos, aunque no puede negarse que han impulsado cambios sociales profundos. La historia de Latinoamérica, particularmente, es la historia de las dictaduras militares, de las corrupciones gubernamentales y de la explotación extrema del hombre por el hombre. Unos pocos, dueños del poder económico, han arrastrado a las grandes mayorías hasta someterlos a estados de pobreza vergonzosos. Estos estados de injusticia han arrastrado a pequeños grupos o grandes contingentes a revelarse contra los explotadores, generándose así verdaderas guerras fraticidas. Sin salir de nuestra patria, podemos mencionar el levantamiento de masas campesinas de occidente, levantamiento que fue reprimido por las fuerzas militares del general Maximiliano Hernández Martínez, quien gobernó de 1931 a 1944.
                                    </p>
                                    <p>
                                        La literatura no ha estado al margen de este estado de convulsión social, y muchas novelas de la primera mitad del siglo XX dan testimonio de aquellos reiterados conflictos generados por un estado permanente de injusticia.
                                    </p>
                                </ul>
                        </div>
                        <div class="contenido subMenu">
                            <h5>La Revolución Mexicana.</h5>
                            <ul>
                                <p>
                                    En México, por los años de la revolución (de 1910 a 1919), las clases medias, los campesinos y los obreros, eran contrarios a la reelección del presidente Díaz, pero también opuestos a las costumbres aristocráticas y al afrancesamiento dominante, a la política económica del colonialismo capitalista y a la falta de libertades políticas bajo el régimen dictatorial. En mayo de 1910 se produjo en Morelos la insurrección de Emiliano Zapata al frente de los campesinos, que ocuparon las tierras en demanda de una reforma agraria. Díaz fue reelegido para un séptimo mandato. El 20 de noviembre se produjo la insurrección de Francisco (Pancho) Villa y Pascual Orozco en Chihuahua, pronto secundada en Puebla, Coahuila y Durango. Enseguida los revolucionarios sacaron a Díaz de la presidencia. El nuevo gobierno procedió al desarme de las fuerzas revolucionarias, aunque los zapatistas se negaron a ello. En las nuevas elecciones presidenciales resultó elegido Madero, pero que no logró alcanzar un acuerdo con Zapata ni con otros líderes agrarios.
                                </p>
                                <p>
                                    Zapata proclamó el Plan de Ayala, en el que se proponía el reparto de tierras y la continuación de la lucha revolucionaria. En ciudad de México tuvo lugar en febrero de 1913 la que se denominó 'Decena Trágica', enfrentamiento entre los insurrectos y las tropas del general Huerta, que causó alrededor de 2.000 muertos y 6.000 heridos.
                                </p>
                                <p>
                                    Las tropas constitucionalistas, formadas por campesinos y gentes del pueblo, derrotaron al Ejército federal por todo el territorio nacional: Villa ocupó Chihuahua y Durango con la División del Norte; Obregón venció en Sonora, Sinaloa y Jalisco con el Cuerpo de Ejército del Noroeste. Después del triunfo constitucionalista en Zacatecas y la ocupación de Querétaro, Guanajuato y Guadalajara, Huerta presentó la dimisión el 15 de julio siguiente y salió del país. En el Tratado de Teoloyucan se acordó la disolución del Ejército federal y la entrada de los constitucionalistas en la capital, que se produjo el 15 de agosto de 1914.
                                </p>
                                <p>
                                    Como suele ocurrir, las diferencias entre los revolucionarios aparecieron. Se dividieron en tres grupos: los villistas, los zapatistas y los carrancistas, vinculados éstos a la burguesía y deseosos de preservar los beneficios obtenidos por los generales, empresarios y abogados adictos a Carranza. Los ejércitos carrancistas al mando del general Obregón ocuparon Puebla el 4 de enero de 1915 y derrotaron a Villa en Celaya, Guanajuato, León y Aguascalientes, entre abril y julio del mismo año, por lo que Estados Unidos reconoció al gobierno de Carranza en el mes de octubre. Villa inició en el norte una guerra de guerrillas y trató de crear conflictos internacionales con Estados Unidos, cuyo gobierno, en 1916, envió tropas en su persecución, aunque éstas no lograron capturarlo. En el sur, Zapata realizó repartos de tierras en Morelos y decretó algunas medidas legales para intentar consolidar las reformas agrarias y las conquistas sociales logradas, pero también los zapatistas fueron derrotados por las tropas constitucionalistas al mando de Pablo González y obligados a replegarse a las montañas.
                                </p>
                                <p>
                                    En septiembre de 1916, Carranza convocó un Congreso Constituyente en Querétaro, donde se elaboró la Constitución de 1917, que consolidaba algunas de las reformas económicas y sociales defendidas por la revolución, en especial la propiedad de la tierra, la regulación de la economía o la protección de los trabajadores. En las elecciones posteriores, Carranza fue elegido presidente de la República y tomó posesión de su cargo el 10 de mayo de 1917. Zapata mantuvo la insurrección en el sur hasta que, víctima de una traición preparada por Pablo González, cayó en una emboscada en la hacienda de San Juan Chinameca, donde el 10 de abril de 1919 fue asesinado.
                                </p>
                                <p>
                                    La masificación de la cultura. Masificar la cultura significa ponerla al alcance de las mayorías. Los inicios del siglo XX ven aparecer dos medios de masificación de la cultura de grandes alcances: la radio y la televisión. Aunque tener un radio era signo de comodidad económica, no tardó mucho tiempo este aparato en llegar a las mayorías. Y con la llegada de la radio llega la información y la cultura. Pero más que la radio, fue la televisión la encargada de llevar imágenes culturales a muchos rincones de nuestras sociedades latinoamericanas.
                                </p>
                                <p>
                                    La mirada hacia lo autóctono. Los medios de comunicación hacen cada vez más pequeño el mundo. Lo que en un tiempo tardaba meses en conocerse, gracias a la radio, podía conocerse en el mismo día o minutos después de ocurridos (en la actualidad conocemos lo que ocurre en su momento) Esta difusión de información ha dotado a las gentes de nuevas costumbres en su estilo de vida: nuevas indumentarias, nuevos vocablos, nuevos hábitos, nueva visión del mundo... En resumen, somos cada vez más cosmopolitas. Todo esto conduce a una pérdida gradual de la identidad nacional. Pero a medida que profundizamos en esta pérdida de identidad, repentinamente, por un acto brusco de nostalgia, echamos una mirada hacia lo autóctono: a lo propio de nuestro país. Es un mecanismo de defensa contra la invasión impía de otras culturas.
                                </p>
                                <iframe class="video" src="https://www.youtube.com/watch?v=ldrxQKSkbE4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </ul>
                        </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>La novelística de tema social.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                            <p> 
                                Nuestros pueblos latinoamericanos han dependido siempre, en gran medida, de la explotación de la tierra. Los productos agrícolas extraídos de su entorno han sido su alimentación. De aquí resulta que el campesino, aquel hombre de piel quemada, ropa sencilla y medio descalzo, es un personaje cotidiano. Narrar sus costumbres, sus vicios, sus conflictos y sus miserias ha sido objetivo de muchos escritores. Da origen esto a la llamada novela regionalista-costumbrista. En una novela regionalistacostumbrista se trata al hombre en relación con la naturaleza. Se exponen en ella problemas como la explotación en las zonas agrícolas, se trata de las enfermedades propias de los trabajadores del campo, se describen las zonas geográficas en las que se mueven los campesinos... En estas novelas se describen los llanos, la pampa, la selva, los ríos... Y los protagonistas son indios, gauchos, llaneros... 
                            </p>
                            <iframe class="video" src="https://www.youtube.com/embed/WQNJZ6qQFQI?start=30" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <div class="contenido subMenu">
                            <h5>La novelística de la Revolución Mexicana.</h5>
                            <ul>
                                <p>
                                    Los principios que desencadenaron la revolución mexicana se irradiaron hacia muchos países latinoamericanos. Surgen en zonas distantes nuevos movimientos populares agrarios buscando los mismos fines perseguidos en México, aunque en la práctica los movimientos revolucionarios mexicanos resultaron ser una secuencia de confusiones ideológicas y un oleaje de injusticias, crímenes y revanchas. Todos aquellos movimientos revolucionarios que perseguían nobles fines, al menos en apariencia, se vieron respaldados por escritores que veían en tales movimientos románticos una tabla de salvación para los desprotegidos. Las escenas revolucionarias proporcionaron a los escritores abundante material para desarrollar un cuento o una novela.
                                </p>
                            </ul>
                        </div>
                        <div class="contenido subMenu">
                            <h5>La novelística indigenista.</h5>
                            <ul>
                                <p>
                                    En la novela indigenista el tema principal es el indígena y su entorno; particularmente el indígena y sus condiciones miserables de vida. Se narran en ellas las condiciones de explotación a las que son sometidos por parte de los blancos y ladinos. Condiciones que con frecuencia llevan al indígena a padecer graves enfermedades, humillación y, en muchas ocasiones, la muerte. En estas novelas encontramos al indígena trabajando rudamente por largas jornadas, jornadas que se prolongan desde que nace el sol hasta la noche. Los hallamos entre los árboles devorando lo que les es posible con el fin de aplacar la martirizante hambre. Vemos a sus pequeños hijos muriendo de infecciones intestinales y a sus mujeres siendo ultrajadas por los señores. El panorama es desalentador, y no se vislumbra una luz de esperanza. Surgen las revueltas por conseguir mejores condiciones laborales, pero lo que encuentran es la represión que los deja en peores condiciones. Así lo narran los novelistas. Pero los novelistas no son historiadores, sino hombres que han tomado partido; hombres que quieren cambiar la realidad que ven y que les parece denigrante; y en el peor de los casos, se trata de hombres que responden a ciertos intereses ideológicos.
                                </p>
                            </ul>
                        </div>
                        <div class="contenido subMenu">
                            <h5>La novela social urbana.</h5>
                            <ul>
                                <p>
                                    La novela social incluye a la indigenista. En estas novelas se tratan asuntos relacionados con revoluciones, conflictos de clase y las condiciones de los trabajadores en las plantaciones, minas, explotaciones marítimas...<br> 
                                    La novela social tuvo gran auge entre 1920 y 1950; época en la que se vio impulsada por las corrientes ideológicas, tales como el socialismo y el nacionalismo. La misma literatura surgida de la revolución mexicana sirvió de catalizadora para que otros países tomaran la misma ruta literaria.
                                </p>
                            </ul>
                        </div>
                        <div class="contenido subMenu">
                            <h5>Peculiaridades de la estética del realismo latinoamericano.</h5>
                            <ul>
                                <p>
                                    El realismo es, en literatura, un movimiento estético que trata de retratar objetivamente la realidad. Este realismo literario fue un movimiento cultural que tuvo su origen en Francia en la segunda mitad del siglo XIX. Dicho movimiento no tardó mucho tiempo en esparcirse por toda Europa, hasta llegar a nuestras tierras: Latinoamérica.
                                </p>
                                <p>
                                    Aquí en Hispanoamérica el realismo nos llega a finales del siglo XIX y a principios del XX. Nos llega con literatos como el uruguayo Eduardo Acevedo, el mexicano Rafael Delgado y el argentino Carlos María Ocantos. Estos escritores se encargaron de reflejar una realidad: los problemas sociales del ser humano. En la segunda mitad del mismo siglo nos llegaría un nuevo realismo: el realismo mágico. En el realismo mágico se nos muestra la realidad con matices fantásticos. Recordemos a García Márquez y Cien años de soledad.
                                </p>
                                <p>
                                    En Latinoamérica, dadas sus circunstancias sociales muy particulares, el realismo adquiere ciertas peculiaridades estéticas. Estas peculiaridades fueron el producto natural de unas sociedades abigarradas como las nuestras, en la que hay una mezcolanza de razas; en las que repentinamente llegan extranjeros a imponernos por la fuerza una nueva cultura. Llegan a nuestras tierras los conquistadores y esclavos negros, que pronto comienzan a mezclarse con los aborígenes. Los choques de razas y culturas hacen posible que el realismo adquiera en nuestras tierra.
                                </p>
                            </ul>
                        </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El español estándar.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Si alguien pronuncia la palabra niño, con seguridad se le entenderá en cualquier país de habla castellana. Pero a alguien que diga cipote, con seguridad que no le entenderán en cualquier país donde se hable español. Sin embargo, en El Salvador sí sabemos qué significa la palabra cipote. Esto se debe a que la palabra niño pertenece al español estándar; mientras que la palabra cipote es un regionalismo.
                                </p>
                               
                                <p>
                                    Y así como nosotros los salvadoreños le llamamos cipote a un infante o niño, así en otros países tienen otras palabras equivalentes que usan para sustituir la palabra niño. Para el caso se tiene que en Guatemala usan la palabra patojo o patoja como equivalente de niño o niña; en Costa Rica usan la palabra güila (para ambos sexos), o también chiquito o chiquita; en Nicaragua usan las palabras chavalo y chavala.
                                </p>
                                <p>
                                    En los diccionarios, los regionalismos están especificados. Por esto encontramos siempre el país o la región en la que se usa determinado vocablo. Para el caso tenemos que si buscamos la palabra aguilote, encontramos lo siguiente: m. Méx. Tomate de raíz venenosa. Venez. Ave de rapiña (Falco guayanensis).
                                </p>
                                <p>
                                    El nacimiento de nuevas palabras equivalentes a otras ya existentes, es el resultado de la dinámica social. Es el resultado de la interacción entre los miembros de nuestras complejas sociedades. Siempre se estarán formando nuevas palabras, de la misma manera que dejarán de usarse otras hasta convertirse en arcaísmos.
                                </p>
                                <p>
                                    En resumen podemos decir que el español estándar es aquel modelo comunicacionallingüístico que puede ser entendido por todo aquel que hable español. Contrario a esto se hallan los regionalismos, que son vocablos que sólo se entienden y usan en determinadas regiones.
                                </p>
                                <div class="subMenu">
                                    <h5>A continuación te presentamos algunos regionalismos.</h5>
                                    <ul>
                                        <table class="table table-hover table-bordered">
                                            <thead class="table-info">
                                                <tr>
                                                    <th>Palabra</th>
                                                    <th>Significado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>Aguatero.</th>
                                                    <th>En América, aguador, el que lleva o vende agua.</th>
                                                </tr>
                                                <tr>
                                                    <th>Bayunco.</th>
                                                    <th>En CA. tosco</th>
                                                </tr>
                                                <tr>
                                                    <th>Bicho.</th>
                                                    <th>El Salv. y Hond. niño, muchacho</th>
                                                </tr>
                                                <tr>
                                                    <th>Cabuda.</th>
                                                    <th>El Salv. y Hond. Contribución económica voluntaria de varias personas para un fin común.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cachivache.</th>
                                                    <th>Tiliche.</th>
                                                </tr>
                                                <tr>
                                                    <th>Canche.</th>
                                                    <th>En Centroamérica, rubio.</th>
                                                </tr>
                                                <tr>
                                                    <th>Chamaco.</th>
                                                    <th>En México, niño, muchacho.</th>
                                                </tr>
                                                <tr>
                                                    <th>Chévere</th>
                                                    <th>En Colombia, México y Venezuela, magnífico, muy bueno.</th>
                                                </tr>
                                                <tr>
                                                    <th>Chiche.</th>
                                                    <th>El Salv. y Nic. fácil.</th>
                                                </tr>
                                                <tr>
                                                    <th>Choyudo.</th>
                                                    <th>El Salv. y Guat. que todo lo hace con pereza</th>
                                                </tr>
                                                <tr>
                                                    <th>Chuco.</th>
                                                    <th>En América, que huele mal.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cuate.</th>
                                                    <th>En México y Guatemala, amigo, camarada.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cuerudo.</th>
                                                    <th>El Salv., Guat. y Nic. Persona a quien no le importan las críticas</th>
                                                </tr>
                                                <tr>
                                                    <th>Culillo.</th>
                                                    <th>En CA., Col., Ecuad., P. Rico y Ven. miedo</th>
                                                </tr>
                                                <tr>
                                                    <th>Dundo.</th>
                                                    <th>En Centroamérica, tonto.</th>
                                                </tr>
                                                <tr>
                                                    <th>Gambetear.</th>
                                                    <th>En Colombia, correr haciendo curvas o zigzag.</th>
                                                </tr>
                                                <tr>
                                                    <th>Goma.</th>
                                                    <th>En CA, malestar que se experimenta después de una borrachera.</th>
                                                </tr>
                                                <tr>
                                                    <th>Guagua.</th>
                                                    <th>En Chile, niño.</th>
                                                </tr>
                                                <tr>
                                                    <th>Guango.</th>
                                                    <th>En Méx. holgado, ancho.</th>
                                                </tr>
                                                <tr>
                                                    <th>Guaracha.</th>
                                                    <th>En Cuba, Guat., Nic., Pan. y P. Rico. baile popular afroantillano en parejas.</th>
                                                </tr>
                                                <tr>
                                                    <th>Guarapo. (Voz quechua).</th>
                                                    <th>En Am. jugo de la caña dulce exprimida, que por vaporización produce el azúcar.</th>
                                                </tr>
                                                <tr>
                                                    <th>Jayán.</th>
                                                    <th>En El Salv. y Nic. persona vulgar y grosera en sus dichos o hechos.</th>
                                                </tr>
                                                <tr>
                                                    <th>Jetón.</th>
                                                    <th>En Chile, tonto.</th>
                                                </tr>
                                                <tr>
                                                    <th>Jilote.</th>
                                                    <th>En CA y Méx. mazorca de maíz cuando sus granos no han cuajado aún.</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El español de América latina.<span></span></a>
                    <ul>
                        <li>
                             <div class="contenido">
                                <p>
                                    El español de América presenta ciertas características propias como producto de diversas influencias internas y externas. Un factor determinante es la influencia de las lenguas indígenas (recordemos que a la llegada de los españoles, en América existían alrededor de 170 grupos lingüísticos, los cuales se fueron reduciendo paulatinamente por la misma acción de los conquistadores); otro factor es el tipo de español que nos trajeron los españoles durante la colonización. Estos españoles, a su llegada a América, se encontraron con una realidad distinta, no imaginada; se encontraron con una gama de situaciones y objetos que necesitaban designar con alguna palabra. Para conseguir tal propósito, echaron mano en primer lugar del vocabulario utilizado por los aborígenes. Así fue como tomaron prestadas ciertas palabras, principalmente de tres lenguas: el arawak, el náhuatl y el quechua.
                                </p>
                                <p>
                                    El arawak es una familia lingüística perteneciente a tribus amerindias que habitaba la zona tropical y ecuatorial de América del Sur, Grandes Antillas y la Florida. Agrupa alrededor de 80 lenguas y dialectos. Del arawak se derivan las palabras canoa, cacique y maíz. El náhuatl es la lengua precolombina que se hablaba en México, principalmente en el centro de México, y también en Centroamérica. El náhuatl se utiliza en el presente por alrededor de un millón de personas, en México y en El Salvador. De esta lengua se derivan las palabras aguacate, chocolate, jitomate, jícara... Y del quechua (lengua precolombina hablada en la zona andina, principalmente en Perú) que comprende más de veinte lenguas. Del quechua se derivan las palabras alpaca, guanaco y cóndor.
                                </p>
                                <p>
                                    Otro aspecto a considerar es el posible influjo de las lenguas precolombinas en la pronunciación, aunque esto es muy discutido. Así tenemos que en los Andes del Ecuador, Perú y Bolivia es frecuente la confusión entre la e y la i; y la o y la u; lo que se debe a una influencia del quechua.
                                </p>
                                <p>
                                    Otra características del español de América es el uso del vocablo usted en lugar de tú (segunda persona del singular); y de vos en lugar de vosotros (segunda persona del plural) Usamos el usted como una forma de respeto ante personas que no conocemos o conocemos muy poco. Y utilizamos el voz con personas de nuestra entera confianza: amigos, familiares, compañeros de clase... Esta es la razón por la cual es casi imposible encontrar a dos hermanos o hermanas tratándose de usted. Pero también utilizamos el tú; y lo hacemos en casos intermedios, por ejemplo ante personas que conocemos poco; aunque es más frecuente su uso en personas con cierto nivel de educación. Por esta razón es que en el campo es muy difícil encontrar a personas que usen el tú; pero es muy frecuente, casi general, su uso en personas de la ciudad y de cierta categoría social.
                                </p>
                                <p>
                                    Pero el uso del usted y del vos trae cambios en el verbo. Cuando usas el vos, el presente del verbo lleva tilde en la última sílaba. Hay otros cambios que puedes observarlos en la tabla siguiente con el verbo comprar.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El español de El salvador.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    El fenómeno ocurrido en América es la sumatoria de los fenómenos ocurridos en cada punto geográfico. Gran parte de América fue conquistada por los españoles; y El Salvador fue conquistado por Pedro de Alvarado y su gente. Con la conquista de nuestras tierras, las lenguas indígenas no desaparecieron automáticamente. Se mantuvieron por siglos, aunque cada vez en grupos más pequeños de aborígenes que no fueron afectados por el mestizaje. Dos fueron las principales lenguas indígenas de nuestras tierras: el náhuatl y el maya. 
                                </p>
                              
                                <p>
                                    Los vocablos derivados del náhuatl se conocen como nahualismos. Son nahualismos los siguientes: jícara, jocote, chocolate, tamal, guacal, aguacate, Quezaltepeque, Cihuatán..
                                </p>
                                <h4>Salvadoreñismos.</h4>
                                <p>
                                    Los salvadoreñismos son vocablos utilizados por los salvadoreños. Muchos de estos vocablos se derivan del náhuat.
                                </p>
                                <div class="contenido subMenu">
                                    <h5>A continuación una lista de salvadoreñismos.</h5>
                                    <ul>
                                        <table>
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>Palabra</th>
                                                    <th>Significado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>Agüitado, da adj. Neologismo.</th>
                                                    <th>Aburrido.</th>
                                                </tr>
                                                <tr>
                                                    <th>Atarantado, da.</th>
                                                    <th>Medio ebrio.</th>
                                                </tr>
                                                <tr>
                                                    <th>Baboso, sa.</th>
                                                    <th>Cambio semántico. Tonto, cándido.</th>
                                                </tr>
                                                <tr>
                                                    <th>Bayunco, ca.</th>
                                                    <th>Adj. Cambio semántico. Ridículo, de mal gusto.</th>
                                                </tr>
                                                <tr>
                                                    <th>Bereco, ca. adj.</th>
                                                    <th>Tonto, falto de entendimiento o razón.</th>
                                                </tr>
                                                <tr>
                                                    <th>Birusco, ca. Adj. Neologismo.</th>
                                                    <th>Avispado, listo, inquieto.</th>
                                                </tr>
                                                <tr>
                                                    <th>Bolo</th>
                                                    <th>la. Adj. Cambio semántico Ebrio.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cacaso.</th>
                                                    <th>Neologismo. Que no sirve.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cacha.</th>
                                                    <th>Anglicismo. De "TO CATCH",coger. Cambio semántico. Hacer la cacha: hacer la lucha, tratar de conseguir.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cachete. Cambio semántico.</th>
                                                    <th>Favor.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cachimbon.</th>
                                                    <th>na. Adj. Cambio semántico. Bueno, bonito, valiente.</th>
                                                </tr>
                                                <tr>
                                                    <th>De choto.</th>
                                                    <th>En El Salvador, de gratis.</th>
                                                </tr>
                                                <tr>
                                                    <th>Cipote.</th>
                                                    <th>Nahuatismo. Posiblemente de SEPOCTI, desvalido, entumido. Nombre que se les da a los niños.</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>Oraciones complejas.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    Las oraciones complejas o compuestas son aquellas formadas por más de un predicado. Cada uno de estos predicados recibe el nombre de proposición. Para el caso se tiene que en la oración compuesta: Ella preparará la comida, vos limpiarás los muebles y él lavará los platos; se tienen tres proposiciones.
                                </p>
                                <p>
                                    En una oración compleja habrá tantas proposiciones como verbos haya. Las proposiciones son segmentos con estructura oracional: poseen sujeto y predicado. Dichas proposiciones se relacionan por coordinación o por subordinación.
                                </p>
                               
                                <iframe class="video" src="https://www.youtube.com/embed/ftFp0XGJmf8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El informe.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    En la unidad 2 dimos algunas sugerencias para quien se dispone a desarrollar un escrito. Hablamos de elegir un tema determinado, delimitarlo, buscar la información pertinente y seleccionar tal información. En resumen, hablamos del tratamiento de la información. Hecho esto, estamos listo para el siguiente paso: la redacción del documento respectivo. Este documento puede ser un ensayo, un reportaje o un informe.
                                </p>
                                <p>
                                    El informe es un resumen de hechos o actividades partiendo de datos comprobados. Frecuentemente, un informe contiene, además de los datos, la interpretación del autor, sus conclusiones y recomendaciones.
                                </p>
                               
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/IfjlHw4O7LQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <script src="../controllers/JS/menu1.js"></script>
    <script src="bootstrap/css/bootstrap.min.css"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>