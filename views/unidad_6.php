<!DOCTYPE html>
<html>
<head>
    <title>Lenguaje: Unidad 6</title>
    <link rel="stylesheet" type="text/css" href="css/unidades.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/fonts.css">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../controllers/JS/arriba.js"></script>
    <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body class="bg-white">
    <div class="container cont-barra col-12 col-md-12 col-sm-12">
            <div class="cont-img col-3 col-md-3 col-sm-3">
                <button onclick="location.href='principal.php'" class="btn btn-outline-dark btn-primary logo"><i class="fas fa-arrow-circle-left"></i>Unidades</button>
            </div>
            <div class="cont-hi col-3 col-md-5 col-sm-5">
                <button class="btn btn-primary" onclick="location.href='testL/test.php'">Iniciar Test</button>
            </div>
            <div class="cont-slideMenu">
                <input type="checkbox" id="btn-menu">
                <label for="btn-menu" class="fas fa-bars float-right"></label>
                <div class="slideMenu">
                    <ul>
                        <li><a class="btn" href="perfil.php"><span class="fas fa-user"></span> Perfil</a></li>
                        <li><a class="btn" href="ranking.php"><span class="fas fa-cubes"></span> Ranking</a></li>
                        <li><a class="btn" href="#"><span class="fas fa-question-circle"></span> Ayuda</a></li>
                        <li><a class="btn" href="../controllers/logout.php"><span class="fas fa-sign-out-alt"></span> Salir</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <header>
        <center><h1>Unidad 6: literatura de la vanguardia latinoamericana</h1></center>
        <div class="menu">
            <ul>
                <li class="subMenu">
                    <a>Sociedad y cultura en Latinoamérica en la segunda mitad del siglo XX.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido">
                                <p>
                                    Bajo el signo de la guerra fría. Buena parte de la segunda mitad del siglo XX estuvo marcada por los signos de la llamada guerra fría. Esta guerra fría fue de naturaleza ideológica, y se libró entre los Estados Unidos y sus aliados y el grupo de naciones lideradas por la Unión Soviética No se produjo un conflicto militar directo entre ambas superpotencias, pero surgieron intensas luchas económicas y diplomáticas. Los distintos intereses condujeron a una sospecha y hostilidad mutuas enmarcadas en una rivalidad ideológica en aumento (socialismo o izquierda y capitalismo o derecha).
                                </p>
                                <h6>El crecimiento de las grandes urbes.</h6>
                                <p>
                                    Es también en el siglo XX cuando las grandes urbes crecen desmedidamente en torno de los centros de producción industrial; pues los empleados buscan alojamiento en torno de los centros laborales. Justamente esto provoca diversos problemas sociales: hacinamiento, delincuencia, prostitución, drogadicción... Esta variedad de conflictos sociales también será abordada por la literatura. Una obra típica que retrata estos conflictos es Los hijos de Sánchez. En esta obra se describe la vida de una familia de las zonas marginales.
                                </p>
                                <p>
                                    El impacto de los medios masivos y las nuevas tecnologías comunicativas en la cultura. Indiscutiblemente que los medios masivos de comunicación (la radio, la televisión, el cine y la internet) han impactado poderosamente en la cultura hasta generar una verdadera revolución cultural. Desde luego que debemos considerar los efectos nocivos de esta revolución. Y es que la televisión la podemos utilizar para ver un espectáculo de alta calidad cultural como para ver basura cultural. En fin, la culpa no es del todo de los medios sino de quien elige qué tipo de programa sintonizará. ¡Cuidado! Démosle un buen uso a los medios de comunicación masivos.
                                </p>
                                <p>
                                    Las distintas temporalidades históricas. En la actualidad, la literatura en´defensa del comunismo ha perdido vigencia. Sería absurdo levantar la pluma para defender un sistema fracasado. Y es que el arte, como ocurre con casi todo en la vida, presenta ciertos matices con el transcurso del tiempo, o mejor dicho, con los cambios generados en las distintas sociedades. En otras palabras, el momento histórico es el que genera una expresión cultural determinada. En lo literario y artístico, el pasado siglo XX presentó distintas tendencias: el romanticismo, el realismo, el realismo mágico y el arte de denuncia. Sin embargo, hay algo que perdura: el arte clásico: ballet, teatro, música...
                                </p>
                            </div> 
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>El realismo mágico.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    El boom. El término boom es una voz inglesa que significa un avance extraordinariamente rápido. Así podemos hablar de un boom en genética o de un boom en informática. En literatura lo que nos interesa es el boom de la literatura hispanoamericana.
                                </p>
                                <iframe class="video" src="https://www.youtube.com/embed/oBJNxpkSV58" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="subMenu">
                    <a>La narrativa de la gran urbe.<span></span></a>
                    <ul>
                        <li>
                            <div class="contenido" style="height='320'">
                                <p>
                                    Las zonas urbanas, dado el crecimiento demográfico y el flujo de personas del campo hacia la ciudad, son cada vez más reservorios de conflictos sociales. Las aglomeraciones humanas generan una descomposición social que va de la mano con la delincuencia, la prostitución, la drogadicción y la propagación de las enfermedades. Pero este ambiente degradado también genera literatura: la narrativa de la gran urbe. Una literatura en la que se describe el mundo sórdido en la que unos seres humanos se mueven agobiados por las responsabilidades que genera la vida urbana. Una literatura denominada por algunos como realismo pesimista, dado que destaca lo negativo del ser humano.
                                </p>
                              
                                <p>
                                    Juan Carlos Onetti escribió narrativa de la gran urbe. El tema unificador de toda su obra es la corrupción de la sociedad, sus efectos sobre el individuo y las dificultades para encontrar una respuesta adecuada a ella. En Tierra de nadie (1942) presenta el depresivo y pesimista retrato del paisaje urbano. En Juntacadáveres (1964) trata de la prostitución y la pérdida de la inocencia. 
                                </p>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <script src="../controllers/JS/menu1.js"></script>
    <script src="bootstrap/css/bootstrap.min.css"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>