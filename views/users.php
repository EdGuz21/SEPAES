<?php
  require_once '../models/conexion.php';
  include '../controllers/funcs/funcs.php';
  session_start(); //Iniciar una nueva sesión o reanudar la existente 
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="AlertifyJS/css/alertify.min.css" />
        <link rel="stylesheet" type="text/css" href="css/menu_materias.css">
        <link rel="stylesheet" type="text/css" href="css/lista_usuarios.css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/3e65a18a1e.js" crossorigin="anonymous"></script>
        <title>Lista de Usuarios</title>
    </head>
<body>
    <div  id="cols" class="col-12">
        <div id="col-img" class="">
            <img  id="sepaes2" src="css/imagenes/sepaes2.png">
        </div>
        <div id="col-saludo" class="">
            
        </div>
        <div id="col-btns" class="">
          <a id="btn-salir" class="btn btn-primary" href="../controllers/logout.php">Salir<span class="sr-only">(current)</span></a>
        </div>
            
    </div>
    <br><br><br><br>

    <div class="todo">  
        <div id="contenido">
            <form class="finder" action="../controllers/buscar.php">
                <div class="search-box">
                    <input name="nombre" type="text" class="search-txt" placeholder="Buscar usuario">
                    <button type="submit" class="search-btn" href="../controllers/buscar.php"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <div id="datos">
            
            </div>
        </div>  
    </div>
    
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../controllers/JavaScript/swiper.min.js"></script>
</body>
</html>